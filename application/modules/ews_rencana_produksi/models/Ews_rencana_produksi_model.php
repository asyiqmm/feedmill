<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Ews_rencana_produksi_model extends CI_Model
{
    public function get_rencana_produksi($date, $date2)
    {
        if ($this->ion_auth->in_group('qa')) {
            $qa = true;
        } else {
            $qa = false;
        }
        $this->db->select('a.*')
            ->from('t_ews_produksi a');
        // var_dump($this->ion_auth->in_group('operato'));die();

        if ($qa) {
            $this->db->where('a.status', 2);
            $this->db->or_where('a.status', 3);
        }
        $this->db->where('a.tgl_produksi BETWEEN "' . $date . '" and "' . $date2 . '"');

        return $this->db->get();

        // var_dump($this->db->last_query());
        // die();
    }

    public function get_pakan()
    {
        $this->db->select('kode_pakan,nama_pakan,jenis,fase');
        $this->db->from('m_pakan a');
        $this->db->where('a.active', 1);
        return $this->db->get();
    }

    public function post_produksi()
    {
        $this->db->trans_begin();
        $date = str_replace('/', '-', $_POST['tgl_produksi']);
        $_POST['tgl_produksi'] =  date('Y-m-d', strtotime($date));

        $this->db->select('COALESCE(MAX(no_input), 0) no_input');
        $this->db->from('t_ews_produksi');
        $max = $this->db->get()->row_array();
        $_POST['no_input'] = $max['no_input'] + 1;
        $this->db->insert('t_ews_produksi', $_POST);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambahkan data'
            ];
        }
    }

    public function post_proses_qa()
    {
        $this->db->trans_begin();
        if ($_POST['status_qa']) {
            $_POST['qa']['status'] = 3;
        }
        $get_limit = $this->db
            ->where('jenis', $_POST['pakan']['jenis'])
            ->where('fase', $_POST['pakan']['fase'])
            ->get('m_limit_ews')->row();
        $_POST['qa']['min_temp_aktual'] = $get_limit->min_temp_aktual;
        $_POST['qa']['temp_aktual_min_temp_aktual'] = ($_POST['qa']['temp_aktual'] <= $get_limit->min_temp_aktual ? 1 : 0);

        $_POST['qa']['max_ampere']      = $get_limit->max_ampere;
        $_POST['qa']['ampere_max_ampere'] = ($_POST['qa']['ampere'] >= $get_limit->max_ampere ? 1 : 0);

        $_POST['qa']['max_dust_cooler'] = $get_limit->max_dust_cooler;
        $_POST['qa']['dust_cooler_max_dust_cooler'] = ($_POST['qa']['dust_cooler'] >= $get_limit->max_dust_cooler ? 1 : 0);

        $_POST['qa']['std_pdi']         = $get_limit->std_pdi;
        $_POST['qa']['pdi_pellet_std_pdi'] = ($_POST['qa']['pdi_pellet'] <= $get_limit->std_pdi ? 1 : 0);

        $_POST['qa']['8_mesh'] = ($_POST['qa']['sz8_sample1'] + $_POST['qa']['sz8_sample2']) / 2;

        $_POST['qa']['tepungan'] = ($_POST['qa']['sz14_sample1'] + $_POST['qa']['sz14_sample2']) / 2;
        $_POST['qa']['max_tepungan']    = $get_limit->max_tepungan;
        $_POST['qa']['tepungan_max_tepungan'] = ($_POST['qa']['tepungan'] >= $get_limit->max_tepungan ? 1 : 0);
        // var_dump($_POST['pakan']['no_input']);die();
        $this->db->where('no_input', $_POST['pakan']['no_input']);
        $this->db->update('t_ews_produksi', $_POST['qa']);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambahkan data'
            ];
        }
    }

    public function post_proses_operator()
    {
        $this->db->trans_begin();
        $_POST['operator']['status'] = 2;
        $this->db->where('no_input', $_POST['no_input'])
            ->update('t_ews_produksi', $_POST['operator']);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambahkan data'
            ];
        }
    }

    public function delete_produksi()
    {
        $this->db->trans_begin();
        $_POST['operator']['status'] = 2;
        $this->db->where('no_input', $_POST['no_input'])
            ->update('t_ews_produksi', $_POST['operator']);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambahkan data'
            ];
        }
    }
}
