<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Ews_rencana_produksi extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('ews_rencana_produksi_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Rencana Produksi | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 28;
			$data['child_active'] = 29;
			$data['grandchild_active'] = null;

			$data['pakan'] = $this->ews_rencana_produksi_model->get_pakan()->result();
			// var_dump($data['pakan']);die();
			$this->_render('ews_rencana_produksi_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_rencana_produksi()
	{
		$dt = DateTime::createFromFormat("d/m/Y", $_GET['date']);
		$date = $dt->format('Y-m-d');
		$dt2 = DateTime::createFromFormat("d/m/Y", $_GET['date2']);
		$date2 = $dt2->format('Y-m-d');
		$datas = $this->ews_rencana_produksi_model->get_rencana_produksi($date, $date2);


		$obj = array("data" => $datas->result_array());
		echo json_encode($obj);
	
	}

	public function post_produksi(Type $var = null)
	{
		
		$insert = $this->ews_rencana_produksi_model->post_produksi();
		echo json_encode($insert);
	}

	public function post_proses_qa(Type $var = null)
	{
		$insert_qa = $this->ews_rencana_produksi_model->post_proses_qa();
		echo json_encode($insert_qa);
	}

	public function post_proses_operator(Type $var = null)
	{
		$insert_operator = $this->ews_rencana_produksi_model->post_proses_operator();
		echo json_encode($insert_operator);
	}

	public function delete()
	{
		var_dump($_POST);die();
		$delete = $this->ews_rencana_produksi_model->delete_produksi();
		echo json_encode($delete);
	}
}
