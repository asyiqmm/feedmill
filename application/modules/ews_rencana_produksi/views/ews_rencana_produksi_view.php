<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Produksi</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">

                                    <div class="col-sm-12 col-md-3">
                                        <?php if ($this->ion_auth->is_admin()) { ?>
                                            <div class="dt-buttons">
                                                <button data-toggle="modal" data-target="#modal-data-produksi" class="dt-button btn btn-sm btn-primary btn-add">Tambah Rencana Produksi</button>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-12 col-md-1">
                                        <div class="dataTables_filter">
                                            <label>Tanggal:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dt-buttons ">
                                            <div class="form-inline justify-content-center">
                                                <div class="form-group">
                                                    <div class="tgl" id="datetimepickerawal" data-target-input="nearest">
                                                        <input type="text" class="form-control form-control-sm form-control form-control-sm-sm datetimepicker-input" value="<?php echo date('d/m/Y') ?>" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickerawal" id="bulan" data-target="#datetimepickerawal" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    ~
                                                </div>
                                                <div class="form-group">
                                                    <div class="tgl" id="datetimepickerakhir" data-target-input="nearest">
                                                        <input type="text" class="form-control form-control-sm form-control form-control-sm-sm datetimepicker-input" value="<?php echo date('d/m/Y') ?>" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickerakhir" id="bulan2" data-target="#datetimepickerakhir" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class=" date dataTables_filter" id="datetimepicker1" data-target-input="nearest">

                                                <input type="text" class="form-control form-control-sm form-control form-control-sm-sm datetimepicker-input" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepicker1" id="bulan" data-target="#datetimepicker1" />

                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="dataTables_filter">
                                            <label>Search:<input type="search" id="tabel_produksi_filter" class="form-control form-control-sm form-control form-control-sm-sm" placeholder="" aria-controls="tabel_rencana_produksi"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <img src="" alt=""> -->
                            <table id="tabel_produksi" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th></th>
                                        <th>Kode Pakan</th>
                                        <th>Jenis Pakan</th>
                                        <th>Fase Pakan</th>
                                        <th>No Input</th>
                                        <th>Kode Batch</th>
                                        <th>Nama Pakan</th>
                                        <th>Formula</th>
                                        <th>Tgl Produksi</th>
                                        <th>Shift</th>
                                        <th>No Pellet</th>
                                        <th>Steam</th>
                                        <th>Retentioner</th>
                                        <th>Temp Panel</th>
                                        <th>
                                            <?php if ($this->ion_auth->in_group('operator')) { ?>
                                                Proses Operator
                                            <?php } elseif ($this->ion_auth->in_group('qa')) { ?>
                                                Proses QA
                                            <?php } ?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<!-- Langkah Supervisor -->
<div class="modal fade" id="modal-data-produksi">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Rencana Produksi Baru </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="post_produksi" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="kode_pakan">Tanggal</label>
                            <div class="tgl" id="datetimepickertanggal" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickertanggal" id="tanggal" name="tgl_produksi" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="shift">Shift</label>
                            <input id="shift" name="shift" type="number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="kode_pakan">Pakan</label>
                            <select name="kode_pakan" id="select_pakan" class="select2bs4" required>
                                <option></option>
                                <?php foreach ($pakan as $key => $value) {
                                    echo '<option value="' . $value->kode_pakan . '" nama="' . $value->nama_pakan . '" jenis="' . $value->jenis . '" fase="' . $value->fase . '">[' . $value->kode_pakan . '] ' . $value->nama_pakan . '</option>';
                                } ?>
                            </select>
                            <input type="hidden" id="nama_pakan" name="nama_pakan">
                        </div>
                        <div class="col-md-3">
                            <label for="kode_pakan">Kode Pakan</label>
                            <input id="kode_pakan" type="text" class="form-control" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="jenis_pakan">Jenis Pakan</label>
                            <input id="jenis_pakan" name="jenis_pakan" type="text" class="form-control" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="fase_pakan">Fase Pakan</label>
                            <input id="fase_pakan" name="fase_pakan" type="text" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="kode_batch">Kode Batch</label>
                            <input type="text" name="kode_batch" id="kode_batch" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="formula">formula:</label>
                            <select class="select2bs4" name="formula" id="formula" required>
                                <option value="010521">010521</option>
                                <option value="040521">040521</option>
                                <option value="050521">050521</option>
                                <option value="090521">090521</option>

                            </select>
                            <!-- <input type="text" id="formula" name="formula" class="form-control form-control-sm"> -->
                        </div>


                    </div>
                    <!-- <div class="form-group row">
                        <div class="col-md-3">
                            <label for="steam">Steam</label>
                            <input type="text" name="steam" id="steam" class="qtytimbang form-control form-control-sm" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="retentioner">Retentioner</label>
                            <input type="text" class="form-control form-control-sm" name="retentioner" id="retentioner" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="temp_panel">Temp Panel</label>
                            <input type="text" class="form-control form-control-sm" name="temp_panel" id="temp_panel" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="no_pellet">No Pellet:</label>
                            <input type="text" class="form-control form-control-sm" name="no_pellet" id="total_prosen" required>
                        </div>

                    </div> -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Langkah operator -->
<div class="modal fade" id="modal-proses-operator">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title-operator"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="form_proses_operator" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="operator_no_input">No Input</label>
                            <input type="text" class="form-control form-control-sm" id="operator_no_input" name="no_input" required readonly>
                        </div>
                        <div class="col-md-4">
                            <label for="operator_tgl">Tanggal</label>
                            <input type="text" class="form-control form-control-sm" id="operator_tgl" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_shift">Shift</label>
                            <input id="operator_shift" type="number" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="operator_nama_pakan">Pakan</label>
                            <input type="text" class="form-control form-control-sm" id="operator_nama_pakan" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="operator_kode_pakan">Kode Pakan</label>
                            <input id="operator_kode_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_jenis_pakan">Jenis Pakan</label>
                            <input id="operator_jenis_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_fase_pakan">Fase Pakan</label>
                            <input id="operator_fase_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="operator_kode_batch">Kode Batch</label>
                            <input type="text" id="operator_kode_batch" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-sm-6">
                            <label for="operator_formula">formula:</label>
                            <input type="text" id="operator_formula" class="form-control form-control-sm" readonly>
                        </div>


                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="steam">Steam</label>
                            <input type="text" name="operator[steam]" id="opertaor_steam" class="qtytimbang form-control form-control-sm input-operator" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="retentioner">Retentioner</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[retentioner]" id="opertaor_retentioner" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="temp_panel">Temp Panel</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[temp_panel]" id="opertaor_temp_panel" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="no_pellet">No Pellet:</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[no_pellet]" id="opertaor_no_pellet" required>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnOperator" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Langkah QA -->
<div class="modal fade" id="modal-proses-qa" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Proses produksi oleh QA </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_proses" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_no_input">No. Input</label>
                                    <input type="text" name="pakan[no_input]" class="form-control form-control-sm" autocomplete="off" id="proses_no_input" readonly />
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_tanggal">Tanggal</label>
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="proses_tanggal" readonly />
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_shift">Shift</label>
                                    <input id="proses_shift" type="number" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="proses_nama_pakan">Nama Pakan</label>
                                    <input id="proses_nama_pakan" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_kode_pakan">Kode Pakan</label>
                                    <input id="proses_kode_pakan" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_jenis_pakan">Jenis Pakan</label>
                                    <input id="proses_jenis_pakan" name="pakan[jenis]" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-2">
                                    <label for="proses_fase_pakan">Fase Pakan</label>
                                    <input id="proses_fase_pakan" name="pakan[fase]" type="text" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="proses_kode_batch">Kode Batch</label>
                                    <input type="text" id="proses_kode_batch" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-sm-6">
                                    <label for="proses_formula">formula:</label>
                                    <input type="text" id="proses_formula" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_steam">Steam</label>
                                    <input type="text" id="proses_steam" class="qtytimbang form-control form-control-sm" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_retentioner">Retentioner</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_retentioner" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_temp_panel">Temp Panel</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_temp_panel" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_no_pellet">No Pellet:</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_no_pellet" readonly>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-6 border-left">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_temp_aktual">Temp Aktual</label>
                                    <input id="proses_temp_aktual" name="qa[temp_aktual]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_ampere">Ampere</label>
                                    <input id="proses_ampere" name="qa[ampere]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_speed_feeder">Speed Feeder</label>
                                    <input id="proses_speed_feeder" name="qa[speed_feeder]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_dust_cooler">Dust Cooler</label>
                                    <input id="proses_dust_cooler" name="qa[dust_cooler]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-top border-bottom">
                                    <tr>
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>PDI</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_pdi_pellet">PDI Pellet</label>
                                                <input id="proses_pdi_pellet" name="qa[pdi_pellet]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_crumble">Crumble</label>
                                                <input id="proses_crumble" name="qa[crumble]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-bottom">
                                    <tr>
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>Tertahan di SIZING 8</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz8_sampe1">Sample 1</label>
                                                <input id="proses_sz8_sampe1" name="qa[sz8_sample1]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz8_sampe2">Sample 2</label>
                                                <input id="proses_sz8_sampe2" name="qa[sz8_sample2]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-bottom">
                                    <tr class="align-middle">
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>Tertahan di SIZING 14</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz14_sampe1">Sample 1</label>
                                                <input id="proses_sz14_sampe1" name="qa[sz14_sample1]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz14_sampe2">Sample 2</label>
                                                <input id="proses_sz14_sampe2" name="qa[sz14_sample2]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="hidden" id="status_qa" name="status_qa">
                                <textarea class="form-control" name="qa[keterangan]" id="proses_keterangan" cols="1" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnProcess" class="btn btn-primary">Proses</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(function() {
        // datatable_premix();
        // list_formula();
        $('.tgl').datetimepicker({
            format: 'DD/MM/YYYY',
            autoclose: true,
            // todayHighlight: true,
            minuteStepping: 1, //set the minute stepping

            // minDate:'1900/1/1',               //set a minimum date  <---- HERE 
            maxDate: moment(),
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.qtytimbang').inputmask('decimal', {
            allowMinus: false,
            integerDigits: 9,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
        });
    });

    $('#select_pakan').on('change', function() {
        var val = $(this);
        var attr = $('option:selected', this);
        $('#nama_pakan').val(attr.attr('nama'));
        $('#kode_pakan').val(val.val());
        $('#jenis_pakan').val(attr.attr('jenis'));
        $('#fase_pakan').val(attr.attr('fase'));
    });

    function list_formula() {
        var formula = $('#fml_no').val();
        var name = $('#fml_nm').val();
        jQuery.ajax({
            type: "GET",
            url: "https://ismaf.mydmc.co.id/api/formulapremixmaster?formula=" + formula + "&name=" + name, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.data.length; index++) {
                    option += '<option value=' + data.data[index].notrans + ' fml_no=' + data.data[index].fml_no + '>' + data.data[index].item_name + '</option>';
                }
                if (data.meta.code == 200) {
                    showToast(data.meta.message);
                    $('#fml_nm').html(option);
                } else {
                    showToastWarning(data.meta.message);
                }

            },
            error: function(xhr, status, error) {
                showToastWarning('error')
            }

        });
    };
    $("#datetimepicker2,#datetimepicker3").on("change.datetimepicker", ({
        date
    }) => {
        var start = $('#fml_start').val();
        var end = $('#fml_end').val();
        jQuery.ajax({
            type: "GET",
            url: "premix_rencana_produksi/list_formula?start=" + start + "&end=" + end, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.length; index++) {
                    option += '<option value=' + data[index].notrans + ' item=' + data[index].item + ' fml_no=' + data[index].fml_no + '>' + data[index].item_name + '</option>';
                }
                $('#fml_nm').html(option);

                // if (data.meta.code == 200) {
                //     showToast(data.meta.message);
                // } else {
                //     showToastWarning(data.meta.message);
                // }

            },
            error: function(xhr, status, error) {
                // showToastWarning('error')
            }

        });
    });

    $("#datetimepickerawal").on("change.datetimepicker", ({
        date
    }) => {
        var date2 = $('#bulan2').val();

        datatable_premix(moment(date).format("DD/MM/YYYY"), date2);
    });

    $("#datetimepickerakhir").on("change.datetimepicker", ({
        date
    }) => {
        var date1 = $('#bulan').val();
        datatable_premix(date1, moment(date).format("DD/MM/YYYY"));
    });


    function datatable_premix(date1, date2) {
        $('#tabel_produksi').DataTable().clear();
        // $('#daily_report_table_body').DataTable().destroy();
        $('#tabel_produksi').empty();
        var siteTable = $('#tabel_produksi').DataTable({
            "ajax": 'Ews_rencana_produksi/get_rencana_produksi?date=' + date1 + '&date2=' + date2,
            "paging": false,
            "lengthChange": false,
            "destroy": true,
            searching: true,
            dom: 't',
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [1, "asc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            rowCallback: function(row, data, index) {
                // if (data.status == 3) {
                //     $(row).find('td').addClass('bg-success');
                //     $(row).find('td').css('color', 'white');
                // }
            },
            columns: [{
                "className": '',
                "orderable": false,
                "data": null,
                "render": function(data, type, row, meta) {
                    console.log('ha', row.steam);
                    var btn = '';
                    if (row.temp_aktual == null || row.ampere == null || row.speed_feeder == null || row.dust_cooler == null || row.pdi_pellet == null || row.crumble == null || row.sz8_sample1 == null || row.sz8_sample2 == null || row.sz14_sample2 == null || row.sz14_sample2 == null || row.keterangan == null) {
                        // btn = '<button title="proses" class="btn btn-primary btn-sm btn-proses-qa">Proses</button>';
                    } else {
                        btn = '<button class="btn details-control"><i class=" center expbd fas fa-angle-right"></i></button>';
                    }
                    return btn;
                },
                "defaultContent": '<button class="btn"><i class="center expbd fas fa-angle-right"></i></button>'
            }, {
                data: 'kode_pakan',
                className: 'align-middle align-middle text-nowrap',
                width: '10%',
            }, {
                data: 'jenis_pakan',
                className: 'align-middle align-middle text-nowrap',
                width: '10%',
            }, {
                data: 'fase_pakan',
                className: 'align-middle align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'no_input',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'kode_batch',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'nama_pakan',
                className: 'align-middle align-middle',
                width: '15%',
            }, {
                data: 'formula',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'tgl_produksi',
                className: 'align-middle align-middle text-center',
                width: '5%',
            }, {
                data: 'shift',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'steam',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'retentioner',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'temp_panel',
                className: 'align-middle text-center align-middle',
                width: '1%',
            }, {
                data: 'no_pellet',
                className: 'align-middle text-center align-middle text-nowrap',
                width: '5%',
            }, {

                className: 'align-middle text-left text-nowrap',
                width: '1%',
                "render": function(data, type, row, meta) {
                    var btn = '';
                    <?php if ($this->ion_auth->in_group('qa')) { ?>

                        if (row.status == 3) {
                            btn = '<button title="edit" class="btn btn-warning btn-sm btn-proses-qa">Edit</button>';
                        } else if (row.status <= 3) {
                            btn = '<button title="proses" class="btn btn-primary btn-sm btn-proses-qa">Proses</button>';
                        }
                    <?php } elseif ($this->ion_auth->in_group('operator')) { ?>
                        if (row.status == 2) {
                            btn = '<button title="edit" class="btn btn-warning btn-sm btn-proses-operator">Edit</button>';
                        } else if (row.status <= 1) {
                            btn = '<button title="proses" class="btn btn-primary btn-sm btn-proses-operator">Proses</button>';
                        }

                    <?php } elseif ($this->ion_auth->is_admin()) { ?>
                        btn = '<button title="hapus" class="btn btn-danger btn-sm btn-hapus">Hapus</button>';
                    <?php } ?>
                    return btn;
                }
            }, ],
        });
        $('#tabel_produksi tbody').on('click', '.details-control', function() {
            var tr = $(this).closest('tr');
            var row = siteTable.row(tr);
            var expbd = $(this).find('.expbd');
            // console.log(expbd.text());
            // expbd.addClass('fa-angle-left');
            if (row.child.isShown()) {

                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                expbd.removeClass('fa-angle-down');
                expbd.addClass('fa-angle-right');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
                expbd.removeClass('fa-angle-right');
                expbd.addClass('fa-angle-down');

            }
        });

        function format_(d) {
            
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding: 0.35 rem !important;border-top: 1 px solid #dee2e6 !important;">' +
                '<tr>' +
                '<th>Temp Aktual:</th>' +
                '<td>' + d.temp_aktual + '</td>' +
                '<th rowspan="2">PDI</th>' +
                '<th>PDI Pellet:</th>' +
                '<td>' + d.pdi_pellet + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Ampere:</th>' +
                '<td>' + d.ampere + '</td>' +
                '<th>Crumble:</th>' +
                '<td>' + d.crumble + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Speed Feeder</th>' +
                '<td>' + d.speed_feeder + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Dust Cooler:</th>' +
                '<td>' + d.dust_cooler + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.temp_aktual + '</td>' +
                '<td>' + d.ampere + '</td>' +
                '<td>' + d.speed_feeder + '</td>' +
                '<td>' + d.dust_cooler + '</td>' +
                '<td>' + d.pdi_pellet + '</td>' +
                '<td>' + d.crumble + '</td>' +
                '<td>' + d.sz8_sample1 + '</td>' +
                '<td>' + d.sz8_sample2 + '</td>' +
                '<td>' + d.sz14_sample2 + '</td>' +
                '<td>' + d.sz14_sample2 + '</td>' +
                '<td>' + d.keterangan + '</td>' +
                '</tr>' +
                '</table>';
        }

        function format(d) {
            // `d` is the original data object for the row
            return '<table cellpadding="5" class="sub-table table-bordered" width="100%" cellspacing="0" border="0" style="padding-left: 44px;">' +
                '<thead  class="bg-secondary" >' +
                '<tr>' +
                '<th rowspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Temp Aktual</th>' +
                '<th rowspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Ampere</th>' +
                '<th rowspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Speed Feeder</th>' +
                '<th rowspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Dust Cooler</th>' +
                '<th colspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">PDI</th>' +
                '<th colspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Tertahan di SIZING 8</th>' +
                '<th colspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Tertahan di SIZING 14</th>' +
                '<th rowspan="2" style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Keterangan</th>' +
                '</tr>' +
                '<tr>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">PDI Pellet</th>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Crumble</th>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Sample 1</th>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Sample 2</th>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Sample 1</th>' +
                '<th style="padding: 0.35rem !important;border-top: 1px solid #dee2e6 !important;">Sample 2</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>' + d.temp_aktual + ':</td>' +
                '<td>' + d.ampere + '</td>' +
                '<td>' + d.speed_feeder + '</td>' +
                '<td>' + d.dust_cooler + '</td>' +
                '<td>' + d.pdi_pellet + '</td>' +
                '<td>' + d.crumble + '</td>' +
                '<td>' + d.sz8_sample1 + '</td>' +
                '<td>' + d.sz8_sample2 + '</td>' +
                '<td>' + d.sz14_sample2 + '</td>' +
                '<td>' + d.sz14_sample2 + '</td>' +
                '<td>' + d.keterangan + '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>';
        }
        $('#tabel_produksi tbody').on('click', '.btn-proses-qa', function() {
            <?php if ($this->ion_auth->in_group('qa')) { ?>
                var datas = siteTable.row($(this).parents('tr')).data();
                console.log(datas);
                //Data produksi
                $('#modal-proses-qa').modal('show');
                $('#proses_no_input').val(datas.no_input);
                $('#proses_tanggal').val(datas.tgl_produksi);
                $('#proses_shift').val(datas.shift);
                $('#proses_nama_pakan').val(datas.nama_pakan);
                $('#proses_kode_pakan').val(datas.kode_pakan);
                $('#proses_jenis_pakan').val(datas.jenis_pakan);
                $('#proses_fase_pakan').val(datas.fase_pakan);
                $('#proses_kode_batch').val(datas.kode_batch);
                $('#proses_formula').val(datas.formula);
                $('#proses_steam').val(datas.steam);
                $('#proses_retentioner').val(datas.retentioner);
                $('#proses_temp_panel').val(datas.temp_panel);
                $('#proses_no_pellet').val(datas.no_pellet);

                //Data produksi qa
                $('#proses_temp_aktual').val(datas.temp_aktual);
                $('#proses_ampere').val(datas.ampere);
                $('#proses_speed_feeder').val(datas.speed_feeder);
                $('#proses_dust_cooler').val(datas.dust_cooler);
                $('#proses_pdi_pellet').val(datas.pdi_pellet);
                $('#proses_crumble').val(datas.crumble);
                $('#proses_sz8_sampe1').val(datas.sz8_sample1);
                $('#proses_sz8_sampe2').val(datas.sz8_sample2);
                $('#proses_sz14_sampe1').val(datas.sz14_sample1);
                $('#proses_sz14_sampe2').val(datas.sz14_sample2);
                $('#proses_keterangan').val(datas.keterangan);
                check_qa()
            <?php } else { ?>
                swal.fire("!Opps ", "Anda tidak mempunyai akses", "error");

            <?php } ?>
        });

        $('#tabel_produksi tbody').on('click', '.btn-proses-operator', function() {
            <?php if ($this->ion_auth->in_group('operator')) { ?>
                var datas = siteTable.row($(this).parents('tr')).data();
                if (datas.status == 2) {
                    $('#title-operator').text('Edit Data Produksi');

                    $('#opertaor_steam').val(datas.steam);
                    $('#opertaor_retentioner').val(datas.retentioner);
                    $('#opertaor_temp_panel').val(datas.temp_panel);
                    $('#opertaor_no_pellet').val(datas.no_pellet);
                } else {
                    $('#title-operator').text('Input Data Produksi');
                    $('.input-operator').val('');
                }
                console.log(datas);
                //Data produksi
                $('#modal-proses-operator').modal('show');
                $('#operator_no_input').val(datas.no_input);
                $('#operator_tgl').val(datas.tgl_produksi);
                $('#operator_shift').val(datas.shift);
                $('#operator_nama_pakan').val(datas.nama_pakan);
                $('#operator_kode_pakan').val(datas.kode_pakan);
                $('#operator_jenis_pakan').val(datas.jenis_pakan);
                $('#operator_fase_pakan').val(datas.fase_pakan);
                $('#operator_kode_batch').val(datas.kode_batch);
                $('#operator_formula').val(datas.formula);
            <?php } else { ?>
                swal.fire("!Opps ", "Anda tidak mempunyai akses", "error");

            <?php } ?>
        });

        $('#tabel_produksi tbody').on('click', '.btn-hapus', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            // if (datas.status == 1) {
            <?php if ($this->ion_auth->is_admin()) { ?>
                swal
                    .fire({
                        title: "Perhatian",
                        html: "Hapus produksi  <b>[" + datas.no_input + "] " + datas.nama_pakan + "</b>?",
                        // type: "question",
                        icon: 'warning',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: "#e74c3c",
                        confirmButtonText: "Hapus",
                        cancelButtonText: "Tidak"
                    })
                    .then(function(result) {
                        if (result.value) {
                            $.ajax({
                                url: <?php base_url() ?> 'ews_rencana_produksi/delete',
                                method: "POST",
                                data: {
                                    no_input: datas.no_input
                                },
                                dataType: "json",
                                beforeSend: function() {
                                    swal.fire({
                                        title: 'Please Wait..!',
                                        text: 'Is working..',
                                        onOpen: function() {
                                            swal.showLoading()
                                        }
                                    })
                                },
                                success: function(data) {
                                    if (data.status) {
                                        swal.fire({
                                            icon: 'success',
                                            title: data.msg,
                                            showConfirmButton: false,
                                            timer: 2000
                                        });
                                        $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                                    } else {
                                        swal.hideLoading();
                                        swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                                    }
                                },
                                complete: function() {
                                    swal.hideLoading();
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    swal.hideLoading();
                                    swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                                }
                            })
                        } else {}
                    });
            <?php } else { ?>
                swal.fire("!Opps ", "Anda tidak mempunyai akses", "error");

            <?php } ?>
            // }
        });

        $('#modal-premix').on('hidden.bs.modal', function(e) {
            var table = $('#tabel_material_premix').DataTable();
            table.clear();
            table.draw();
        })
    }


    $('#tabel_rencana_produksi_filter').keyup(function() {
        $('#tabel_rencana_produksi').DataTable().search(
            $(this).val()
        ).draw();

    });


    $('#fml_nm').on('change', function() {
        if ($('#total_prosen').val() > 0 && $(this).val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
    });

    $('#total_prosen').keyup(function() {
        var tot_prosen = $(this).val()
        if (tot_prosen > 0 && $('#fml_nm').val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
        var tot = 0;
        $('#tabel_material_formula').DataTable().$('.prosen').each(function(index, Obj) {
            var value = parseFloat($(this).text().replace(',', ''));
            var data = $('#tabel_material_formula').DataTable().row(index).data();
            var total = (data.prosen / 100) * tot_prosen;
            var $ThisCheck = $(this).parents('tr').find('.prosen');
            var result = "<input type='hidden' name='dosing[" + index + "][qty_prosen]' value='" + data.prosen + "'><input type='hidden' name='dosing[" + index + "][sequence]' value='" + data.seq + "'><input type='hidden' name='dosing[" + index + "][material_nm]' value='" + data.material_nm + "'><input type='hidden' name='dosing[" + index + "][material_kd]' value='" + data.material + "'><input type='hidden' name='dosing[" + index + "][qty_material]' value=" + total.toFixed(2) + ">" + total.toFixed(2);
            $ThisCheck.html(result);
            if (!isNaN(total)) tot += total;
        });
    });

    function CalcFooter() {
        // var amtPage = 0;
        var amtTotal = 0;
        var difTotal = 0;

        var Sum = 0;

        $('#tabel_material_premix_proses').DataTable().$('.paynow').each(function(index, Obj) {
            var value = parseFloat($(this).val().replace(',', ''));

            if (!isNaN(value)) amtTotal += value;
        });

        $('#tabel_material_premix_proses').DataTable().$('.dif').each(function(index, Obj) {

            var value = parseFloat($(this).text().replace(',', ''));
            if (!isNaN(value)) difTotal += value;
        });
        $('#total_timbang').text(
            amtTotal.toFixed(2)
        );

        $('#input_timbang').val(
            amtTotal.toFixed(2)
        );

        $('#total_dif').text(
            difTotal.toFixed(2)
        );
    }

    function check_qa() {
        var isValid = true;
        $(".input-qa").each(function() {
            var element = $(this);
            // console.log('data',element.val());
            if (element.val() == "" || element.val() == 0) {
                isValid = false;
            }
        });
        $('#status_qa').val(isValid);

        console.log($('#status_qa').val());
    }
    $('.input-qa').on('keyup', function() {
        check_qa();
    })

    function cek_input() {
        // console.log(isValid);
    }
    $('#form_proses').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_proses_qa',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnProcess').attr('disabled', true);

            },
            success: function(data) {

                $('#btnProcess').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-proses-qa").modal("hide");
                } else {
                    showToastError(data.msg);
                };

            }
        });
    });

    $('#form_proses_operator').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_proses_operator',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnOperator').attr('disabled', true);

            },
            success: function(data) {

                $('#btnOperator').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-proses-operator").modal("hide");
                } else {
                    showToastError(data.msg);
                };

            }
        });
    });


    $('#post_produksi').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_produksi',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnTambah').attr('disabled', true);
                swal.fire({
                    title: 'Tunggu..!',
                    text: 'Memproses..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                $('#btnTambah').attr('disabled', false);
                if (data.status) {
                    swal.hideLoading();

                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };
                $('#modal-data-produksi').modal('hide');
            },
            error: function() {
                $('#btnTambah').attr('disabled', false);
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
            }
        });
    });
</script>