<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Dashboard_model extends CI_Model
{
    public function get_daily_perawatan($category){
        $this->db->select('COUNT(*) total')
        ->from('d_daily_report a')
        ->where('a.category',$category)
        ->where('a.active',1)
        ->where('tanggal BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');
        return $this->db->get()->row();
    }
    
    public function get_work_order($id){
        $this->db->select('COUNT(*) total')
        ->from('d_work_order a')
        ->where('a.id_status',$id)
        ->where('tanggal BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');
        return $this->db->get()->row();
    }
    
    public function get_daily_report($unit_id,$d){
        $date = $d.'-01';
        $prev_date = date("Y-m-d", strtotime("-1 month",strtotime($date)));
        
        $enddate = $d.'-31';
        $p_enddate = date("Y-m", strtotime("-1 month",strtotime($d)));
        $prev_enddate = $p_enddate.'-31';
        $query = "SELECT 
         sum(case when a.category = 1 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) perawatan,
         sum(case when a.category = 2 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) perbaikan,
         sum(case when a.category = 3 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) project,
         b.day, b.hari
         FROM d_daily_report a
         RIGHT JOIN (
         SELECT '$date' + INTERVAL t.n - 1 DAY day, t.n hari
         FROM tally t
         WHERE t.n <= DATEDIFF(LAST_DAY('$date'), '$date') + 1) b on a.tanggal = b.day
         GROUP BY b.day
         ORDER BY b.hari";
         $data['grafik'] = $this->db->query($query)->result();
         
         $timestamp = strtotime($date);
         
         $this->db->select("SUM(case when a.category = 1 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) perawatan,
         SUM(case when a.category = 2 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) perbaikan,
         SUM(case when a.category = 3 and a.id_lokasi_unit = $unit_id and a.active = 1 then 1 else 0 end) project")
         ->from('d_daily_report a')
         ->where('MONTH(a.tanggal)',date("m", $timestamp))
         ->where('YEAR(a.tanggal)',date("Y", $timestamp));
         $data['sum'] = $this->db->get()->result();
         
         $this->db->select("SUM(case when a.category = 1 and a.active = 1 then 1 else 0 end) perawatan,
         SUM(case when a.category = 2 and a.active = 1 then 1 else 0 end) perbaikan,
         SUM(case when a.category = 3 and a.active = 1 then 1 else 0 end) project")
         ->from('d_daily_report a')
         ->join('m_unit b','a.id_lokasi_unit = b.unit_id')
         ->where_in('b.kategori',[1, 3])
         ->where('MONTH(a.tanggal)',date("m", $timestamp))
         ->where('YEAR(a.tanggal)',date("Y", $timestamp));
         $data['total'] = $this->db->get()->result();
         
        
        //  $this->db->select("count(*) sisa")
        //  ->from('d_task')
        //  ->where('progres <',100)
        //  ->where('created_date <=',$prev_enddate);
        //  $data['task_total_prev'] = $this->db->get()->row();
         
         
        //  $this->db->select("count(*) total")
        //  ->from('d_task a')
        //  ->where('created_date >=',$date)
        //  ->where('created_date <=',$enddate);
        //  $data['task_total'] = $this->db->get()->row()->total + $data['task_total_prev']->sisa;
         $this->db->select("count(*) sisa")
         ->from('d_task')
         ->where('progres <',100)
         ->where('active',1)
         ->where('updated_date <=',$enddate);
        //  ->where('updated_date >=',$date);
         $data['task_sisa'] = $this->db->get()->row();
         
         $this->db->select("count(*) selesai")
         ->from('d_task')
         ->where('progres >=',100)
         ->where('active',1)
         ->where('updated_date <=',$enddate)
         ->where('updated_date >=',$date);
         $data['task_selesai'] = $this->db->get()->row();
         
         $data['task_total'] = $data['task_selesai']->selesai + $data['task_sisa']->sisa;
         $data['persen'] = intVal($data['task_selesai']->selesai / $data['task_total'] * 100) .'%';
         return $data;
    }
    
}
