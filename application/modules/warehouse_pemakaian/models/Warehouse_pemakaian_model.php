<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Warehouse_pemakaian_model extends CI_Model
{

	public function get_material()
	{
		$this->db->select("id, material_nm")
			->order_by("material_nm", "ASC");
		$query = $this->db->get('m_material');
		return $query;
	}

	public function get_pemakaian()
	{
		$query = $this->db->select("b.created_date,a.material_kode,a.material_nm,b.qty,c.id_transaction")
			->from('m_material a')
			->join('t_riwayat_stok b', 'b.id_material = a.material_kode')
			->join('t_rencana_produksi c', 'c.id_transaction = b.id_transaction')
			->where('b.active', 1)
			->order_by("b.created_date", "DESC")->get();
		return $query;
	}
}
