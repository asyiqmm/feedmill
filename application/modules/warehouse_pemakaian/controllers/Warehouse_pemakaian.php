<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Warehouse_pemakaian extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('warehouse_pemakaian_model');
		$this->load->library('form_validation');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Pamakaian | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 2;
			$data['child_active'] = 5;
			$data['grandchild_active'] = null;


			$data['material'] = $this->warehouse_pemakaian_model->get_material()->result();
			$this->_render('warehouse_pemakaian_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}



	public function get_pemakaian()
	{
		$datas = $this->warehouse_pemakaian_model->get_pemakaian();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);

		echo json_encode($obj);
	}
}
