<!-- Main Sidebar Container -->
<?php
require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
require_once(APPPATH . "libraries/phpqrcode/qrconfig.php");
?>
<style>
	@media print {
		body * {
			visibility: hidden;
		}

		#section-to-print,
		#section-to-print * {
			visibility: visible;
		}

		#section-to-print {
			position: absolute;
			left: 0;
			top: 0;
			size: auto;
			margin: 0;
			padding: 0;
			height: 50mm;
			width: 58mm;
			display: flex;
			height: 100%;
		}
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">

		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="container-fluid">

			<div class='card'>
				<div class='card-header'>
					<h4 class='card-title'>Data Bin</h4>
					<div class="card-tools">
					</div>
				</div>

				<div class='card-body table-responsive p-3'>

					<table id='tabel_getdtmaterial' class='table table-bordered table-hover' style='width:100%'>
						<thead class="bg-primary">
							<tr>
								<th>No Bin</th>
								<th>Barcode</th>
								<th>Jenis Material</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

		</div>

		<!-- /.card -->

		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Bin</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_edit_bin" enctype="multipart/form-data" class="form-horizontal">
				<input type="hidden" name="id" id="id_bin">
				<div class="modal-body ">
					<div class="form-group">
						<label for="">No Bin :</label>
						<input type="text" class="form-control" name="bin_no" id="ebinno">
					</div>
					<!-- <div class="form-group">
						<label for="">Barcode :</label>
						<input type="text" class="form-control" name="bin_code" id="ebarcode">
					</div> -->
					<div class="form-group">
						<label for="">Jenis Material :</label>
						<select name="bin_material" id="ematerial" class="select2bs4" placeholder="pilih material">
							<option value=""></option>
							<?php foreach ($material as $u) {
								echo '<option value="' . $u->id . '">' . $u->material_nm . '</option>';
							} ?>
						</select>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					<button type="Submit" value="Submit" id="btnProccess" class="btn btn-primary">Ubah</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Bin </h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_add_bin" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body ">
					<div class="form-group">
						<label for="">No Bin :</label>
						<input type="text" class="form-control" required name="bin_no" placeholder="nomor bin">
					</div>
					<!-- <div class="form-group">
						<label for="">Barcode :</label>
						<input type="text" class="form-control" required name="bin_code" placeholder="barcode">
					</div> -->
					<div class="form-group">
						<label for="">Untuk Material :</label>
						<select name="bin_material" id="bin_material" class="form-control" placeholder="pilih material">
							<?php foreach ($material as $u) {
								echo '<option value="' . $u->id . '⁑' . $u->material_kode . '⁑' . $u->material_nm . '">' . $u->material_nm . '</option>';
							} ?>
						</select>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					<button type="Submit" value="Submit" id="btnAdd" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-print">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Print Code</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_add_bin" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body ">
					<div class="form-group">
						<label for="">No Bin :</label>
						<input type="text" class="form-control" name="bin_no" id="print_bin" placeholder="nomor bin" readonly>
					</div>

					<div class="form-group">
						<label for="">Material :</label>
						<input type="text" id="print_material" class="form-control" readonly>
					</div>
					<div id="section-to-print" media="print">
						<div class="form-group text-center">
							<img height="110px" id="qrcode" src="" />
							<br>
							<strong for="" id="print_kode"></strong>
							<br>
							Bin No : <strong for="" id="no_bin"></strong>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					<button type="button" onclick="print()" id="btn_print" target="_blank" class="btn btn-primary"> <i class="fas fa-print"></i> Print</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	$(function() {
		datatable_warehouse();
	});

	function print() {
		// window.location.href = "data_bin/print/" + $('#print_kode').text() + '/' + $('#no_bin').text();
		window.open(
			"data_bin/print/" + $('#print_kode').text() + '/' + $('#no_bin').text(),
			'_blank' // <- This is what makes it open in a new window.
		);
	}
	// $('#btn_print').on('click', function() {

	// });

	function datatable_warehouse() {

		var siteTable = $('#tabel_getdtmaterial').DataTable({
			"info": false,
			"destroy": true,
			
				buttons: [
					<?php if ($this->ion_auth->is_admin()) { ?>		
					{
					text: 'Tambah Bin',
					className: 'btn btn-primary btn-sm btn-add',
					action: function(e, dt, node, config) {
						$('.btn-add')
							.attr('data-toggle', 'modal')
							.attr('data-target', '#modal-add');
					}
				}, 
				<?php } ?>
			],
			

			"paging": false,
			"lengthChange": false,
			"scrollCollapse": true,
			"scrollY": "500px",
			"scrollX": true,
			"ajax": 'data_bin/get_dtbin',
			"columns": [

				{
					"data": 'bin_no',
					"className": 'details-control align-middle text-nowrap text-center',
					width: '1%'

				},
				{
					"data": 'bin_code',
					"className": 'details-control align-middle text-nowrap',
					width: '1%'
				},
				{
					"data": 'material_nm',
					"className": 'details-control align-middle text-nowrap',
					width: '50%'
				},
				{
					data: 'btn',
					className: 'details-control text-center align-middle text-nowrap',
					width: '1%',

				},


			],
			"columnDefs": [{
				"targets": '_all',
				"defaultContent": '0'
			}],
			"order": [
				[0, 'asc']
			],

			initComplete: function() {
				this.api().buttons().container()
					.appendTo($('.col-md-6:eq(0)', this.api().table().container()));
			}
		});
		$('#tabel_getdtmaterial tbody').on('click', '.btn-print', function() {
			var datas = siteTable.row($(this).parents('tr')).data();
			console.log(datas);
			$('#modal-print').modal('show');
			$('#qrcode').attr('src', 'data:image/jpeg;base64,' + datas.url);
			$('#print_bin').val(datas.bin_no);
			$('#print_material').val(datas.material_nm);
			$('#print_kode').text(datas.bin_code);
			$('#no_bin').text(datas.bin_no);
		});
		$('#tabel_getdtmaterial tbody').on('click', '.btn-edit', function() {
			var datas = siteTable.row($(this).parents('tr')).data();
			console.log(datas);
			$('#modal-edit').modal('show');
			$('#ebarcode').val(datas.bin_code);
			$('#ebinno').val(datas.bin_no);
			$('#id_bin').val(datas.id);
			$('#ematerial').val(datas.bin_material).trigger('change');
		});

		$('#tabel_getdtmaterial tbody').on('click', '.btn-delete', function() {
			var datas = siteTable.row($(this).parents('tr')).data();

			swal
				.fire({
					title: "Perhatian",
					html: "Hapus material  <b>[" + datas.material + "] " + datas.material_nm + "</b>?",
					// type: "question",
					showCloseButton: true,
					showCancelButton: true,
					confirmButtonColor: "#e74c3c",
					confirmButtonText: "Hapus",
					cancelButtonText: "Tidak"
				})
				.then(function(result) {
					if (result.value) {
						$.ajax({
							url: <?php base_url() ?> 'data_bin/delete',
							method: "POST",
							data: {
								id_material: datas.id
							},
							dataType: "json",
							success: function(data) {
								if (data.status == true) {
									showToast(data.msg);
									$("#tabel_getdtmaterial")
										.DataTable()
										.ajax.reload();
								} else {
									showToastWarning(data.msg);
								}
							}
						})
					} else {}
				});
		});
	}

	$('#form_add_bin').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: <?php base_url() ?> 'data_bin/add',
			type: "post",
			data: new FormData(this),
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			dataType: "JSON",
			beforeSend: function() {
				$('#btnAdd').attr('disabled', true)

			},
			success: function(data) {
				console.log('result', data);
				$('#btnAdd').attr('disabled', false);
				if (data.status) {
					showToast(data.msg);
					$("#tabel_getdtmaterial")
						.DataTable()
						.ajax.reload();
					$('#modal-add').modal('hide');
				} else {
					showToastError(data.msg);
				};


			}
		});
	});

	$('#form_edit_bin').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: <?php base_url() ?> 'data_bin/edit',
			type: "post",
			data: new FormData(this),
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			dataType: "JSON",
			beforeSend: function() {
				$('#btnAdd').attr('disabled', true)

			},
			success: function(data) {
				console.log('result', data);
				$('#btnAdd').attr('disabled', false);
				if (data.status) {
					showToast(data.msg);
					$("#tabel_getdtmaterial")
						.DataTable()
						.ajax.reload();
				} else {
					showToastError(data.msg);
				};
				$('#modal-edit').modal('hide');

			}
		});
	});
</script>