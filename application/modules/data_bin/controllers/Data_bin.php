<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_bin extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('data_bin_model');
		$this->load->library('form_validation');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Data Bin | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 6;
			$data['child_active'] = 25;
			$data['grandchild_active'] = 27;

			$data['unit'] = $this->data_bin_model->get_unit()->result();

			$data['material'] = $this->data_bin_model->get_material()->result();
			$this->_render('data_bin_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_dtbin()
	{
		require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
		require_once(APPPATH . "libraries/phpqrcode/qrconfig.php");

		$datas = $this->data_bin_model->get_dtbin();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		if ($this->ion_auth->is_admin()) {
			$btn = '<button title="print code" class="btn btn-outline-dark btn-sm btn-print mr-1"><i class="fas fa-qrcode"></i> Print</button><button title="edit" class="btn btn-outline-primary btn-sm btn-edit mr-1"><i class="far fa-edit"></i> Edit</button><button title="hapus" class="btn btn-outline-danger btn-sm btn-delete"><i class="far fa-trash-alt"></i> Hapus</button>';
		}else {
			$btn = '<button title="print code" class="btn btn-outline-dark btn-sm btn-print mr-1"><i class="fas fa-qrcode"></i> Print</button><button title="edit" class="btn btn-outline-primary btn-sm btn-edit mr-1"><i class="far fa-edit"></i> Edit</button>';
		}
		foreach ($data as $key => $value) {
			ob_start();
			QRcode::png($value['bin_code']);
			$result_qr_content_in_png = ob_get_contents();
			ob_end_clean();
			header("Content-type: text/html");
			$result_qr_content_in_base64 =  base64_encode($result_qr_content_in_png);
			$data[$key]['url'] = $result_qr_content_in_base64;
			$data[$key]['btn'] = $btn;
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function get_stock()
	{
		$datas = $this->warehouse_stok_model->get_stock();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function add()
	{

		$stud = explode("⁑", $_POST['bin_material']);
		$_POST['material'] = $stud[1];
		$_POST['material_nm'] = $stud[2];
		$_POST['bin_material'] = $stud[0];

		// $cek = $this->db->select('*')
		// 	->from('m_bin')
		// 	->where('bin_no', $_POST['bin_no'])
		// 	->active('active', 1)
		// 	->get();
		$cek = $this->data_bin_model->cek($_POST['bin_no']);
		if ($cek->num_rows() > 0) {
			$add = [
				'status' => false,
				'msg' => 'Nomor bin sudah digunakan',
			];
		} else {
			$add = $this->data_bin_model->save();
		}
		echo json_encode($add);
	}

	public function edit()
	{
		$edit = $this->data_bin_model->edit_bin();
		echo json_encode($edit);
	}

	public function delete()
	{
		$edit = $this->data_bin_model->delete_bin();
		echo json_encode($edit);
	}

	public function print($code, $bin)
	{
		$data['code'] = $code;
		$data['bin'] = $bin;
		$this->load->view('print', $data);
	}
}
