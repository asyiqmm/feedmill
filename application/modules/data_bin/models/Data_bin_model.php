<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Data_bin_model extends CI_Model
{

	public function get_dtbin()
	{
		$this->db->select("a.id,a.bin_material,LPAD(a.bin_no +0, 2, '0') bin_no, a.bin_code, b.material_nm,a.material")
			->from('m_bin a')
			->join('m_material b', 'b.id = a.bin_material')
			->where('a.active', 1)
			->order_by("a.bin_no", "ASC");
		$query = $this->db->get();
		return $query;
	}

	public function get_material()
	{
		'SELECT b.* FROM m_material b 
		LEFT JOIN m_bin a on b.id = a.bin_material
		WHERE a.id IS NULL
		OR a.active = 0';
		$this->db->select("b.*")
			->from('m_material b')
			->join('m_bin a', 'b.id = a.bin_material', 'LEFT')
			->where('a.id IS NULL')
			->or_where('a.active', 0)
			->order_by("a.material_nm", "ASC");
		$query = $this->db->get();
		// var_dump($this->db->last_query());die();
		return $query;
	}

	public function get_unit()
	{
		$this->db->select("id, unit_nm")
			->order_by("unit_nm", "ASC");
		$query = $this->db->get('m_unit_satuan');
		return $query;
	}

	public function get_stock()
	{
		$this->db->select("id, material_nm, material_kode, qty")
			->order_by("material_nm", "ASC");
		$query = $this->db->get('m_material');
		return $query;
	}
	public function cek($bin)
	{
		$cek = $this->db->select('*')
			->from('m_bin')
			->where('bin_no', $bin)
			->where('active', 1)
			->get();
		return $cek;
	}
	public function save()
	{
		$this->db->trans_begin();

		$date = date('Ymd');
		$fullname = $date . 'BIN' . mt_rand(10000, 99999);
		$_POST['bin_code'] = $fullname;
		"SELECT COUNT(slave) total,min(SLAVE) as slave,max(relay+1) relay FROM `m_bin`  
		group by slave
		having total != 8
		ORDER BY `m_bin`.`bin_no` ASC
		limit 1";

		$slave = $this->db
			// ->select('COUNT(slave) total,min(SLAVE) as slave,max(relay+1) relay')
			->select('COUNT(slave) total,min(SLAVE) as slave,max(relay+1) relay')
			->from('m_bin')
			->group_by('slave')
			->having('total != 8')
			->order_by('slave', 'ASC')
			->limit(1)->get();
		
		if ($slave->num_rows() == 0) {
			$slave2 = $this->db
				// ->select('COUNT(slave) total,min(SLAVE) as slave,max(relay+1) relay')
				->select('max(slave+1) slave, min(relay)')
				->from('m_bin')
				->group_by('slave')
				->order_by('slave', 'ASC')
				->limit(1)->get();
			if ($slave2->num_rows() == 0) {
				$_POST['slave'] = 1;
				$_POST['relay'] = 0;
			}else {
				// var_dump($slave2->row());
				// die();
				$_POST['slave'] = $slave2->row()->slave;
				$_POST['relay'] = $slave2->row()->relay+1;
			}
		} else {
			$_POST['slave'] = $slave->row()->slave;
			$_POST['relay'] = $slave->row()->relay;
		}


		$this->db->insert('m_bin', $_POST);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => 'Gagal menambahkan bin, coba sesaat lagi.',
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'Berhasil menambah bin'
			];
		}
	}

	public function edit_bin()
	{
		$this->db->trans_begin();
		$m_material = $this->db->where('id', $_POST['bin_material'])
			->get('m_material')->row();
		$_POST['material'] = $m_material->material_kode;
		$_POST['material_nm'] = $m_material->material_nm;
		$_POST['qty'] = 0;

		$this->db->where('id', $_POST['id']);
		$this->db->update('m_bin', $_POST);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'berhasil mengubah data'
			];
		}
	}

	public function delete_bin()
	{
		$this->db->trans_begin();
		// $this->db->set('active', 0);
		// $this->db->where('id', $_POST['id_material']);
		// $this->db->update('m_bin');
		$this->db->where('id', $_POST['id_material']);
		$this->db->delete('m_bin');
		// var_dump($this->db->last_query());die();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'berhasil menghapus data'
			];
		}
	}
}
