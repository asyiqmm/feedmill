<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Ews_limit_ews_model extends CI_Model
{
    public function get_data_limit(Type $var = null)
    {
        $query = $this->db->where('active', 1)
            ->get('m_limit_ews');
        return $query->result_array();
    }

    public function add(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->insert('m_limit_ews', $_POST);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil menambah data limit'
            ];
        }
    }

    public function edit(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->where('id', $_POST['id']);
        $this->db->update('m_limit_ews', $_POST);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil mengubah data limit'
            ];
        }
    }

    public function delete(Type $var = null)
    {
        $this->db->trans_begin();
        $this->db->where('id', $_POST['id']);
        $this->db->delete('m_limit_ews');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil menghapus data limit'
            ];
        }
    }
}
