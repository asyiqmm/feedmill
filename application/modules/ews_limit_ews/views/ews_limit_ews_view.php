<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Limit EWS</h3>
                        </div>
                        <!-- /.card-header -->
                        <style>
                        </style>
                        <div class="card-body">
                            <table id="tabel_limit" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>JENIS</th>
                                        <th>FASE</th>
                                        <th>MIN. TEMP AKTUAL</th>
                                        <th>MAX AMPERE</th>
                                        <th>MAX DUST COOLER</th>
                                        <th>STD. PDI</th>
                                        <th>MAX TEPUNGAN</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Limit </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_add_limit_ews" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="input_jenis">Jenis:</label>
                            <input class="form-control" type="text" required name="jenis" id="input_jenis">
                        </div>
                        <div class="col-md-6">
                            <label for="input_fase">Fase:</label>
                            <input class="form-control" type="text" required name="fase" id="input_fase">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="input_min_temp_aktual">Min. Temp Aktual:</label>
                            <input type="text" required class="form-control qtytimbang" name="min_temp_aktual" id="input_min_temp_aktual">
                        </div>
                        <div class="col-md-4">
                            <label for="input_max_ampere">Max Ampere:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_ampere" id="input_max_ampere">
                        </div>
                        <div class="col-md-4">
                            <label for="input_max_dust_cooler">Max Dust Cooler:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_dust_cooler" id="input_max_dust_cooler">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="input_std_pdi">STD PDI:</label>
                            <input type="text" required class="form-control qtytimbang" name="std_pdi" id="input_std_pdi">
                        </div>
                        <div class="col-md-6">
                            <label for="input_max_tepungan">Max Tepungan:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_tepungan" id="input_max_tepungan">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnAdd" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Limit </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_edit_limit_ews" enctype="multipart/form-data" class="form-horizontal">
                <input type="hidden" name="id" id="edit_id">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="edit_jenis">Jenis:</label>
                            <input class="form-control" type="text" required name="jenis" id="edit_jenis">
                        </div>
                        <div class="col-md-6">
                            <label for="edit_fase">Fase:</label>
                            <input class="form-control" type="text" required name="fase" id="edit_fase">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="edit_min_temp_aktual">Min. Temp Aktual:</label>
                            <input type="text" required class="form-control qtytimbang" name="min_temp_aktual" id="edit_min_temp_aktual">
                        </div>
                        <div class="col-md-4">
                            <label for="edit_max_ampere">Max Ampere:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_ampere" id="edit_max_ampere">
                        </div>
                        <div class="col-md-4">
                            <label for="edit_max_dust_cooler">Max Dust Cooler:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_dust_cooler" id="edit_max_dust_cooler">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="edit_std_pdi">STD PDI:</label>
                            <input type="text" required class="form-control qtytimbang" name="std_pdi" id="edit_std_pdi">
                        </div>
                        <div class="col-md-6">
                            <label for="edit_max_tepungan">Max Tepungan:</label>
                            <input type="text" required class="form-control qtytimbang" name="max_tepungan" id="edit_max_tepungan">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnEdit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(function() {
        // datatable_premix();
        // list_formula();
        $('.tgl').datetimepicker({
            format: 'DD/MM/YYYY',
            autoclose: true,
            // todayHighlight: true,
            minuteStepping: 1, //set the minute stepping

            // minDate:'1900/1/1',               //set a minimum date  <---- HERE 
            maxDate: moment(),
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.qtytimbang').inputmask('decimal', {
            allowMinus: false,
            integerDigits: 9,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
        });
        datatable_limit();
    });

    function datatable_limit() {
        $('#tabel_limit').DataTable().clear();
        // $('#daily_report_table_body').DataTable().destroy();
        $('#tabel_limit').empty();
        var siteTable = $('#tabel_limit').DataTable({
            "ajax": 'ews_limit_ews/get_data_limit',
            "info": false,
            "destroy": true,
            "order": [
                [0, "asc"]
            ],
            buttons: [{
                text: 'Tambah Data Limit',
                className: 'btn btn-primary btn-sm btn-add',
                action: function(e, dt, node, config) {
                    $('.btn-add')
                        .attr('data-toggle', 'modal')
                        .attr('data-target', '#modal-add');
                }
            }, ],
            "paging": false,
            "lengthChange": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                data: 'jenis',
                className: 'details-control align-middle text-center text-nowrap',
                width: '10%',
            }, {
                data: 'fase',
                className: 'details-control align-middle text-center text-nowrap',
                width: '10%',
            }, {
                data: 'min_temp_aktual',
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
            }, {
                data: 'max_ampere',
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
            }, {
                data: 'max_dust_cooler',
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
            }, {
                data: 'std_pdi',
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
            }, {
                data: 'max_tepungan',
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
            }, {
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
                "render": function(data, type, row, meta) {
                    var btn = '<button class="btn btn-sm btn-outline-warning btn-edit" title="edit">Edit</button>\n<button class="btn btn-sm btn-outline-danger btn-hapus" title="hapus">Hapus</button>';
                    return btn;
                }
            }],
        });


        $('#tabel_limit tbody').on('click', '.btn-edit', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            $('#modal-edit').modal('show');
            $('#edit_id').val(datas.id);
            $('#edit_jenis').val(datas.jenis);
            $('#edit_fase').val(datas.fase);
            $('#edit_min_temp_aktual').val(datas.min_temp_aktual);
            $('#edit_max_ampere').val(datas.max_ampere);
            $('#edit_max_dust_cooler').val(datas.max_dust_cooler);
            $('#edit_std_pdi').val(datas.std_pdi);
            $('#edit_max_tepungan').val(datas.max_tepungan);
        });

        $('#tabel_limit tbody').on('click', '.btn-hapus', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            console.log(datas);
            swal
                .fire({
                    title: "Perhatian",
                    html: "Hapus Data Limit?",
                    // type: "question",
                    icon: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#e74c3c",
                    confirmButtonText: "Hapus",
                    cancelButtonText: "Tidak"
                })
                .then(function(result) {
                    if (result.value) {
                        $.ajax({
                            url: <?php base_url() ?> 'ews_limit_ews/delete',
                            method: "POST",
                            data: {
                                id: datas.id
                            },
                            dataType: "json",
                            beforeSend: function() {
                                swal.fire({
                                    title: 'Please Wait..!',
                                    text: 'Is working..',
                                    onOpen: function() {
                                        swal.showLoading()
                                    }
                                })
                            },
                            success: function(data) {
                                if (data.status) {
                                    swal.fire({
                                        icon: 'success',
                                        title: data.msg,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                                } else {
                                    swal.hideLoading();
                                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                                }
                            },
                            complete: function() {
                                swal.hideLoading();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                swal.hideLoading();
                                swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                            }
                        })
                    } else {}
                });
        });

        $('#modal-premix').on('hidden.bs.modal', function(e) {
            var table = $('#tabel_material_premix').DataTable();
            table.clear();
            table.draw();
        })
    }

    $('#fml_nm').on('change', function() {
        $('#fml_name').val($('option:selected', this).text());
        var nm = $('option:selected', this).attr('fml_no');
        var item = $('option:selected', this).attr('item');
        $('#fml_no').val(nm);
        $('#item').val(item);
        var table = $('#tabel_material_formula').DataTable();
        table.clear();
        table.draw();
        table = $('#tabel_material_formula').DataTable({
            "oLanguage": {
                "sEmptyTable": "Data Kosong..."
            },
            "ajax": 'premix_rencana_produksi/get_data_formula?formula=' + $(this).val(),
            "paging": false,
            "lengthChange": false,
            "destroy": true,
            searching: false,
            "info": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            // responsive: true,
            ordering: false,
            initComplete: function(row, datas) {

                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));

            },
            // "fnDrawCallback": function(oSettings) {
            //     $($.fn.dataTable.tables(true)).DataTable()
            //         .columns.adjust();
            // },
            // "footerCallback": function(row, data, start, end, display) {

            // },
            columns: [{
                    data: 'seq',
                    className: 'details-control align-middle text-nowrap text-center',
                    width: '1%',
                    orderable: false
                },

                {
                    data: 'material_name',
                    className: 'details-control align-middle text-nowrap',
                    width: '15%',
                    orderable: false

                },
                {
                    data: 'prosen',
                    className: 'text-center details-control align-middle prosen',
                    width: '15%',
                    orderable: false,
                    "render": function(data, type, row, meta) {
                        var total = (row.prosen / 100) * $('#total_prosen').val();
                        var result = "<input type='hidden' name='dosing[" + meta.row + "][qty_prosen]' value='" + row.prosen + "'><input type='hidden' name='dosing[" + meta.row + "][sequence]' value='" + row.seq + "'><input type='hidden' name='dosing[" + meta.row + "][material_nm]' value='" + row.material_nm + "'><input type='hidden' name='dosing[" + meta.row + "][material_kd]' value='" + row.material + "'><input type='hidden' name='dosing[" + meta.row + "][qty_material]' value=" + total.toFixed(2) + ">" + total.toFixed(2);
                        return result;
                        // return '<input class="qtytimbang paynow" title="double click to type" type="text" size="5"  name="proses[' + meta.row + '][qty]" value="' + row.qty_jmltimbang + '" readonly="true" ondblclick="this.readOnly=false">  <input type="hidden" name="proses[' + meta.row + '][id_produksi]" value="' + datas.id_produksi + '"> <input type="hidden" name="proses[' + meta.row + '][id_material]" value="' + row.id_material + '">';

                    }
                },
            ],
        });
    })

    $('#tabel_rencana_produksi_filter').keyup(function() {
        $('#tabel_rencana_produksi').DataTable().search(
            $(this).val()
        ).draw();

    });


    $('#fml_nm').on('change', function() {
        if ($('#total_prosen').val() > 0 && $(this).val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
    });

    $('#total_prosen').keyup(function() {
        var tot_prosen = $(this).val()
        if (tot_prosen > 0 && $('#fml_nm').val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
        var tot = 0;
        $('#tabel_material_formula').DataTable().$('.prosen').each(function(index, Obj) {
            var value = parseFloat($(this).text().replace(',', ''));
            var data = $('#tabel_material_formula').DataTable().row(index).data();
            var total = (data.prosen / 100) * tot_prosen;
            var $ThisCheck = $(this).parents('tr').find('.prosen');
            var result = "<input type='hidden' name='dosing[" + index + "][qty_prosen]' value='" + data.prosen + "'><input type='hidden' name='dosing[" + index + "][sequence]' value='" + data.seq + "'><input type='hidden' name='dosing[" + index + "][material_nm]' value='" + data.material_nm + "'><input type='hidden' name='dosing[" + index + "][material_kd]' value='" + data.material + "'><input type='hidden' name='dosing[" + index + "][qty_material]' value=" + total.toFixed(2) + ">" + total.toFixed(2);
            $ThisCheck.html(result);
            if (!isNaN(total)) tot += total;
        });
    });

    function CalcFooter() {
        // var amtPage = 0;
        var amtTotal = 0;
        var difTotal = 0;

        var Sum = 0;

        $('#tabel_material_premix_proses').DataTable().$('.paynow').each(function(index, Obj) {
            var value = parseFloat($(this).val().replace(',', ''));

            if (!isNaN(value)) amtTotal += value;
        });

        $('#tabel_material_premix_proses').DataTable().$('.dif').each(function(index, Obj) {

            var value = parseFloat($(this).text().replace(',', ''));
            if (!isNaN(value)) difTotal += value;
        });
        $('#total_timbang').text(
            amtTotal.toFixed(2)
        );

        $('#input_timbang').val(
            amtTotal.toFixed(2)
        );

        $('#total_dif').text(
            difTotal.toFixed(2)
        );
    }

    $('#form_edit_limit_ews').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_limit_ews/edit',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnEdit').attr('disabled', true);

            },
            success: function(data) {
                $('#btnEdit').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-edit").modal("hide");
                } else {
                    showToastError(data.msg);
                };
            }
        });
    });

    $('#form_add_limit_ews').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_limit_ews/add',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnAdd').attr('disabled', true);
                swal.fire({
                    title: 'Tunggu..!',
                    text: 'Memproses..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                $('#btnAdd').attr('disabled', false);
                if (data.status) {
                    swal.hideLoading();

                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };
                $('#modal-add').modal('hide');
            },
            error: function() {
                $('#btnAdd').attr('disabled', false);
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
            }
        });
    });
</script>