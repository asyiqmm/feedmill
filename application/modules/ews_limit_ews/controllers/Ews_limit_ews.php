<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Ews_limit_ews extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('ews_limit_ews_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Data Limit EWS | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 28;
			$data['child_active'] = 30;
			$data['grandchild_active'] = 31;
			$this->_render('ews_limit_ews_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_data_limit()
	{
		$datas = $this->ews_limit_ews_model->get_data_limit();
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}

	public function add(Type $var = null)
	{
		$_POST['jenis'] = strtoupper($_POST['jenis']);
		$_POST['fase'] = strtoupper($_POST['fase']);
		$add = $this->ews_limit_ews_model->add();
		echo json_encode($add);
	}

	public function edit(Type $var = null)
	{
		$_POST['jenis'] = strtoupper($_POST['jenis']);
		$_POST['fase'] = strtoupper($_POST['fase']);
		$edit = $this->ews_limit_ews_model->edit();
		echo json_encode($edit);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->ews_limit_ews_model->delete();
		echo json_encode($delete);
	}
}
