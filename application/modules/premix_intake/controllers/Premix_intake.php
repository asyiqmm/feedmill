	<?php
	defined('BASEPATH') or exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');
	// require __DIR__ . '../../vendor/autoload.php';
	// use Curl\Curl;
	class Premix_intake extends CI_Controller
	{

		/**
		 * Index Page for this controller.
		 *
		 * Maps to the following URL
		 * 		http://example.com/index.php/welcome
		 *	- or -
		 * 		http://example.com/index.php/welcome/index
		 *	- or -
		 * Since this controller is set as the default controller in
		 * config/routes.php, it's displayed at http://example.com/
		 *
		 * So any other public methods not prefixed with an underscore will
		 * map to /index.php/welcome/<method_name>
		 * @see https://codeigniter.com/user_guide/general/urls.html
		 */
		public function __construct()
		{
			parent::__construct();
			$this->load->library('curl');
			$this->load->model('premix_intake_model');
		}

		private function _render($view, $data = array())
		{
			$data['title'] 	= "Intake | Feedmill - PT.Dinamika Megatama Citra";
			$data['users'] = $this->ion_auth->user()->row();
			$this->load->view('header', $data);
			$this->load->view('navbar');
			$this->load->view('sidebar', $data);
			$this->load->view($view, $data);
			$this->load->view('footer');
		}

		public function index()
		{
			$uri = &load_class('URI', 'core');
			if ($this->ion_auth->logged_in()) {
				$data['parent_active'] = 6;
				$data['child_active'] = 8;
				$data['grandchild_active'] = null;
				$this->premix_intake_model->refresh_bin();
				$this->_render('premix_intake_view', $data);
			} else {
				redirect('auth/login', 'refresh');
			}
		}

		public function get_list_stok()
		{
			$datas = $this->premix_intake_model->get_list_stok();
			$data =  json_decode(json_encode($datas->result()), TRUE);
			$obj = array("data" => $data);
			echo json_encode($obj);
		}

		public function get_material_by_id()
		{
			$datas = $this->premix_intake_model->get_material_by_id($_GET['code']);
			if ($datas->num_rows() > 0) {
				if ($datas->row()->sts_digunakan == 1) {
					$result = [
						'status'	=> false,
						'msg'		=> 'Kode material sudah pernah digunakan',
						'tipe'	 => false
					];
				} else {
					$result = [
						'status'	=> true,
						'data'		=> $datas->row(),

					];
				}
			} else {
				$result = [
					'status' => false,
					'msg' 	 => 'Kode tidak ditemukan',
					'tipe'	 => true
				];
			}
			// $data =  json_decode(json_encode($datas->result()), TRUE);
			// $obj = array("data" => $data);
			echo json_encode($result);
		}

		public function get_bin_by_id()
		{
			$datas = $this->premix_intake_model->get_bin_by_id();
			if ($datas->num_rows() > 0) {
				$result = [
					'status'	=> true,
					'data'		=> $datas->row()
				];
			} else {
				$result = [
					'status' => false
				];
			}
			echo json_encode($result);
		}

		public function get_list_riwayat()
		{
			$datas = $this->premix_intake_model->get_list_riwayat();
			$data =  json_decode(json_encode($datas->result()), TRUE);
			$obj = array("data" => $data);
			echo json_encode($obj);
		}

		public function form_intake()
		{
			$datas = $this->premix_intake_model->form_intake();
			echo json_encode($datas);
			// var_dump($datas);die();
		}
	}
