<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <form action="" method="post" id="form_intake" enctype="multipart/form-data" class="form-horizontal">

                <div class="row">
                    <div class="col-8">
                        <div class="card text-center">
                            <div class="card-header">
                                Informasi Material & Bin
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="50%" style="font-size: 1.5em;">Material</th>
                                        <th width="50%" style="font-size: 1.5em;">Bin</th>
                                    </tr>
                                    <tr>
                                        <td><textarea class="form-control form-control-sm  font-weight-bold text-center" rows="1" style="font-size: 2em;" placeholder="nomor kode material" type="text" name="code_material" id="code_material"></textarea></td>
                                        <td>
                                            <textarea class="form-control form-control-sm  font-weight-bold text-center" rows="1" style="font-size: 2em;" placeholder="nomor kode bin" type="text" name="code_bin" id="code_bin" disabled></textarea>
                                            <input type="hidden" name="txt_hide_bin" id="txt_hide_bin">
                                            <input type="hidden" name="id_material" id="id_material">
                                            <input type="hidden" name="id_bin" id="id_bin">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a class="text-danger" style="display: none;" id="msg_material">*Kode <b>material</b> tidak ditemukan</a></td>
                                        <td><a class="text-danger" style="display: none;" id="msg_bin">*Kode <b>bin</b> tidak ditemukan</a></td>
                                    </tr>
                                    <tr>
                                        <td id="td_material">
                                            <h4 id="txt_materil"></h4>
                                        </td>
                                        <td id="td_bin" class="align-middle">
                                            <h4 id="txt_bin"></h4>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="card text-center">
                            <div class="card-header">
                                Timbangan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <style>
                                            @media screen and (max-width : 1180px) {
                                                .timbangan {
                                                    font-size: 3.4em;
                                                }
                                            }

                                            @media screen and (min-width : 1181px) {
                                                .timbangan {
                                                    font-size: 5.04em;
                                                }
                                            }
                                        </style>
                                        <table width="100%" style="background-color: darkorange;">
                                            <tr>
                                                <td colspan="2" class="text-right"><span class="font-weight-bold align-top timbangan" id="nilai_timbangan">0.00</span> <span class="font-weight-bold align-top timbangan">KG</span></td>
                                                <!-- <td width="1%" class="text-left font-weight-bold align-top timbangan"> KG</td> -->
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center text-nowrap"><span class="text-light text-danger" style="display: none; font-size:1.6em" id="alert_timbangan">*Timbangan tidak terhubung</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan1" value="1" name="r1" checked>
                                                        <label for="timbangan1">
                                                            Timbangan 1
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan2" value="2" name="r1">
                                                        <label for="timbangan2">
                                                            Timbangan 2
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                        <!-- <h1 class="description-header font-weight-bold " >0.00</h1>
                                        <span class="description-text">Kg</span> -->
                                        <input type="hidden" name="qty_timbang" id="qty_timbang">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-2" hidden>
                        <div class="card text-center">
                            <div class="card-header">
                                Simpan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <button class="btn btn-primary btn-lg" hidden id="btnSimpan" type="submit" disabled>Simpan</button>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                </div>
            </form>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Stok Material</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_bin" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>No Bin</th>
                                        <th>Nama Material</th>
                                        <th>Unit</th>
                                        <th>Qty</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card collapsed-card card-info">
                        <div class="card-header" id="riwayat_collapse">
                            <h3 class="card-title">Data Riwayat Intake</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" onclick="datatable_riwayat()"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_riwayat_intake" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-info">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>No Bin</th>
                                        <th>Nama Material</th>
                                        <th>Qty</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<script>
    $('.pilihtimbangan').on('change', function() {
        // alert($(this).val());
    })

    document.onkeypress = dump;
    var word = "";

    function dump(e) {
        var unicode = e.keyCode ? e.keyCode : e.charCode;
        var actualkey = String.fromCharCode(unicode);
        word += actualkey;
        console.log('wadasda');
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 5 second for example
    var $input = $('#codes');

    //on keyup, start the countdown
    $input.on('keyup', function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //user is "finished typing," do something
    function doneTyping() {
        var cek = word.substring(8, 11);
        console.log(cek)
        var input = word.substring(0, 8);
        if (input === '37775814') {
            $('#btnSimpan').attr('disabled', false);
            $('#btnSimpan').trigger('click');
        } else {
            if ($('#code_material').val() == "" || $('#code_material').val() == null) {
                $('#code_material').focus();
            }
            // if (!sts_material) {
            //     if (cek == 'MAT') {
            //         if (($("#code_material").is(":focus")) == false || ) {
            //             $('#code_material').focus();
            //         }
            //     }
            // }
            // if (cek == 'MAT') {
            //     if (!($("#code_material").is(":focus"))) {
            //         $('#code_material').focus();
            //     }
            // } else if (cek == 'BIN') {
            //     $('#code_bin').focus();
            // }
            $('#btnSimpan').attr('disabled', true);
            $('#codes').focus();
            word = '';
        }
    }

    $(function() {
        $('#code_material').focus();
        datatable_intake();
        $('#code_material').keydown(function(e) {

            var code = $(this).val();

            // var key = e.which;
            if (e.keyCode == 13) // the enter key code
            {
                e.preventDefault();
                // alert("osk");
                $.ajax({
                    url: 'premix_intake/get_material_by_id?code=' + code,
                    method: "GET",
                    dataType: "json",
                    beforeSend: function() {
                        swal.fire({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            onOpen: function() {
                                swal.showLoading()
                            }
                        })
                    },
                    success: function(data) {
                        console.log(data.data);
                        if (data.status) {
                            weight = true;
                            get_timbang();
                            $('#code_bin').val('')
                            $('#txt_hide_bin').val(data.data.code_bin);
                            $('#id_material').val(data.data.id);
                            $('#id_bin').val(data.data.id_bin);

                            swal.fire({
                                icon: 'success',
                                title: 'kode material ditemukan',
                                showConfirmButton: false,
                                timer: 1000
                            });

                            setTimeout(focuses, 2000);
                            // showToast('kode material ditemukan');
                            $('#msg_material').hide();
                            $('#td_material').show(500);
                            $('#td_bin').show(500);
                            $('#txt_materil').text(data.data.material)
                            $('#txt_bin').text('BIN: ' + data.data.bin_no)
                        } else {

                            if (data.tipe) {
                                // showToastError(data.msg);
                                swal.fire({
                                    icon: 'error',
                                    title: data.msg,
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            } else {
                                swal.fire({
                                    icon: 'warning',
                                    title: data.msg,
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                                // showToastWarning(data.msg);
                            }
                            $('#code_bin').attr('disabled', true);
                            $('#msg_material').show();
                            $('#td_material').hide();
                            $('#td_bin').hide();
                            $('#code_material').val('');
                            weight = false;
                        }
                    },
                    error: function(data) {

                    }
                })
                return false;
            }
        });
    });

    function focuses(params) {
        $('#code_bin').attr('disabled', false);
        $('#code_bin').focus();
    }

    function outfocus(params) {
        $('#code_bin').blur();
    }
    $('#code_bin').keydown(function(e) {
        var cek = word.substring(8, 11);
        if (cek == 'MAT') {
            $('#code_material').val('').focus();
            $(this).val('');
        }
        var bin = $(this).val();
        var code = $('#code_material').val();
        if (e.keyCode == 13) // the enter key code
        {
            e.preventDefault();
            // alert("osk");
            $.ajax({
                url: 'premix_intake/get_bin_by_id?bin=' + bin + '&code=' + code,
                method: "GET",
                dataType: "json",
                beforeSend: function() {
                    swal.fire({
                        title: 'Please Wait..!',
                        text: 'Is working..',
                        onOpen: function() {
                            swal.showLoading()
                        }
                    })
                },
                success: function(data) {
                    console.log(data.status);
                    if (data.status) {
                        swal.fire({
                            icon: 'success',
                            title: 'Material dan Bin cocok!',
                            showConfirmButton: false,
                            timer: 1000
                        });
                        // showToast('Material dan Bin cocok!');
                        $('#msg_bin').hide();
                        $('#btnSimpan').attr('disabled', false);
                        //     $('#txt_hide_bin').val(data.data.code_bin);
                        //     $('#id_material').val(data.data.id);
                        //     $('#id_bin').val(data.data.id_bin);
                        //     $('#code_bin').attr('disabled', false).focus();
                        //     showToast('kode material ditemukan');
                        //     $('#msg_material').hide();
                        //     $('#td_material').show(500);
                        //     $('#td_bin').show(500);
                        //     $('#txt_materil').text(data.data.material)
                        //     $('#txt_bin').text('BIN: ' + data.data.bin_no)
                        // $('#code_bin').blur();
                        setTimeout(outfocus, 2000);
                    } else {
                        $('#code_bin').val('');

                        // showToastError('Tidak Cocok');
                        swal.fire({
                            icon: 'error',
                            title: 'Tidak Cocok',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        $('#msg_bin').show();

                        $('#btnSimpan').attr('disabled', true);

                        //     showToastError('kode material tidak ditemukan');
                        //     $('#code_bin').attr('disabled', true);
                        //     $('#msg_material').show();
                        //     $('#td_material').hide();
                        //     $('#td_bin').hide();
                        //     $('#code_material').val('')
                    }
                },
                error: function(data) {

                }
            })
            return false;
        }
        // var code = $(this).val();
        // var code_bin = $('#txt_hide_bin').val();
        // console.log(code_bin);
        // console.log(code);
        // console.log(code === code_bin);
        // if (e.keyCode == 13) // the enter key code
        // {
        //     e.preventDefault();
        //     if (code === code_bin) {
        //         showToast('Material dan Bin cocok!');
        //         $('#msg_bin').hide();
        //     } else {
        //         showToastError('Tidak Cocok');
        //         $('#msg_bin').show();
        //     }
        //     return false;
        // }
    });

    // $('#code_bin').on('keyup change', function(e) {
    //     var code = $(this).val();
    //     var code_bin = $('#txt_hide_bin').val();

    //     if (code === code_bin) {
    //         $('#msg_bin').hide();
    //         $('#btnSimpan').attr('disabled', false);
    //         showToast('Material dan Bin cocok!');
    //     } else {
    //         $('#btnSimpan').attr('disabled', true);
    //         $('#msg_bin').show();
    //     }

    // });

    var weight = true;

    function get_timbang() {
        // console.log('jalan');
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_makro/get_data_timbangan?timbangan=' + $('.pilihtimbangan:checked').val(),
            method: "GET",
            dataType: "json",
            success: function(data) {
                if (data.data[0].status == 'success') {
                    var kg = Number(data.data[0].random);
                    if (weight) {
                        setTimeout(get_timbang, 500);
                        console.log('Terhubung 0.5 detik');
                    }
                    $('#alert_timbangan').hide();
                    $('#nilai_timbangan').removeClass('text-danger');
                    $('#nilai_timbangan').text(kg.toFixed(2));
                    $('#qty_timbang').val(kg.toFixed(2));
                } else {
                    // if (weight) {
                    setTimeout(get_timbang, 5000);
                    console.log('tidak terhubung 5 detik');
                    // }
                    $('#alert_timbangan').show();
                    $('#nilai_timbangan').addClass('text-danger');
                    a = Number(0);
                    $('#nilai_timbangan').text(a.toFixed(2));
                    $('#qty_timbang').val(a.toFixed(2));
                }
                // if (data.status) {
                //     var kg = Number(data.data.ADC);
                //     if (weight) {
                //         setTimeout(get_timbang, 500);
                //         console.log('Terhubung 0.5 detik');
                //     }
                //     $('#alert_timbangan').hide();
                //     $('#nilai_timbangan').removeClass('text-danger');
                //     $('#nilai_timbangan').text(kg.toFixed(2));
                //     $('#qty_timbang').val(kg.toFixed(2));
                // } else {

                //     if (weight) {
                //         setTimeout(get_timbang, 5000);
                //         console.log('tidak terhubung 5 detik');
                //     }
                //     $('#alert_timbangan').show();
                //     $('#nilai_timbangan').addClass('text-danger');
                //     a = Number(0);
                //     $('#nilai_timbangan').text(a.toFixed(2));
                //     $('#qty_timbang').val(a.toFixed(2));
                // }
            },
            error: function(data) {
                // clearInterval(timeout);
                if (weight) {
                    setTimeout(get_timbang, 5000);
                    console.log('tidak terhubung 5 detik');
                }
                $('#alert_timbangan').show();
                $('#nilai_timbangan').addClass('text-danger');
                $('#btnSimpan').attr('disabled', true);
                a = Number(0);
                $('#nilai_timbangan').text(a.toFixed(2));
                $('#qty_timbang').val(a.toFixed(2));
                // console.log(data);
            }
        })
    }

    function datatable_riwayat() {
        var siteTable = $('#tabel_riwayat_intake').DataTable({
            "lengthChange": false,
            "ajax": 'premix_intake/get_list_riwayat',
            "paging": false,
            // "ordering": false,
            "info": false,
            "searching": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [0, "desc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'created_date',
                    className: 'details-control align-middle text-center text-nowrap',
                    width: '1%',
                },
                {
                    data: 'bin_no',
                    className: 'details-control align-middle text-center text-nowrap',
                    width: '5%',
                },
                {
                    data: 'material',
                    className: 'details-control',
                    width: '30%',
                },
                {
                    data: 'qty',
                    className: 'details-control text-right',
                    width: '5%',
                },
            ],
        });

    }

    function datatable_intake() {
        var siteTable = $('#tabel_bin').DataTable({
            "lengthChange": false,
            "ajax": 'premix_intake/get_list_stok',
            "paging": false,
            "ordering": false,
            "info": false,
            "searching": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "350px",
            "scrollX": true,
            "order": [
                [0, "desc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'bin_no',
                    className: 'details-control align-middle text-center text-nowrap',
                    width: '1%'
                },
                {
                    data: 'material',
                    className: 'details-control align-middle text-nowrap',
                    width: '30%'
                },
                {
                    defaultContent: 'KG',
                    className: 'details-control text-center',
                    width: '1%',
                },
                {
                    data: 'qty',
                    className: 'details-control text-right',
                    width: '5%'
                },
            ],
        });

    }

    $('#form_intake').on('submit', function(event) {
        $('#btnSimpan').attr('disabled', true);
        swal.fire({
            title: 'Tunggu..!',
            text: 'Memproses..',
            onOpen: function() {
                swal.showLoading()
            }
        })
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_intake/form_intake',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {

            },
            success: function(data) {
                $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                swal.hideLoading();
                if (data.status) {
                    showToast(data.msg);
                    weight = false;
                    $('#code_material').val('').focus();
                    $('#code_bin').val('');
                    $('#code_bin').attr('readonly', true);

                    $('#nilai_timbangan').text(0);
                    $('#qty_timbang').val('');

                    $('#td_material').hide();
                    $('#td_bin').hide();

                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);

                } else {
                    $('#btnSimpan').attr('disabled', true);
                    showToastError(data.msg);
                    swal.fire({
                        icon: 'error',
                        title: "Terjadi masalah, coba lagi nanti",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    // swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };

            },
            error: function() {
                swal.hideLoading();
                $('#btnSimpan').attr('disabled', true);
                swal.fire({
                    icon: 'error',
                    title: "Terjadi masalah, coba lagi nanti",
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        });
    });
</script>