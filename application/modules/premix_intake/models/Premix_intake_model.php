<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Premix_intake_model extends CI_Model
{
    public function get_list_stok()
    {
        'SELECT a.bin_no ,a.qty ,b.material_nm 
        from m_bin a
        join m_material b on b.id = a.bin_material 
        where a.active = 1';
        $this->db->select("LPAD(a.bin_no +0, 2, '0') bin_no,a.qty ,concat('[',b.material_kode ,'] ',b.material_nm) material")
            ->from('m_bin a')
            ->join('m_material b', 'b.id = a.bin_material')
            ->where('a.active', 1)
            ->order_by('a.bin_no');
        return $this->db->get();
    }

    public function get_material_by_id($code)
    {
        "SELECT concat('[',a.material_kode ,'] ',a.material_nm) material,b.codes , LPAD(c.bin_no +0, 2, '0') bin_no ,b.sts_digunakan
        FROM m_material a
        JOIN m_qrcodes b on b.material = a.material_kode
        WHERE b.codes = '202107120101'
        AND b.sts_digunakan = 0";

        $this->db->select("concat('[',a.material_kode ,'] ',a.material_nm) material,b.codes ,LPAD(c.bin_no +0, 2, '0') bin_no ,b.sts_digunakan")
            ->from('m_material a')
            ->join('m_qrcodes b', 'b.material = a.material_kode')
            ->join('m_bin c', 'c.bin_material = a.id', 'left')
            // ->join('')
            ->where('b.codes', $code);
        // ->where('b.sts_digunakan', 0);
        return $this->db->get();
    }

    public function get_bin_by_id()
    {
        "SELECT a.id aidi,* 
        FROM m_bin a
        JOIN m_material b on a.bin_material = b.id
        JOIN m_qrcodes c on c.material = b.material_kode
        WHERE c.codes = '202107160101'
        AND a.bin_code = 'BIN2021071680441'
        AND c.sts_digunakan = 0";
        $this->db->select("a.id aidi,a.*,b.*,c.*")
            ->from('m_bin a')
            ->join('m_material b', 'a.bin_material = b.id')
            ->join('m_qrcodes c', 'c.material = b.material_kode')
            ->where('c.codes', $_GET['code'])
            ->where('a.bin_code', $_GET['bin'])
            ->where('c.sts_digunakan', 0);
        $get = $this->db->get();
        // var_dump($this->db->last_query());die();
        if ($get->num_rows() > 0) {
            $this->db->set('bin_open', 1)
                ->where('id', $get->row()->aidi)
                ->update('m_bin');
        }
        return $get;
    }

    public function get_list_riwayat()
    {
        'SELECT a.created_date ,b.material_kode ,b.material_nm ,c.bin_no ,a.qty 
        from t_riwayat_intake a
        join m_material b on b.id = a.id_material 
        join m_bin c on c.id = a.id_bin 
        where a.active = 1';

        // $this->db->select("a.created_date ,concat('[',b.material_kode ,'] ',b.material_nm) material ,b.material_kode ,b.material_nm ,a.bin_no ,a.qty")
        $this->db->select("a.created_date ,concat('[',b.material_kode ,'] ',b.material_nm) material ,b.material_kode ,b.material_nm ,c.bin_no ,a.qty")
            ->from('t_riwayat_intake a')
            ->join('m_material b', 'b.id = a.id_material')
            ->join('m_bin c', 'c.material = b.material_kode')
            ->where('a.active', 1);
        return $this->db->get();
    }

    public function form_intake()
    {
        $this->db->trans_begin();
        // $this->db->select("a.bin_code,b.id id_material,a.id id_bin,a.bin_no,c.codes code_material")
        $this->db->select("d.berat_karung,a.bin_code,b.id id_material,a.bin_no,a.id id_bin,c.codes code_material")
            ->from('m_bin a')
            ->join('m_material b', 'a.bin_material = b.id')
            ->join('m_qrcodes c', 'c.material = b.material_kode')
            ->join('d_original_pack d', 'd.material = b.material_kode and d.active = 1', 'left')
            ->where('c.codes', $_POST['code_material'])
            ->where('a.bin_code', $_POST['code_bin'])
            ->where('c.sts_digunakan', 0);
        $data = $this->db->get();
        $get = $data->row();
        $this->db->set('bin_open', 0)
            ->where('id', $get->id_bin)
            ->update('m_bin');
        if ($data->num_rows() > 0) {
            $riwayat = array(
                'id_material'   => $get->id_material,
                'code_barang'   => $get->code_material,
                'qty'           => $_POST['qty_timbang'],
                'bin_no'        => $get->bin_no
                // 'id_bin'        => $get->id_bin
            );
            $this->db->insert('t_riwayat_intake', $riwayat);
            // var_dump($this->db->last_query());die();
            $this->db->set('sts_digunakan', 1)
                ->set('tgl_digunakan', date('Y-m-d H:i:s'))
                ->where('codes', $_POST['code_material'])
                ->update('m_qrcodes');
            // var_dump($data->row()->berat_karung);
            // die();
            $total = $_POST['qty_timbang'] - $data->row()->berat_karung;
            $q = "UPDATE m_bin a
            SET a.qty = a.qty + " . $total . "
            WHERE a.bin_code = '" . $_POST['code_bin'] . "'";
            $this->db->query($q);
            $status = true;
            $msg = 'Berhasil intake material';
        } else {
            $status = false;
            $msg = 'Barcode sudah pernah digunakan';
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => 'Error!'
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => $status,
                'msg' => $msg
            ];
        }
    }

    public function refresh_bin()
    {
        $this->db->set('bin_open', 0)
            ->update('m_bin');
    }
}
