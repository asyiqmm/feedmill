<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">

		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="container-fluid">

			<div class='card'>
				<div class='card-header'>
					<h4 class='card-title'>Data Material</h4>
					<div class="card-tools">
					</div>
				</div>

				<div class='card-body table-responsive p-3'>

					<table id='tabel_getdtmaterial' class='table table-bordered table-hover' style='width:100%'>
						<thead class="bg-primary">
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Nama Material</th>
								<th>Original Pack</th>
								<th>Unit</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

		</div>

		<!-- /.card -->

		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Material </h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_edit_material" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body ">
					<div class="form-group">
						<label for="">Kode Material :</label>
						<input type="text" readonly class="form-control" name="material" id="edit_material_kode">
					</div>
					<div class="form-group">
						<label for="">Nama Material :</label>
						<input type="text" readonly class="form-control" id="edit_material_nama">
					</div>
					<div class="form-group">
						<label for="">Original Pack :</label>
						<input type="text" class="form-control" name="original_pack" id="edit_original_pack">
					</div>
					<div class="form-group">
						<label for="">Unit :</label>
						<input type="text" readonly class="form-control" id="edit_unit">
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					<button type="Submit" value="Submit" id="btnEdit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Material </h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_add_material" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body ">
					<div class="form-group">
						<label for="material">Pilih Material:</label>
						<select required class="select2bs4" name="material_nm" id="material">
							<option></option>
						</select>
					</div>
					<div class="form-group">
						<label for="original_pack">Original Pack:</label>
						<input type="number" min=0 class="form-control" name="original_pack" id="original_pack">
					</div>
					<div class="form-group">
						<label for="kd_material">Kode Material:</label>
						<input type="text" readonly class="form-control" name="material_kode" id="kd_material">
					</div>
					<div class="form-group">
						<label for="unit_mateiral">Unit:</label>
						<input type="text" readonly class="form-control" name="unit" id="unit_mateiral">
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

					<button type="Submit" value="Submit" id="btnAdd" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	$(function() {
		datatable_warehouse();
		list_material();
	});

	function list_material() {
		'https://ismaf.mydmc.co.id/api/material';
		jQuery.ajax({
			type: "GET",
			url: "https://ismaf.mydmc.co.id/api/material", // the method we are calling
			dataType: "json",
			success: function(data) {
				var option = '<option></option>';
				for (let index = 0; index < data.data.length; index++) {
					option += '<option unit="'+data.data[index].unit+'" kd="'+data.data[index].material +'" value="' + data.data[index].name +'" >' + data.data[index].name + '</option>';
				}
				if (data.meta.code == 200) {
					showToast(data.meta.message);
					$('#material').html(option);
				} else {
					showToastWarning(data.meta.message);
				}

			},
			error: function(xhr, status, error) {
				showToastWarning('error')
			}

		});
	}

	$('#material').on('change',function(){
		var kd = $(this).find('option:selected').attr('kd');
		var unit = $(this).find('option:selected').attr('unit');
		$('#kd_material').val(kd);
		$('#unit_mateiral').val(unit);
	});

	function datatable_warehouse() {

		var siteTable = $('#tabel_getdtmaterial').DataTable({
			"info": false,
			"destroy": true,
			"order": [
				[1, "asc"]
			],
			buttons: [{
				text: 'Tambah Material',
				className: 'btn btn-primary btn-sm btn-add',
				action: function(e, dt, node, config) {
					$('.btn-add')
						.attr('data-toggle', 'modal')
						.attr('data-target', '#modal-add');
				}
			}, ],
			"paging": false,
			"lengthChange": false,
			"scrollCollapse": true,
			"scrollY": "500px",
			"scrollX": true,
			"ajax": 'data_material/get_dtmaterial',
			"columns": [{
					"data": '',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%'
				},
				{
					"data": 'material_kode',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%'
				},
				{
					"data": 'material_nm',
					"className": 'details-control align-middle text-nowrap',
					"width": '15%'
				},
				{
					"data": 'original_pack',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%'

				},

				{
					"data": 'unit',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%'
				},

				{
					data: null,
					defaultContent: '<button title="edit" class="btn btn-outline-primary btn-sm btn-edit mr-1">Edit</button><button title="hapus" class="btn btn-outline-danger btn-sm btn-delete">Hapus</button>',
					className: 'details-control text-center align-middle text-nowrap',
					width: '1%'
				},


			],
			"columnDefs": [{
				"targets": '_all',
				"defaultContent": '0'
			}],
			"order": [
				[2, 'desc']
			],

			initComplete: function() {
				this.api().buttons().container()
					.appendTo($('.col-md-6:eq(0)', this.api().table().container()));
			}
		});
		siteTable.on('order.dt search.dt', function() {
			siteTable.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function(cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
		$('#tabel_getdtmaterial tbody').on('click', '.btn-edit', function() {
			var datas = siteTable.row($(this).parents('tr')).data();
			console.log(datas);
			$('#modal-edit').modal('show');
			$('#edit_material_kode').val(datas.material_kode);
			$('#edit_material_nama').val(datas.material_nm);
			$('#edit_original_pack').val(datas.original_pack);
			$('#edit_unit').val(datas.unit);
		});

		$('#tabel_getdtmaterial tbody').on('click', '.btn-delete', function() {
			var datas = siteTable.row($(this).parents('tr')).data();

			swal
				.fire({
					title: "Perhatian",
					html: "Hapus material  <b>[" + datas.material_kode + "] " + datas.material_nm + "</b>?",
					// type: "question",
					showCloseButton: true,
					showCancelButton: true,
					confirmButtonColor: "#e74c3c",
					confirmButtonText: "Hapus",
					cancelButtonText: "Tidak"
				})
				.then(function(result) {
					if (result.value) {
						$.ajax({
							url: <?php base_url() ?> 'data_material/delete',
							method: "POST",
							data: {
								id_material: datas.id
							},
							dataType: "json",
							success: function(data) {
								if (data.status == true) {
									showToast(data.msg);
									$("#tabel_getdtmaterial")
										.DataTable()
										.ajax.reload();
								} else {
									showToastWarning(data.msg);
								}
							}
						})
					} else {}
				});
		});
	}

	$('#form_add_material').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: <?php base_url() ?> 'data_material/add',
			type: "post",
			data: new FormData(this),
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			dataType: "JSON",
			beforeSend: function() {
				$('#btnAdd').attr('disabled', true)

			},
			success: function(data) {
				console.log('result', data);
				$('#btnAdd').attr('disabled', false);
				if (data.status) {
					showToast(data.msg);
					$("#tabel_getdtmaterial")
						.DataTable()
						.ajax.reload();
				} else {
					showToastError(data.msg);
				};
				$('#modal-add').modal('hide');

			}
		});
	});

	$('#form_edit_material').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: <?php base_url() ?> 'data_material/edit',
			type: "post",
			data: new FormData(this),
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			dataType: "JSON",
			beforeSend: function() {
				$('#btnAdd').attr('disabled', true)

			},
			success: function(data) {
				console.log('result', data);
				$('#btnAdd').attr('disabled', false);
				if (data.status) {
					showToast(data.msg);
					$("#tabel_getdtmaterial")
						.DataTable()
						.ajax.reload();
				} else {
					showToastError(data.msg);
				};
				$('#modal-edit').modal('hide');

			}
		});
	});
</script>