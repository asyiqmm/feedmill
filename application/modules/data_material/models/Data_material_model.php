<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Data_material_model extends CI_Model
{

	public function get_dtmaterial()
	{
		$this->db->select("a.*,b.original_pack")
			->from('m_material a')
			->join('d_original_pack b', 'b.material = a.material_kode and b.active = 1', 'left')
			->where('a.active', 1)
			->order_by("a.material_nm", "ASC");
		$query = $this->db->get();
		return $query;
	}

	public function get_material()
	{
		$this->db->select("id, material_nm")
			->order_by("material_nm", "ASC");
		$query = $this->db->get('m_material');
		return $query;
	}

	public function get_unit()
	{
		$this->db->select("id, unit_nm")
			->order_by("unit_nm", "ASC");
		$query = $this->db->get('m_unit_satuan');
		return $query;
	}

	public function get_stock()
	{
		$this->db->select("id, material_nm, material_kode, qty")
			->order_by("material_nm", "ASC");
		$query = $this->db->get('m_material');
		return $query;
	}

	public function save()
	{
		$this->db->trans_begin();
		$this->db->where('material_kode', $_POST['material_kode']);
		$q = $this->db->get('m_material');
		if ($q->num_rows() > 0) {
			$this->db->set('material_nm', $_POST['material_nm']);
			if ($q->row()->active == 0) {
				$this->db->set('active', 1);
				$msg = 'Berhasil menambahkan data material baru';
			} else {
				$msg = 'Material sudah ada';
			}
			$this->db->set('unit', $_POST['unit']);
			$this->db->where('material_kode', $_POST['material_kode']);
			$this->db->update('m_material');
		} else {
			$this->db->set('material_nm', $_POST['material_nm']);
			$this->db->set('unit', $_POST['unit']);
			$this->db->set('material_kode', $_POST['material_kode']);
			$this->db->insert('m_material');
			$msg = 'Berhasil menambahkan data material baru';
		}
		if (!empty($_POST['original_pack'])) {
			$this->db->set('active', 0)
				->where('material', $_POST['material_kode'])
				->update('d_original_pack');
			$original_pack = array(
				'material' 		=> $_POST['material_kode'],
				'active' 		=> 1,
			);
			if (!empty($_POST['original_pack'])) {
				$original_pack['original_pack'] = $_POST['original_pack'];
			}
			$this->db->insert('d_original_pack', $original_pack);
		}
		// $this->db->insert('m_material', $_POST);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => $msg
			];
		}
	}

	public function edit_material()
	{
		$this->db->trans_begin();
		$this->db->set('active', 0)
			->where('material', $_POST['material'])
			->update('d_original_pack');
		$original_pack = array(
			'material' 		=> $_POST['material'],
			'active' 		=> 1,
		);
		$original_pack['original_pack'] = $_POST['original_pack'];
		$this->db->insert('d_original_pack', $original_pack);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'berhasil mengubah data'
			];
		}
	}

	public function delete_material()
	{
		$this->db->trans_begin();
		$this->db->set('active', 0);
		$this->db->where('id', $_POST['id_material']);
		$this->db->update('m_material');
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'berhasil menghapus data'
			];
		}
	}
}
