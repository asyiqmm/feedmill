<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Warehouse_stok_model extends CI_Model
{
	
	public function get_material()
		{
			$this->db->select("id, material_nm")
			->order_by("material_nm", "ASC")
			;
			$query=$this->db->get('m_material');
			return $query;
		}
	
	public function get_stock()
		{
			$this->db->select("id, material_nm, material_kode, qty, unit")
			->order_by("material_nm", "ASC")
			;
			$query=$this->db->get('m_material');
			return $query;
		}    
    
}
