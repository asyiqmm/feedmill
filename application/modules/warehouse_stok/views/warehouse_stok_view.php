<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">

		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="container-fluid">

			<div class='card'>
				<div class='card-header'>
					<h4 class='card-title'>Data Stok</h4>
					<div class="card-tools">

					</div>
				</div>

				<div class='card-body table-responsive p-3'>

					<table id='tabel_getstock' class='table table-bordered table-hover' style='width:100%'>
						<thead class="bg-primary">
							<tr>
								<th>Kode</th>
								<th>Nama Material</th>
								<th>Unit</th>
								<th>Stok</th>
							</tr>
						</thead>
					</table>

				</div>
			</div>

		</div>

		<!-- /.card -->

		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>


<div class="modal fade" id="ModalAddQrcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Data QR Code</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method='post' action='<?php echo site_url('warehouse_receive/add'); ?>'>
					<div class='form-group row'>
						<label for='waktu' class='col-sm-2 col-form-label'>Nama</label>
						<div class='col-sm-10'>
							<select class='form-control' id='nama' name='nama'>
								<?php
								foreach ($material as $mtr) {
									echo "
							<option value='$mtr->id'>$mtr->material_nm</option>
							";
								}
								?>
							</select>
						</div>
					</div>

					<div class='form-group row'>
						<label for='jumlah' class='col-sm-2 col-form-label'>Jumlah</label>
						<div class='col-sm-10'>
							<input type='text' class='form-control' id='jumlah' placeholder='0' name='jumlah'>
						</div>
					</div>

					<!--<input type='submit' class='btn btn-primary' value='Simpan'> 
					
					-->

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
				<input type="submit" class="btn btn-primary" value="Simpan">

				</form>
			</div>
		</div>
	</div>
</div>


<script>
	$(function() {
		datatable_warehouse();
	});

	function datatable_warehouse() {

		var siteTable = $('#tabel_getstock').DataTable({
			"info": true,
			"destroy": true,
			"lengthChange": true,
			"order": [
				[1, "asc"]
			],
			"paging": false,
			"pageLength": 7,
			"lengthMenu": [
				[7, 10, 25, 50, -1],
				[7, 10, 25, 50, "All"]
			],
			"scrollCollapse": true,
			"scrollY": "500px",
			"scrollX": true,
			"ajax": 'warehouse_stok/get_stock',
			"columns": [

				{
					"data": 'material_kode',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '15%'
				},
				{
					"data": 'material_nm',
					"className": 'details-control align-middle text-nowrap',

				},
				{
					"data": 'unit',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%',
					"orderable":false,	
				},
				{
					"data": 'qty',
					"className": 'details-control align-middle text-nowrap text-center',
					"width": '1%'
				},


			],
			"columnDefs": [{
				"targets": '_all',
				"defaultContent": '0'
			}],
			"order": [
				[3, 'desc']
			],

			initComplete: function() {
				this.api().buttons().container()
					.appendTo($('.col-md-6:eq(0)', this.api().table().container()));
			}
		});
	}
</script>