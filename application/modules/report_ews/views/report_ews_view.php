<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>
    <style>
        .table td,
        .table th {
            padding: 0.45rem !important;
            vertical-align: middle !important;
            border-top: 1 px solid #dee2e6 !important;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Produksi</h3>
                        </div>
                        <!-- /.card-header -->
                        <style>
                        </style>
                        <div class="card-body">
                            <!-- <img src="" alt=""> -->
                            <table id="tabel_produksi" width="100%" class="table table-bordered">
                                <thead class="bg-primary">
                                    <tr>
                                        <th class="align-middle" rowspan="2">No Input</th>
                                        <th class="align-middle" rowspan="2">Kode Batch</th>
                                        <th class="align-middle" rowspan="2">Tgl Produksi</th>
                                        <th class="align-middle" rowspan="2">Nama Pakan</th>
                                        <th class="align-middle" rowspan="2">Formula</th>
                                        <th class="align-middle" colspan="2">Min. Temp Aktual</th>
                                        <th class="align-middle" colspan="2">Max Ampere</th>
                                        <th class="align-middle" colspan="2">Max Dust Cooler</th>
                                        <th class="align-middle" colspan="2">Std. PDI</th>
                                        <th class="align-middle" colspan="2">Max Tepungan</th>
                                        <th class="align-middle" rowspan="2">Revisi</th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Data</th>
                                        <th class="align-middle">Limit EWS</th>
                                        <th class="align-middle">Data</th>
                                        <th class="align-middle">Limit EWS</th>
                                        <th class="align-middle">Data</th>
                                        <th class="align-middle">Limit EWS</th>
                                        <th class="align-middle">Data</th>
                                        <th class="align-middle">Limit EWS</th>
                                        <th class="align-middle">Data</th>
                                        <th class="align-middle">Limit EWS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<!-- Langkah Supervisor -->
<div class="modal fade" id="modal-data-produksi">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Rencana Produksi Baru </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="post_produksi" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="kode_pakan">Tanggal</label>
                            <div class="tgl" id="datetimepickertanggal" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickertanggal" id="tanggal" name="tgl_produksi" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="shift">Shift</label>
                            <input id="shift" name="shift" type="number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="kode_pakan">Pakan</label>
                            <select name="kode_pakan" id="select_pakan" class="select2bs4" required>
                                <option></option>
                                <?php foreach ($pakan as $key => $value) {
                                    echo '<option value="' . $value->kode_pakan . '" nama="' . $value->nama_pakan . '" jenis="' . $value->jenis . '" fase="' . $value->fase . '">[' . $value->kode_pakan . '] ' . $value->nama_pakan . '</option>';
                                } ?>
                            </select>
                            <input type="hidden" id="nama_pakan" name="nama_pakan">
                        </div>
                        <div class="col-md-3">
                            <label for="kode_pakan">Kode Pakan</label>
                            <input id="kode_pakan" type="text" class="form-control" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="jenis_pakan">Jenis Pakan</label>
                            <input id="jenis_pakan" name="jenis_pakan" type="text" class="form-control" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="fase_pakan">Fase Pakan</label>
                            <input id="fase_pakan" name="fase_pakan" type="text" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="kode_batch">Kode Batch</label>
                            <input type="text" name="kode_batch" id="kode_batch" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="formula">formula:</label>
                            <select class="select2bs4" name="formula" id="formula" required>
                                <option value="010521">010521</option>
                                <option value="040521">040521</option>
                                <option value="050521">050521</option>
                                <option value="090521">090521</option>

                            </select>
                            <!-- <input type="text" id="formula" name="formula" class="form-control form-control-sm"> -->
                        </div>


                    </div>
                    <!-- <div class="form-group row">
                        <div class="col-md-3">
                            <label for="steam">Steam</label>
                            <input type="text" name="steam" id="steam" class="qtytimbang form-control form-control-sm" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="retentioner">Retentioner</label>
                            <input type="text" class="form-control form-control-sm" name="retentioner" id="retentioner" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="temp_panel">Temp Panel</label>
                            <input type="text" class="form-control form-control-sm" name="temp_panel" id="temp_panel" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="no_pellet">No Pellet:</label>
                            <input type="text" class="form-control form-control-sm" name="no_pellet" id="total_prosen" required>
                        </div>

                    </div> -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Langkah operator -->
<div class="modal fade" id="modal-proses-operator">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title-operator"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="form_proses_operator" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="operator_no_input">No Input</label>
                            <input type="text" class="form-control form-control-sm" id="operator_no_input" name="no_input" required readonly>
                        </div>
                        <div class="col-md-4">
                            <label for="operator_tgl">Tanggal</label>
                            <input type="text" class="form-control form-control-sm" id="operator_tgl" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_shift">Shift</label>
                            <input id="operator_shift" type="number" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="operator_nama_pakan">Pakan</label>
                            <input type="text" class="form-control form-control-sm" id="operator_nama_pakan" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="operator_kode_pakan">Kode Pakan</label>
                            <input id="operator_kode_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_jenis_pakan">Jenis Pakan</label>
                            <input id="operator_jenis_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="operator_fase_pakan">Fase Pakan</label>
                            <input id="operator_fase_pakan" type="text" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="operator_kode_batch">Kode Batch</label>
                            <input type="text" id="operator_kode_batch" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-sm-6">
                            <label for="operator_formula">formula:</label>
                            <input type="text" id="operator_formula" class="form-control form-control-sm" readonly>
                        </div>


                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="steam">Steam</label>
                            <input type="text" name="operator[steam]" id="opertaor_steam" class="qtytimbang form-control form-control-sm input-operator" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="retentioner">Retentioner</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[retentioner]" id="opertaor_retentioner" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="temp_panel">Temp Panel</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[temp_panel]" id="opertaor_temp_panel" required>
                        </div>
                        <div class="col-sm-3">
                            <label for="no_pellet">No Pellet:</label>
                            <input type="text" class="form-control form-control-sm input-operator" name="operator[no_pellet]" id="opertaor_no_pellet" required>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnOperator" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Langkah QA -->
<div class="modal fade" id="modal-proses-qa" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Proses produksi oleh QA </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_proses" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_no_input">No. Input</label>
                                    <input type="text" name="pakan[no_input]" class="form-control form-control-sm" autocomplete="off" id="proses_no_input" readonly />
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_tanggal">Tanggal</label>
                                    <input type="text" class="form-control form-control-sm" autocomplete="off" id="proses_tanggal" readonly />
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_shift">Shift</label>
                                    <input id="proses_shift" type="number" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="proses_nama_pakan">Nama Pakan</label>
                                    <input id="proses_nama_pakan" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_kode_pakan">Kode Pakan</label>
                                    <input id="proses_kode_pakan" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_jenis_pakan">Jenis Pakan</label>
                                    <input id="proses_jenis_pakan" name="pakan[jenis]" type="text" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-2">
                                    <label for="proses_fase_pakan">Fase Pakan</label>
                                    <input id="proses_fase_pakan" name="pakan[fase]" type="text" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="proses_kode_batch">Kode Batch</label>
                                    <input type="text" id="proses_kode_batch" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-sm-6">
                                    <label for="proses_formula">formula:</label>
                                    <input type="text" id="proses_formula" class="form-control form-control-sm" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_steam">Steam</label>
                                    <input type="text" id="proses_steam" class="qtytimbang form-control form-control-sm" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_retentioner">Retentioner</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_retentioner" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_temp_panel">Temp Panel</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_temp_panel" readonly>
                                </div>
                                <div class="col-sm-3">
                                    <label for="proses_no_pellet">No Pellet:</label>
                                    <input type="text" class="form-control form-control-sm" id="proses_no_pellet" readonly>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-6 border-left">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="proses_temp_aktual">Temp Aktual</label>
                                    <input id="proses_temp_aktual" name="qa[temp_aktual]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_ampere">Ampere</label>
                                    <input id="proses_ampere" name="qa[ampere]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_speed_feeder">Speed Feeder</label>
                                    <input id="proses_speed_feeder" name="qa[speed_feeder]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                                <div class="col-md-3">
                                    <label for="proses_dust_cooler">Dust Cooler</label>
                                    <input id="proses_dust_cooler" name="qa[dust_cooler]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-top border-bottom">
                                    <tr>
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>PDI</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_pdi_pellet">PDI Pellet</label>
                                                <input id="proses_pdi_pellet" name="qa[pdi_pellet]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_crumble">Crumble</label>
                                                <input id="proses_crumble" name="qa[crumble]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-bottom">
                                    <tr>
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>Tertahan di SIZING 8</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz8_sampe1">Sample 1</label>
                                                <input id="proses_sz8_sampe1" name="qa[sz8_sample1]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz8_sampe2">Sample 2</label>
                                                <input id="proses_sz8_sampe2" name="qa[sz8_sample2]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group row">
                                <table width="100%" class="border-bottom">
                                    <tr class="align-middle">
                                        <td rowspan="2" width="20%">
                                            <div class="col-md-12 text-center">
                                                <strong>Tertahan di SIZING 14</strong>
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz14_sampe1">Sample 1</label>
                                                <input id="proses_sz14_sampe1" name="qa[sz14_sample1]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                        <td width="40%">
                                            <div class="col-md-12 text-center">
                                                <label for="proses_sz14_sampe2">Sample 2</label>
                                                <input id="proses_sz14_sampe2" name="qa[sz14_sample2]" type="text" class="form-control input-qa qtytimbang form-control-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="hidden" id="status_qa" name="status_qa">
                                <textarea class="form-control" name="qa[keterangan]" id="proses_keterangan" cols="1" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnProcess" class="btn btn-primary">Proses</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(function() {
        datatable_report();
        // list_formula();
        $('.tgl').datetimepicker({
            format: 'DD/MM/YYYY',
            autoclose: true,
            // todayHighlight: true,
            minuteStepping: 1, //set the minute stepping

            // minDate:'1900/1/1',               //set a minimum date  <---- HERE 
            maxDate: moment(),
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.qtytimbang').inputmask('decimal', {
            allowMinus: false,
            integerDigits: 9,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
        });
    });

    $('#select_pakan').on('change', function() {
        var val = $(this);
        var attr = $('option:selected', this);
        $('#nama_pakan').val(attr.attr('nama'));
        $('#kode_pakan').val(val.val());
        $('#jenis_pakan').val(attr.attr('jenis'));
        $('#fase_pakan').val(attr.attr('fase'));
    });

    function list_formula() {
        var formula = $('#fml_no').val();
        var name = $('#fml_nm').val();
        jQuery.ajax({
            type: "GET",
            url: "https://ismaf.mydmc.co.id/api/formulapremixmaster?formula=" + formula + "&name=" + name, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.data.length; index++) {
                    option += '<option value=' + data.data[index].notrans + ' fml_no=' + data.data[index].fml_no + '>' + data.data[index].item_name + '</option>';
                }
                if (data.meta.code == 200) {
                    showToast(data.meta.message);
                    $('#fml_nm').html(option);
                } else {
                    showToastWarning(data.meta.message);
                }

            },
            error: function(xhr, status, error) {
                showToastWarning('error')
            }

        });
    };
    $("#datetimepicker2,#datetimepicker3").on("change.datetimepicker", ({
        date
    }) => {
        var start = $('#fml_start').val();
        var end = $('#fml_end').val();
        jQuery.ajax({
            type: "GET",
            url: "premix_rencana_produksi/list_formula?start=" + start + "&end=" + end, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.length; index++) {
                    option += '<option value=' + data[index].notrans + ' item=' + data[index].item + ' fml_no=' + data[index].fml_no + '>' + data[index].item_name + '</option>';
                }
                $('#fml_nm').html(option);

                // if (data.meta.code == 200) {
                //     showToast(data.meta.message);
                // } else {
                //     showToastWarning(data.meta.message);
                // }

            },
            error: function(xhr, status, error) {
                // showToastWarning('error')
            }

        });
    });

    $("#datetimepickerawal").on("change.datetimepicker", ({
        date
    }) => {
        var date2 = $('#bulan2').val();

        datatable_premix(moment(date).format("DD/MM/YYYY"), date2);
    });

    $("#datetimepickerakhir").on("change.datetimepicker", ({
        date
    }) => {
        var date1 = $('#bulan').val();
        datatable_premix(date1, moment(date).format("DD/MM/YYYY"));
    });


    function datatable_report() {
        $('#tabel_produksi').DataTable().clear();
        // // $('#daily_report_table_body').DataTable().destroy();
        $('#tabel_produksi').empty();
        var siteTable = $('#tabel_produksi').DataTable({
            "ajax": 'report_ews/get_report_ews',
            searching: true,
            "destroy": true,
            "order": [
                [1, "asc"]
            ],
            'rowCallback': function(row, data, index) {
                console.log(data);
                if (data.temp_aktual <= data.min_temp_aktual) {
                    $(row).find('td:eq(5)').addClass('bg-danger');
                }

                if (data.ampere >= data.max_ampere) {
                    $(row).find('td:eq(7)').addClass('bg-danger');
                }

                if (data.dust_cooler >= data.max_dust_cooler) {
                    $(row).find('td:eq(9)').addClass('bg-danger');
                }

                if (data.pdi_pellet <= data.std_pdi) {
                    $(row).find('td:eq(11)').addClass('bg-danger');
                }

                if (data.tepungan >= data.max_tepungan) {
                    $(row).find('td:eq(13)').addClass('bg-danger');
                }
                // if (data[3] > 11.7) {
                //     $(row).find('td:eq(3)').css('color', 'red');
                // }
                // if (data[2].toUpperCase() == 'EE') {
                //     $(row).find('td:eq(2)').css('color', 'blue');
                // }
            },
            columns: [{
                data: 'no_input',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'kode_batch',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'tgl_produksi',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'nama_pakan',
                className: 'details-control align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'formula',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'temp_aktual',
                className: 'details-control align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'min_temp_aktual',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'ampere',
                className: 'details-control align-middle text-nowrap',
                width: '5%',
            }, {
                data: 'max_ampere',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'dust_cooler',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'max_dust_cooler',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'pdi_pellet',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'std_pdi',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'tepungan',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                data: 'max_tepungan',
                className: 'details-control align-middle text-center text-nowrap',
                width: '5%',
            }, {
                className: 'details-control align-middle text-center text-nowrap',
                width: '1%',
                "render": function(data, type, row, meta) {
                    var btn = '<button class="btn btn-sm btn-secondary btn-revisi" title="Revisi">Revisi</button>'
                    return btn;
                }
            }, ],
        });
        
        $('#tabel_produksi tbody').on('click', '.btn-revisi', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            console.log(datas);
            
        });

        $('#modal-premix').on('hidden.bs.modal', function(e) {
            var table = $('#tabel_material_premix').DataTable();
            table.clear();
            table.draw();
        })
    }

    $('#tabel_rencana_produksi_filter').keyup(function() {
        $('#tabel_rencana_produksi').DataTable().search(
            $(this).val()
        ).draw();

    });

    function check_qa() {
        var isValid = true;
        $(".input-qa").each(function() {
            var element = $(this);
            // console.log('data',element.val());
            if (element.val() == "" || element.val() == 0) {
                isValid = false;
            }
        });
        $('#status_qa').val(isValid);

        console.log($('#status_qa').val());
    }
    $('.input-qa').on('keyup', function() {
        check_qa();
    })

    function cek_input() {
        // console.log(isValid);
    }
    $('#form_proses').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_proses_qa',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnProcess').attr('disabled', true);

            },
            success: function(data) {

                $('#btnProcess').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-proses-qa").modal("hide");
                } else {
                    showToastError(data.msg);
                };

            }
        });
    });

    $('#form_proses_operator').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_proses_operator',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnOperator').attr('disabled', true);

            },
            success: function(data) {

                $('#btnOperator').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-proses-operator").modal("hide");
                } else {
                    showToastError(data.msg);
                };

            }
        });
    });


    $('#post_produksi').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'ews_rencana_produksi/post_produksi',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnTambah').attr('disabled', true);
                swal.fire({
                    title: 'Tunggu..!',
                    text: 'Memproses..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                $('#btnTambah').attr('disabled', false);
                if (data.status) {
                    swal.hideLoading();

                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };
                $('#modal-data-produksi').modal('hide');
            },
            error: function() {
                $('#btnTambah').attr('disabled', false);
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
            }
        });
    });
</script>