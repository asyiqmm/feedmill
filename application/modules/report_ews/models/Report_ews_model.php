<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Report_ews_model extends CI_Model
{
    public function get_report_ews()
    {
        // if ($this->ion_auth->in_group('qa')) {
        //     $qa = true;
        // } else {
        //     $qa = false;
        // }
        $this->db->select('a.*')
            ->from('t_ews_produksi a')
            ->where('temp_aktual_min_temp_aktual = 1')
            ->or_where('ampere_max_ampere = 1')
            ->or_where('dust_cooler_max_dust_cooler = 1')
            ->or_where('pdi_pellet_std_pdi = 1')
            ->or_where('tepungan_max_tepungan = 1')
            ->where('status',2)
            ->or_where('status',3);
        return $this->db->get();

        
    }

    
}
