<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Report_ews extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('report_ews_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Report EWS | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 28;
			$data['child_active'] = 35;
			$data['grandchild_active'] = null;
			$this->_render('report_ews_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_report_ews()
	{
		$get = $this->report_ews_model->get_report_ews();
		$obj = array("data" => $get->result_array());
		echo json_encode($obj);
	}
}
