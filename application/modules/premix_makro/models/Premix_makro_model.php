<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Premix_makro_model extends CI_Model
{
    public function cek_no($no)
    {
        $this->db->where('id_transaction', $no)
            ->where('status', 1);
        return $this->db->get('t_rencana_produksi');
    }

    public function refresh_bin()
    {
        $this->db->set('bin_open', 0)
            ->update('m_bin');
    }
    public function get_makro()
    {
        'select sum(b.qty_material) qty,a.no_transaction ,a.tanggal ,c.formula_no ,c.formula_kode ,c.formula_name ,a.status 
        from t_rencana_produksi a
        join d_formula_material b on  b.id_rencana_produksi = a.id 
        group by a.no_transaction';
        // $this->db->select("COUNT(CASE WHEN b.sts_makro = 1 then 1 ELSE NULL END) as 'proses_timbang',
        // COUNT(CASE WHEN b.sts_makro = 0 then 1 ELSE NULL END) as 'belum_timbang',
        // sum(b.qty_material) qty,a.no_transaction ,a.tanggal ,c.formula_no ,c.formula_kode ,c.formula_name ,a.status,d.nama status_nm")
        //     ->from('t_rencana_produksi a')
        //     ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
        //     ->join('m_formula c', 'c.id = b.id_formula')
        //     ->join('m_status d', 'd.id = a.status ')
        //     ->group_by('a.no_transaction')
        //     ->having('proses_timbang != 0 or belum_timbang != 0');
        // return $this->db->get();
        $this->db->select("a.id_transaction, a.tanggal, a.fml_no, a.fml_transaction, a.fml_name")
            ->from('t_rencana_produksi a')
            // ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
            // ->join('m_formula c', 'c.id = b.id_formula')
            // ->join('m_status d', 'd.id = a.status ')
            // ->group_by('a.no_transaction')
            // ->having('proses_timbang != 0 or belum_timbang != 0');
            ->where('a.tanggal', date('Y-m-d'))
            ->where('a.status', 1)
            ->where('a.active', 1)
            ->where('sts_makro', 0);
        return $this->db->get();
    }

    public function get_count_makro($no_transaction)
    {
        "select LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material ,d.original_pack
        from t_rencana_produksi a
        join d_formula_material b on  b.id_rencana_produksi = a.id 
        join m_formula c on c.id = b.id_formula 
        join m_material d on d.id = b.id_material 
        join m_unit_satuan e on e.id = d.unit 
        where a.no_transaction = '2021042701PMX001'
        order by seq";
        $this->db->select("LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material ,d.original_pack ,qty_timbang_makro")
            ->from('t_rencana_produksi a')
            ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
            ->join('m_formula c', 'c.id = b.id_formula')
            ->join('m_material d', 'd.id = b.id_material')
            ->join('m_unit_satuan e', 'e.id = d.unit')
            ->where('a.no_transaction', $no_transaction)
            ->where_in('sts_makro', [1, 2])
            ->where('qty_timbang_makro <> 0')
            ->order_by('seq');
        return $this->db->get();
    }
    public function get_list_makro($no_transaction)
    {
        // var_dump($type);die();
        $this->db->select("lpad(a.sequence + 0, 3, '0') seq,
                a.id_transaction,
                a.material_kd,
                a.material_nm,
                ifnull(b.original_pack,
                25) original_pack,
                a.qty_material,
                a.qty_timbang_makro,
                (FLOOR(a.qty_material / ifnull(b.original_pack, 25)) * ifnull(b.original_pack, 25)) total_makro ,
                format(FLOOR(a.qty_material / ifnull(b.original_pack, 25)) * ifnull(b.original_pack, 25) - a.qty_timbang_makro, 2) qty_makro, a.sequence")
            ->from('d_dosing a')
            ->join('d_original_pack b', 'b.material = a.material_kd and b.active = 1', 'left')
            ->join('t_rencana_produksi c', 'c.id_transaction = a.id_transaction and c.active = 1')
            ->where('a.id_transaction', $no_transaction);

        $this->db->order_by('a.`sequence`', 'ASC');
        $result =  $this->db->get();
        // var_dump($this->db->last_query());die();
        return $result;
    }
    public function get_list_timbang($no_transaction, $type)
    {
        // var_dump($type);die();
        $this->db->select("d.id id_bin,lpad(a.sequence + 0, 3, '0') seq,
                a.id_transaction,
                a.material_kd,
                a.material_nm,
                ifnull(b.original_pack,
                25) original_pack,
                a.qty_material,
                a.qty_timbang_makro,
                (FLOOR(a.qty_material / ifnull(b.original_pack, 25)) * ifnull(b.original_pack, 25)) total_makro ,
                format(FLOOR(a.qty_material / ifnull(b.original_pack, 25)) * ifnull(b.original_pack, 25) - a.qty_timbang_makro, 2) qty_makro, a.sequence,
                COALESCE(d.qty,0) qty_bin")
            ->from('d_dosing a')
            ->join('d_original_pack b', 'b.material = a.material_kd and b.active = 1', 'left')
            ->join('t_rencana_produksi c', 'c.id_transaction = a.id_transaction and c.active = 1')
            ->join('m_bin d', 'd.material = a.material_kd', 'left')
            ->where('a.id_transaction', $no_transaction);
        if ($type >= 2) {
            $this->db->having('a.qty_timbang_makro < total_makro')
                ->having('total_makro <> 0');
        } else {
            $this->db->having('a.qty_timbang_makro > 0');
        }
        // $this->db->having('qty_makro <> 0');
        $this->db->order_by('a.`sequence`', 'ASC');
        if ($type == 2) {
            $this->db->limit(1);
        }
        $result =  $this->db->get();
        // var_dump($this->db->last_query());die();
        return $result;

        // "SELECT
        //     LPAD(b.`sequence` + 0, 3, '0') seq,
        //     concat('[', d.material_kode , '] ', d.material_nm) material,
        //     e.unit_nm ,
        //     b.qty_material ,
        //     d.original_pack,
        //     b.qty_timbang_makro,
        //     format((b.qty_material / d.original_pack) * d.original_pack - b.qty_timbang_makro,2) qty_makro
        // from
        //     t_rencana_produksi a
        // join d_formula_material b on
        //     b.id_rencana_produksi = a.id
        // join m_formula c on
        //     c.id = b.id_formula
        // join m_material d on
        //     d.id = b.id_material
        // join m_unit_satuan e on
        //     e.id = d.unit
        // where
        //     a.no_transaction = '2021042701PMX001'
        //     and b.sts_makro in (0,1)
        // HAVING b.qty_timbang_makro <= (FLOOR(b.qty_material / d.original_pack) * d.original_pack)
        // order by
        //     seq";

        // $this->db->select("b.id id_formula_material,
        //         LPAD(b.`sequence` + 0, 3, '0') seq,
        //         concat('[', d.material_kode , '] ', d.material_nm) material,
        //         e.unit_nm ,
        //         b.qty_material ,
        //         d.original_pack,
        //         b.qty_timbang_makro,
        //         (FLOOR(b.qty_material / d.original_pack) * d.original_pack) - b.qty_timbang_makro qty_makro")
        //     ->from('t_rencana_produksi a')
        //     ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
        //     ->join('m_formula c', 'c.id = b.id_formula')
        //     ->join('m_material d', 'd.id = b.id_material')
        //     ->join('m_unit_satuan e', 'e.id = d.unit')
        //     ->where('a.no_transaction', $no_transaction)
        //     ->where_in('b.sts_makro', [0, 1])
        //     // ->having('b.qty_timbang_makro <= qty_makro')
        //     ->order_by('seq')
        //     ->limit(1);
        // return $this->db->get();
    }

    public function post_timbang_material()
    {
        // var_dump($_POST);
        // die();
        $this->db->trans_begin();
        // $sql = "UPDATE
        //     d_formula_material
        // set
        //     qty_timbang_makro = qty_timbang_makro + " . $data['qty_timbang_makro'] . ",
        //     tgl_timbang_makro = '" . date('Y-m-d H:i:s') . "',
        //     sts_makro = 1
        // where
        //     id = " . $data['id_formula_material'] . "";

        // $this->db->query($sql);
        // $sql2 = "UPDATE d_material_qrcode b
        // join m_qrcodes a 
        //     on a.id = b.id_qrcode 
        // set
        //     b.sts_use = 1
        // where
        //     a.codes = '" . $data['codes'] . "'";
        // $this->db->query($sql2);
        $timbang = floatval($_POST['qty_timbang_makro']);
        $this->db->set('qty', "`qty` - $timbang", FALSE)
            ->where('material', $_POST['id_material'])
            ->update('m_bin');
        // var_dump($this->db->last_query());die();
        $this->db->set('qty_timbang_makro', "`qty_timbang_makro` + $timbang", FALSE)
            ->set('tgl_timbang_makro', date('Y-m-d H:i:s'))
            ->where('material_kd', $_POST['id_material'])
            ->where('id_transaction', $_POST['id_transaction'])
            ->update('d_dosing');

        $this->db->set('bin_open', 0)
            ->where('material', $_POST['id_material'])
            ->update('m_bin');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil menginput data'
            ];
        }
    }

    public function cek_kdmaterial()
    {
        $this->db->select('a.id')
            ->from('m_material a')
            ->join('d_material_qrcode b', 'b.id_material = a.id')
            ->join('m_qrcodes c', 'c.id = b.id_qrcode')
            ->where('c.codes', $_POST['codes'])
            ->where('b.sts_use', 0);
        return $this->db->get();
    }

    public function cek_kode()
    {
        "SELECT *
        from m_material a
        join d_material_qrcode b on b.id_material = a.id 
        join m_qrcodes c on c.id = b.id_qrcode 
        where c.codes = '20210430110820003'
        and a.id = 1
        and b.sts_use = 0";

        $kode = $this->db->select('*')
            ->from('m_qrcodes a')
            ->where('a.codes', $_POST['code'])
            // ->where('b.sts_use', 0)
            ->where('a.material', $_POST['id_material'])
            ->get();
        $cek = $kode->num_rows();
        if ($cek > 0) {
            $this->db->set('bin_open', 1)
                ->where('material', $_POST['id_material'])
                ->update('m_bin');
            $result = [
                'status' => true,
                'msg' => 'Kode material cocok'
            ];
        } else {
            $result = [
                'status' => false,
                'msg' => 'Kode material salah'
            ];
        }
        return $result;
    }
}
