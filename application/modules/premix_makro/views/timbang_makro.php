<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form action="" method="post" id="form_penimbangan" enctype="multipart/form-data" class="form-horizontal">

                <div class="row">
                    <div class="col-12">
                        <div class="card text-center">
                            <div class="card-header">
                                Informasi Material Makro
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">

                                <div class="col">
                                    <div class="row justify-content-center">
                                        <h2 id="material" style="font-size: 2.5em; font-weight:bold;">-</h2>
                                        <input type="hidden" name="id_transaction" id="id_formula_material">
                                        <input type="hidden" name="id_material" id="id_material">

                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <h2 class="description-header" style="font-size: 2.5em;">Target :&nbsp;</h2>
                                        <h2 class="description-header font-weight-bold" id="qty_target" style="font-size: 2.5em;">-</h2>
                                        <span class="description-text" style="font-size: 1.7em;">Kg</span>
                                        <input type="hidden" name="qty_timbang_makro" id="qty_timbang_makro">
                                    </div>
                                    <!-- /.description-block -->
                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Timbangan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <style>
                                            @media screen and (max-width : 1180px) {
                                                .timbangan {
                                                    font-size: 3.4em;
                                                }
                                            }

                                            @media screen and (min-width : 1181px) {
                                                .timbangan {
                                                    font-size: 5.04em;
                                                }
                                            }
                                        </style>
                                        <table width="100%" style="background-color: darkorange;">
                                            <tr>
                                                <td colspan="2" class="text-right"><span class="font-weight-bold align-top timbangan" id="nilai_timbangan">0.00</span> <span class="font-weight-bold align-top timbangan">KG</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center text-nowrap"><span class="text-light text-danger" style="display: none; font-size:1.6em" id="alert_timbangan">*Timbangan tidak terhubung</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan1" value="1" name="r1" checked>
                                                        <label for="timbangan1">
                                                            Timbangan 1
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan2" value="2" name="r1">
                                                        <label for="timbangan2">
                                                            Timbangan 2
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- <style>
                                            .h1,
                                            h1 {
                                                font-size: 4.5rem;
                                            }
                                        </style>
                                        <h1 class="description-header font-weight-bold " id="nilai_timbangan">0.00</h1>
                                        <span class="description-text">Kg</span> -->
                                    </div>
                                    <!-- <span class="text-danger" id="alert_timbangan" style="display: none;">*Timbangan tidak terhubung</span> -->
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Cek Kode
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <button class="btn btn-primary btn-lg" disabled hidden id="btnSimpan" type="submit">Simpan</button>
                                <input autofocus id="codes" name="codes" autocomplete="off" class="form-control font-weight-bold text-center" style="font-size: 2em;" required />

                                <div class="col-sm-12 sukses" style="display: none;">
                                    <!-- <label for="">Cocok</label> -->

                                    <div class="row justify-content-center">
                                        <i class="fas fa-check-circle text-success display-4"></i>
                                    </div>
                                </div>
                                <div class="col-sm-12 gagal" style="display: none;">
                                    <!-- <label for="">Tidak Cocok</label> -->

                                    <div class="row justify-content-center">
                                        <i class="fas fa-times-circle text-danger display-4"></i>
                                    </div>
                                </div>
                                <div class="col-sm-12 kurang justify-content-center" style="display: none;">
                                    <span class="text-danger">*Stok Bin kurang</span>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                </div>
            </form>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Produksi Mikro</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_makro" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>Seq</th>
                                        <th>Material</th>
                                        <th>Unit</th>
                                        <th>Qty Makro</th>
                                        <th>Qty Timbang</th>
                                        <th>Dif</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    // $('#codes').blur(function(event) {
    //     event.target.checkValidity();
    //     console.log(event.target.value);
    // }).bind('invalid', function(event) {
    //     setTimeout(function() {
    //         $(event.target).focus();
    //         console.log('fokus');
    //     }, 1000);
    // });
    document.onkeypress = dump;
    $(function() {
        datatable_makro();
        get_list_timbang();
    });
    var word = "";

    function dump(e) {
        var unicode = e.keyCode ? e.keyCode : e.charCode;
        var actualkey = String.fromCharCode(unicode);
        word += actualkey;
        console.log('wadasda');
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 5 second for example
    var $input = $('#codes');

    //on keyup, start the countdown
    $input.on('keyup', function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });


    //user is "finished typing," do something
    function doneTyping() {
        var input = word.substring(0, 8);
        if (input == '37775814') {
            if (cek_status) {
                $('#btnSimpan').attr('disabled', false);
                $('#btnSimpan').trigger('click');
            } else {
                $('#btnSimpan').attr('disabled', true);
                $('#codes').focus();
                word = '';
            }
        } else {
            $('#btnSimpan').attr('disabled', true);
            $('#codes').focus();
            word = '';
        }
    }

    $('#codes').keypress(function(event) {
        var code = $(this).val();
        var id_material = $('#id_material').val();

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $.ajax({
                url: <?php base_url() ?> '/premix/premix_makro/cek_code',
                method: "post",
                data: {
                    code: code,
                    id_material: id_material,
                },
                dataType: "JSON",
                beforeSend: function() {
                    $('#btnSimpan').attr('disabled', true);
                },
                success: function(data) {
                    $("#tabel_makro")
                        .DataTable()
                        .ajax.reload();
                    // get_list_timbang();

                    if (data.status) {

                        cek_status = true;
                        $('#codes').attr('readonly', true);
                        $('#codes').focusout();
                        $('.sukses').fadeIn(500);
                        $('.gagal').hide();
                        $('#btnSimpan').attr('disabled', true);
                        weight = true;
                        get_timbang();
                    } else {
                        weight = false;
                        timeout = null;
                        cek_status = false;
                        $('#codes').val('');
                        $('.gagal').fadeIn(500);
                        $('.sukses').hide();
                    };

                },
                error: function() {}
            });
        }
    });

    $('#form_penimbangan').on('submit', function(event) {
        var codes = $('#codes').val()
        if (codes == '' || codes == null) {
            console.log('hei');
            word = "";
            return false;
        } else {

            console.log('ha');

            event.preventDefault();
            $.ajax({
                url: <?php base_url() ?> '/premix/premix_makro/post_timbang_material',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                dataType: "JSON",
                beforeSend: function() {
                    $('#btnSimpan').attr('disabled', true);
                    cek_status = false;
                },
                success: function(data) {
                    $("#tabel_makro")
                        .DataTable()
                        .ajax.reload();

                    $('#btnSimpan').attr('disabled', false);
                    if (data.status) {
                        $('#codes').val('').focus();
                        $('#codes').attr('readonly', false);
                        $('#btnSimpan').attr('disabled', true);
                        weight = false;
                        timeout = null;
                        cek_status = false;
                        showToast(data.msg);
                        $('.sukses').hide();
                        $('.gagal').hide();
                    } else {
                        showToastError(data.msg);
                        cek_status = true;
                    };
                    get_list_timbang();


                },
                error: function() {
                    cek_status = true;
                    get_list_timbang();
                }
            });
        }
    });

    function get_list_timbang() {
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_makro/get_list_timbang',
            data: {
                id_transaction: '<?php echo $no_transaction ?>'
            },
            method: "GET",
            dataType: "json",
            success: function(data) {
                console.log(data);
                if (data.status) {
                    var bin = data.qty_bin;
                    var qty = data.qty_makro;
                    console.log(Number(bin), qty);
                    if (data.id_bin) {
                        if (Number(bin) >= Number(qty)) {
                            $('.kurang').hide();
                            $('#codes').attr('disabled', false);
                        } else {
                            $('.kurang').fadeIn(500);
                            $('#codes').attr('disabled', true);
                        }
                    }
                    $('#qty_target').text(data.qty_makro);
                    $('#material').text(data.material_nm);
                    $('#id_formula_material').val(data.id_transaction);
                    $('#id_material').val(data.material_kd);

                    // get_timbang();
                } else {
                    $('#material').text('Selesai').addClass('text-success');
                    weight = false;
                    timeout = null;
                    // $('#btnSimpan').attr('disabled', true);
                    $('#qty_target').text(0);
                    $('#qty_timbang_makro').val(0);
                    $('#nilai_timbangan').text(0);
                    $('#id_formula_material').val('');
                    // clearTimeout(timeout);
                }

            }
        })
    }

    var weight = true;

    function get_timbang() {
        console.log('jalan');
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_makro/get_data_timbangan?timbangan=' + $('.pilihtimbangan:checked').val(),
            method: "GET",
            dataType: "json",
            success: function(data) {
                // if (data.data[0].status == 'success') {
                //     var kg = Number(data.data[0].random);
                //     if (weight) {
                //         setTimeout(get_timbang, 500);
                //         console.log('Terhubung 0.5 detik');
                //     }
                //     $('#alert_timbangan').hide();
                //     $('#nilai_timbangan').removeClass('text-danger');
                //     $('#nilai_timbangan').text(kg.toFixed(2));
                //     $('#qty_timbang').val(kg.toFixed(2));
                // } else {
                //     // if (weight) {
                //     setTimeout(get_timbang, 5000);
                //     console.log('tidak terhubung 5 detik');
                //     // }
                //     $('#alert_timbangan').show();
                //     $('#nilai_timbangan').addClass('text-danger');
                //     a = Number(0);
                //     $('#nilai_timbangan').text(a.toFixed(2));
                //     $('#qty_timbang').val(a.toFixed(2));
                // }

                if (data.status) {
                    var kg = Number(data.data.ADC);
                    if (weight) {
                        setTimeout(get_timbang, 500);
                        console.log('Terhubung 0.5 detik');
                    }
                    $('#alert_timbangan').hide();
                    $('#nilai_timbangan').removeClass('text-danger');
                    $('#nilai_timbangan').text(kg.toFixed(2));
                    $('#qty_timbang').val(kg.toFixed(2));
                } else {
                    if (weight) {
                        setTimeout(get_timbang, 5000);
                        console.log('tidak terhubung 5 detik');
                    }
                    $('#alert_timbangan').show();
                    $('#nilai_timbangan').addClass('text-danger');
                    a = Number(0);
                    $('#nilai_timbangan').text(a.toFixed(2));
                    $('#qty_timbang').val(a.toFixed(2));
                }
            },
            error: function(data) {
                if (weight) {
                    setTimeout(get_timbang, 5000);
                    console.log('tidak terhubung 5 detik');
                }
                $('#alert_timbangan').show();
                $('#nilai_timbangan').addClass('text-danger');
                $('#btnSimpan').attr('disabled', true);
                a = Number(0);
                $('#nilai_timbangan').text(a.toFixed(2));
                $('#qty_timbang').val(a.toFixed(2));
                // console.log(data);
            }
        })
    }

    function datatable_makro() {
        var siteTable = $('#tabel_makro').DataTable({
            "lengthChange": false,
            "ajax": <?php base_url() ?> '/premix/premix_makro/get_count_makro?no_transaction=<?php echo $no_transaction ?>',
            "paging": false,
            "searching": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            ordering: false,
            "order": [
                [1, "desc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'seq',
                    className: 'align-middle text-nowrap',
                    width: '1%'
                },
                {
                    data: 'material_nm',
                    className: 'align-middle text-nowrap',
                    width: '15%'
                },
                {
                    // data: 'unit_nm',
                    className: 'text-center',
                    width: '1%',
                    "render": function(data, type, row, meta) {
                        return 'KG';
                    }
                },
                {
                    data: 'total_makro',
                    className: 'align-middle text-right',
                    width: '5%',
                    // "render": function(data, type, row, meta) {
                    //     var dibagi = row.qty_material / row.original_pack;
                    //     var final = Math.trunc(dibagi) * row.original_pack;
                    //     // var dibagi = row.qty_material / row.original_pack;
                    //     // var remainder = dibagi % 1;
                    //     // var final = remainder.toFixed(2) * row.original_pack;
                    //     return final.toFixed(2);

                    // }
                },
                {
                    data: 'qty_timbang_makro',
                    className: 'ext-right align-middle text-right text-nowrap',
                    width: '5%',
                },
                {
                    data: null,
                    width: '5%',
                    "render": function(data, type, row, meta) {
                        var dif = row.qty_timbang_makro - row.total_makro;
                        return dif.toFixed(2);
                    }
                }
            ],
        });
        $('#tabel_makro tbody').on('dblclick', 'tr', function() {
            var data = siteTable.row(this).data();
            // console.log(data);
        });

    }
</script>