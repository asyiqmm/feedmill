<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Premix Makro</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_mikro" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>No Transaction</th>
                                        <th>Date</th>
                                        <th>Formula No</th>
                                        <th>Material Name</th>
                                        <!-- <th class="text-center">Qty</th> -->
                                        <th></th>
                                    </tr>
                                </thead>
                                <!-- <tbody>
                                    <tr>
                                        <td>2021042701PMX001</td>
                                        <td>2021.04.27</td>
                                        <td>140421</td>
                                        <td>[22200320] PMX MASH 1404/21</td>
                                        <td>800,00</td>
                                        <td></td>
                                    </tr>
                                </tbody> -->
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modal-premix">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Material Formula</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_penerimaan_sparepart" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="table-responsive">
                        <table id="tabel_material_makro" width="100%" class="table table-bordered">
                            <thead class="text-center">
                                <tr width="100%">
                                    <th>Seq</th>
                                    <th>Material</th>
                                    <th>Unit</th>
                                    <th>Qty Mikro</th>
                                    <th>Qty Timbang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!-- <button type="Submit" value="Submit" id="btnSubmit" class="btn btn-primary">Save</button> -->
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(function() {
        datatable_premix();
    });

    function datatable_premix() {
        var siteTable = $('#tabel_mikro').DataTable({
            "lengthChange": false,
            "ajax": 'premix_makro/get_makro',
            "paging": false,
            "ordering": false,
            "info": false,
            "searching": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [1, "desc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'id_transaction',
                    className: 'details-control align-middle text-nowrap',
                    width: '10%'
                },
                {
                    data: 'tanggal',
                    className: 'details-control align-middle text-nowrap',
                    width: '5%'
                },
                {
                    data: 'fml_no',
                    className: 'details-control',
                    width: '5%'
                },
                {
                    data: 'fml_name',
                    className: 'details-control align-middle',
                    width: '20%'
                },
                {
                    data: null,
                    defaultContent: '<button class="btn btn-outline-success btn-sm btn-proses" title="proses">Proses</button>',
                    className: 'details-control text-center align-middle text-nowrap',
                    width: '1%'
                },
            ],
        });

        $('#tabel_mikro tbody').on('dblclick', 'tr', function() {
            var data = siteTable.row(this).data();
            if (data) {
                $('#modal-premix').modal('show');
                datatable_makro(data.id_transaction);
            }
        });

        $('#tabel_mikro tbody').on('click', '.btn-proses', function() {
            var data = siteTable.row($(this).parents('tr')).data();
            console.log(data);
            location.replace("premix_makro/timbang/" + data.id_transaction);
        });

    }

    function datatable_makro(id_transaction) {
        var table = $('#tabel_material_makro').DataTable({
            ordering: false,
            "destroy": true,

        });
        table.clear();
        table.draw();

        console.log(id_transaction);
        var siteTable = $('#tabel_material_makro').DataTable({
            "lengthChange": false,
            "ajax": 'premix_makro/get_list_makro?id_transaction=' + id_transaction,
            "paging": false,
            "destroy": true,
            "searching": false,
            "info": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            responsive: true,
            ordering: false,
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            "fnDrawCallback": function(oSettings) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            },
            columns: [{
                    data: 'seq',
                    className: 'details-control align-middle text-nowrap',
                    width: '1%'
                },
                {
                    data: 'material_nm',
                    className: 'details-control align-middle',
                    width: '30%'
                },
                {
                    defaultContent: 'KG',
                    className: 'details-control align-middle text-center',
                    width: '1%',
                },
                {
                    data: 'total_makro',
                    className: 'details-control align-middle text-right',
                    width: '1%',
                },

                {
                    data: 'qty_timbang_makro',
                    className: 'details-control align-middle text-right',
                    width: '1%'
                },

            ],

        });

        $('#modal-premix').on('hidden.bs.modal', function(e) {
            var table = $('#tabel_material_mikro').DataTable();
            table.clear();
            table.draw();
        })

        $('#tabel_makro tbody').on('dblclick', 'tr', function() {
            var data = siteTable.row(this).data();
        });
    }
</script>