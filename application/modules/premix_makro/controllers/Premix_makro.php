<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
// require __DIR__ . '../../vendor/autoload.php';
// use Curl\Curl;
class Premix_makro extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('premix_makro_model');
		$this->load->model('premix_mikro/premix_mikro_model');
		$this->load->model('premix_intake/premix_intake_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Makro | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 6;
			$data['child_active'] = 9;
			$data['grandchild_active'] = 11;
			$this->premix_intake_model->refresh_bin();
			$this->_render('premix_makro_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_makro()
	{
		$datas = $this->premix_makro_model->get_makro();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function timbang($no)
	{
		$uri = &load_class('URI', 'core');
		$data['parent_active'] = 6;
		$data['child_active'] = 9;
		$data['grandchild_active'] = 11;
		$data['no_transaction'] = $no;
		$data['fml_transaction'] = $this->premix_mikro_model->get_fml($no);
		$cek = $this->premix_makro_model->cek_no($no);
		if ($cek->num_rows() > 0) {
			$datas = $this->premix_makro_model->refresh_bin();
			$this->_render('timbang_makro', $data);
		} else {
			$this->_render('error_view', $data);
		}
	}

	public function get_list_makro()
	{
		$data = $this->premix_makro_model->get_list_makro($_GET['id_transaction']);
		$obj = array("data" => $data->result());
		echo json_encode($obj);
	}
	public function get_count_makro()
	{
		$data = $this->premix_makro_model->get_list_timbang($_GET['no_transaction'], 1);
		// $no_transaction = $this->input->get('no_transaction');
		// $datas = $this->premix_makro_model->get_count_makro($no_transaction);
		// $data =  json_decode(json_encode($datas->result()), TRUE);
		// foreach ($data as $key => $value) {
		// 	// intVal($result->qty_material / $result->original_pack) * $result->original_pack
		// 	$data[$key]['qty_makro'] = number_format(intVal($value['qty_material'] / $value['original_pack']) * $value['original_pack'], 2);
		// }
		$obj = array("data" => $data->result());
		echo json_encode($obj);
	}

	public function get_data_timbangan()
	{
		$this->load->library('curl');
		if ($_GET['timbangan'] == 1) {
			$url = 'http://192.168.137.4/';
		} else {
			$url = 'http://192.168.58.3/';
		}

		$url = 'https://csrng.net/csrng/csrng.php?min=24&max=25';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		// curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers

		$r = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		// var_dump(isset($r));die();
		if ($httpcode == 200) {
			if (isset($r)) {
				$result = [
					'status' => true,
					'data'	 => json_decode($r,true)
					// 'data'	 => json_decode($r)
				];
			} else {
				$result = [
					'status' => false,
				];
			}
		} else {
			$result = [
				'status' => false
			];
		}
		echo json_encode($result);
		// if ($result[0]['status'] == false) {
		// 	$result[0]['status'] = false;
		// } else {
		// 	$result[0]['status'] = true;
		// }
		// var_dump($result);die();

		// echo $result;
	}

	public function get_list_timbang()
	{
		// $sum = $data->num_rows();
		// $result = $data->row();
		// // var_dump($data->result());die();
		// if ($sum > 0) {
		// 	if ($result->qty_makro == 0 || 0 >= $result->qty_makro) {

		// 		$this->db->set('sts_makro', 2)
		// 			->where('id', $result->id_formula_material)
		// 			->update('d_formula_material');
		// 		return $this->get_list_timbang();
		// 	} else {
		// 		$datas = array(
		// 			'status'				=> true,
		// 			'qty_makro' 			=> $result->qty_makro,
		// 			'material' 				=> $result->material,
		// 			'id_formula_material' 	=> $result->id_formula_material,
		// 		);
		// 	}
		// } else {
		// 	$datas = array(
		// 		'status'	=> false
		// 	);
		// }
		$data = $this->premix_makro_model->get_list_timbang($_GET['id_transaction'], 2);
		$cek = $this->db->select('status,sts_mikro,sts_makro')
			->from('t_rencana_produksi')
			->where('id_transaction', $_GET['id_transaction'])->get()->row();

		if ($data->num_rows() == 0) {
			if ($cek->sts_makro == 0) {
				if ($cek->sts_mikro == 1) {
					// QUERY MENGUBAH STATUS MIKRO dan status transaksi
					$this->db->set('status', 2)
						->set('sts_makro', 1)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				} else {
					$this->db->set('sts_makro', 1)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				}
			} elseif ($cek->sts_makro == 1 && $cek->sts_mikro == 1) {
				if ($cek->status >= 2) { //cek apakah status sudah dirubah
					//menampilkan informasi dosing selesai
					// $result['status'] = false;
				} else {
					//mengubah status transaksi dan menampilkan informasi dosing selesai
					$this->db->set('status', 2)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				}
			}
			$result['status'] = false;
			// else {
			// 	if ($data->num_rows() == 0) {
			// 		$result['status'] = false;
			// 	} else {
			// 		$result = $data->row_array();
			// 		$result['status'] = true;
			// 	}
			// }
		} else {
			$result = $data->row_array();
			$result['status'] = true;
		}
		echo json_encode($result);
	}

	public function post_timbang_material()
	{
		$result = $this->premix_makro_model->post_timbang_material();
		echo json_encode($result);
		// $cek = $this->premix_makro_model->cek_kdmaterial()->row_array();
		// if ($_POST['id_formula_material'] == $cek['id']) {
		// 	$_POST['qty_timbang_makro'] = floatval($_POST['qty_timbang_makro']);
		// 	$result = $this->premix_makro_model->post_timbang_material($_POST);
		// } else {
		// 	$result = [
		// 		'status' => false,
		// 		'msg' => 'Kode material salah'
		// 	];
		// }

		// echo json_encode($result);
	}

	public function cek_code()
	{
		$cek = $this->premix_makro_model->cek_kode();


		echo json_encode($cek);

		// var_dump($cek);die();

	}
}
