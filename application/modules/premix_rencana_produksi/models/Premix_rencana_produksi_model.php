<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Premix_rencana_produksi_model extends CI_Model
{
    public function get_rencana_produksi($date, $date2)
    {

        // var_dump($date);
        // // var_dump(date("Y-m-d",strtotime($dt->date)));
        // die();
        "SELECT *
        from t_rencana_produksi a
        where a.tanggal = '2021-07-01' ";

        $this->db->select('a.item,a.fml_name,a.fml_no,a.fml_transaction,a.id_transaction,a.prosen,a.status,a.tanggal,b.nama status_nm');
        $this->db->from('t_rencana_produksi a');
        $this->db->join('m_status b', 'b.id = a.status');
        $this->db->where('a.tanggal BETWEEN "' . $date . '" and "' . $date2 . '"');
        $this->db->where('a.active', 1);
        return $this->db->get();
    }

    public function get_timbang_tgl()
    {
        $this->db->select("max(updated_date) updated_date")
            ->from('d_dosing a')
            ->where('id_transaction', $_GET['id_transaction'])
            ->where('active', 1);
            // ->group_by('a.id');
        // $this->db->get();
        // var_dump($this->db->last_query());die();
        return $this->db->get();
    }

    public function get_data_material()
    {
        $this->db->select("(a.qty_timbang_makro + a.qty_timbang_mikro) total,(a.qty_timbang_makro + a.qty_timbang_mikro) - qty_material  diff,LPAD(a.`sequence` +0, 3, '0') seq,a.*");
        $this->db->where('a.id_transaction', $_GET['id_transaction']);
        $this->db->where('a.active', 1);
        return $this->db->get('d_dosing a');
    }

    public function get_example($no_transaction)
    {
        // "select LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material 
        // from t_rencana_produksi a
        // join d_formula_material b on  b.id_rencana_produksi = a.id 
        // join m_formula c on c.id = b.id_formula 
        // join m_material d on d.id = b.id_material 
        // join m_unit_satuan e on e.id = d.unit 
        // where a.no_transaction = '2021042701PMX001'";
        // $this->db->select("a.id, LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material, b.qty_timbang_mikro, b.qty_timbang_makro,  d.qty, (b.qty_timbang_mikro+b.qty_timbang_makro) as qty_jmltimbang, b.id_material, f.qty qty_timbang_total")
        //     ->from('t_rencana_produksi a')
        //     ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
        //     ->join('m_formula c', 'c.id = b.id_formula')
        //     ->join('m_material d', 'd.id = b.id_material')
        //     ->join('m_unit_satuan e ', 'e.id = d.unit')
        //     ->join('t_riwayat_stok f', 'f.id_produksi = a.id and f.id_material = d.id', 'left')
        //     ->where('a.id_transaction', $no_transaction)
        //     ->order_by('seq');
        $this->db->select("a.qty_prosen,LPAD(a.`sequence` +0, 3, '0') seq,(a.qty_timbang_makro + a.qty_timbang_mikro) dif,a.*")
            ->from('d_dosing a')
            ->where('id_transaction', $no_transaction)
            ->where('active', 1);
            // ->group_by('a.id');
        // $this->db->get();
        // var_dump($this->db->last_query());die();
        return $this->db->get();
    }

    public function post_proses_material()
    {
        // var_dump(round(800.10));die();
        $this->db->trans_begin();
        $this->db->select('urutan,tanggal')
            ->from('t_rencana_produksi')
            ->where('id_transaction', $_POST['id_transaction']);
        $max = $this->db->get()->row_array();
        $date = date('Ymd', strtotime($max['tanggal']));
        $urutan = $date . '' . str_pad($max['urutan'], 2, '0', STR_PAD_LEFT);

        for ($i = 0; $i < round($_POST['total_kg'] / 25); $i++) {
            $arr[] = array(
                'codes' => $urutan . '' . str_pad($i + 1, 2, '0', STR_PAD_LEFT),
                'id_transaction' => $_POST['id_transaction'],
                'no_pack' => $i + 1,
            );
        }

        $this->db->where('active', 1);
        $this->db->update_batch('d_dosing', $_POST['dosing'], 'id');

        $this->db->set('tgl_proses', date('Y-m-d H:i:s'));
        $this->db->set('status', 3);
        $this->db->where('id_transaction', $_POST['id_transaction']);
        $this->db->update('t_rencana_produksi');

        $this->db->insert_batch('t_riwayat_stok', $_POST['riwayat']);

        $this->db->insert_batch('t_qrcodes_premix_packing', $arr);

        $case = '';
        foreach ($_POST['proses'] as $key => $value) {

            $case .= ' WHEN material_kode = ' . $value['material_kd'] . ' THEN qty - ' . $value['qty'] . ' ';
            $id_mat[] = $value['material_kd'];
        }
        $ids = join("','", $id_mat);

        $update = "UPDATE m_material SET qty = CASE $case ELSE qty END WHERE material_kode IN('$ids')";
        $this->db->query($update);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->load->library('curl');
            $url = 'https://ismaf.mydmc.co.id/api/premixproduct';
            /* eCurl */
            $curl = curl_init($url);
            /* Set JSON data to POST */
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($_POST['post']));
            /* Define content type */
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            /* Return json */
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT,10);
            /* make request */
            $result = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $r = json_decode($result);
            curl_close($curl);
            if ($httpcode == 200) {
                if ($r->data == true) {
                    $this->db->trans_commit();
                    return [
                        'status' => true,
                        'msg' => 'Berhasil verifikasi premix'
                    ];
                } else {
                    return [
                        'status' => false,
                        'msg' => 'Gagal memverifikasi premix'
                    ];
                }
            } else {
                return [
                    'status' => false,
                    'msg' => 'Gagal memverifikasi premix'
                ];
            }
        }
    }

    public function post_proses_material2($data)
    {
        "select COALESCE(MAX(urutan+1), 1) AS urutan from t_rencana_produksi 
        where tanggal = '2021-04-27'";
        $this->db->select('urutan,tanggal')
            ->from('t_rencana_produksi')
            ->where('id_transaction', $_POST['id_transaction']);
        $max = $this->db->get()->row_array();
        $date = date('Ymd', strtotime($max['tanggal']));
        $urutan = $date . '' . str_pad($max['urutan'], 2, '0', STR_PAD_LEFT);
        var_dump($urutan);
        die();
        $case = '';
        foreach ($data['proses'] as $key => $value) {

            $case .= ' WHEN id = ' . $value['id_material'] . ' THEN qty - ' . $value['qty'] . ' ';
            $id_mat[] = $value['id_material'];
        }
        $this->db->insert_batch('t_riwayat_stok', $data['proses']);
        $ids = join("','", $id_mat);

        $update = "UPDATE m_material SET qty = CASE $case ELSE qty END WHERE id IN('$ids')";
        $this->db->query($update);
        $this->db->trans_begin();
        $this->db->set('status', 2)
            ->set('urutan', $max['urutan'])
            ->where('id', $_POST['id_produksi'])
            ->update('t_rencana_produksi');
        // var_dump(ceil((0.2)));die();
        for ($i = 0; $i < ceil($data['total_timbang'] / 25); $i++) {
            $arr[] = array(
                'codes' => $urutan . '' . str_pad($i + 1, 2, '0', STR_PAD_LEFT),
                'id_produksi' => $data['id_produksi'],
                'no_pack' => $i + 1,
            );
        }
        // var_dump($arr);die();
        $this->db->insert_batch('t_qrcodes_premix_packing', $arr);
        // var_dump($this->db->last_query());die();


        // var_dump($arr);die();
        // $c = $data['c'];
        // //$arr=();
        // for ($i = 0; $i < $c; $i++) {
        //     /*
        // 	$sql = "UPDATE
        //     m_material
        // 	set
        //     qty = qty - " . $data['qty_jmltimbang'][$i] . ",
        // 	where
        //     id = " . $data['id_material'][$i] . "";
        // 	$this->db->query($sql);
        // 	*/
        //     $arr[] = array(
        //         'id' => $data['id_material'][$i],
        //         'qty' => $data['qty_jmltimbang'][$i],

        //     );

        //     //$this->db->set('qty', //$data['qty_jmltimbang'][$i],false);
        //     //$this->db->where('id', $data['id_material'][$i]);
        //     //$this->db->update('m_material');
        // }
        // $this->db->update_batch('m_material', $arr, 'id');

        // //update status t_rencana_produksi, id=no_transaction

        // $this->db->set('status', 2);
        // $this->db->where('id', $data['no_transaction']);
        // $this->db->update('t_rencana_produksi');



        // var_dump($this->db->last_query());die();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'perubahan data berhasil'
            ];
        }
    }

    public function post_material_formula()
    {
        $this->db->trans_begin();
        $material = $this->db->get('m_material')->result_array();
        foreach ($material as $key => $value) {

            $mt[] = $value['material_kode'];
        }
        // var_dump($_POST['dosing']);die();
        foreach ($_POST['dosing'] as $key => $value) {
            if (!in_array($value['material_kd'], $mt)) {
                $mt_baru[] = array(
                    'material_kode' => $value['material_kd'],
                    'material_nm'   => $value['material_nm'],
                    'unit'          => 'KG',
                );
            }

            $_POST['dosing'][$key]['id_transaction'] = $_POST['transaction']['id_transaction'];
        }
        // var_dump($mt_baru);die();
        if (!empty($mt_baru)) {
            $this->db->insert_batch('m_material', $mt_baru);
        }

        $this->db->insert_batch('d_dosing', $_POST['dosing']);

        $this->db->insert('t_rencana_produksi', $_POST['transaction']);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menambahkan data'
            ];
        }
    }

    public function generate_id()
    {

        $this->db->select("LPAD(COALESCE(max(urutan+1),1), 3, '0') urutan")
            ->from('t_rencana_produksi')
            ->where('tanggal', date('Y-m-d'));
        return $this->db->get()->row();
        // $a = $this->db->get()->row();
        // var_dump($a);die();
    }

    public function delete()
    {
        $this->db->trans_begin();
        $this->db->set('active', 0);
        $this->db->where('id_transaction', $_POST['id_transaction']);
        $this->db->update('d_dosing');
        $this->db->set('active', 0);
        $this->db->where('id_transaction', $_POST['id_transaction']);
        $this->db->update('t_rencana_produksi');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'Berhasil menghapus data'
            ];
        }
    }
}
