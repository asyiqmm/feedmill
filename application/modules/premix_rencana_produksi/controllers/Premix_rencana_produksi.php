<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Premix_rencana_produksi extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->model('premix_rencana_produksi_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Rencana Produksi | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 6;
			$data['child_active'] = 7;
			$data['grandchild_active'] = null;
			$this->_render('premix_rencana_produksi_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_rencana_produksi()
	{
		$dt = DateTime::createFromFormat("d/m/Y", $_GET['date']);
		$date = $dt->format('Y-m-d');
		$dt2 = DateTime::createFromFormat("d/m/Y", $_GET['date2']);
		$date2 = $dt2->format('Y-m-d');
		$datas = $this->premix_rencana_produksi_model->get_rencana_produksi($date, $date2);
		// var_dump($datas->result());die();


		$obj = array("data" => $datas->result_array());
		echo json_encode($obj);
	}

	public function get_data_material()
	{
		$datas = $this->premix_rencana_produksi_model->get_data_material();
		// $url = 'https://ismaf.mydmc.co.id/api/formulapremixdetail?formula=' . $_GET['fml_transaction'];
		// $ch = curl_init($url);
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		// $r = curl_exec($ch);
		// curl_close($ch);

		// $result = json_decode($r, true);
		// $data = array();
		// if (isset($result['meta']['code']) == 200) {
		// 	foreach ($result['data'] as $key => $value) {
		// 		$result['data'][$key]['total'] = number_format(($value['prosen'] / 100) * $datas->row_array()['prosen'], 2, '.', '');
		// 		$result['data'][$key]['full_material'] = '[' . $value['material'] . '] ' . $value['material_name'] . '';
		// 		$result['data'][$key]['unit'] = 'KG';
		// 	}
		$obj = array("data" => $datas->result());
		// } else {
		// 	$obj['data'] = array();
		// }


		// var_dump($result['data']);
		// die();
		// foreach ($datas->result() as $key => $value) {
		// 	$data[] = array(
		// 		'id_produksi'		=> $value->id,
		// 		'id_material'		=> $value->id_material,
		// 		'seq' 				=> $value->seq,
		// 		'material' 			=> $value->material,
		// 		'unit_nm'		 	=> $value->unit_nm,
		// 		'qty_material' => $value->qty_material,
		// 		'qty_timbang' => $value->qty_timbang_total,
		// 		'qty_dif_timbang' => number_format((float)$value->qty_timbang_total - $value->qty_material, 2, '.', ''),
		// 		'qty_jmltimbang' => $value->qty_jmltimbang,
		// 		'qty_dif' => number_format((float)$value->qty_material - $value->qty_jmltimbang, 2, '.', ''),
		// 	);
		// }

		echo json_encode($obj);
	}

	public function get_example()
	{
		$datas = $this->premix_rencana_produksi_model->get_example($_GET['id_transaction']);
		// var_dump($datas->result());die();
		foreach ($datas->result() as $key => $value) {
			$data[] = array(
				'id_dosing'				=> $value->id,
				'id_transaction'	=> $value->id_transaction,
				'id_material'		=> $value->material_kd,
				'seq' 			=> $value->seq,
				'qty_prosen' 			=> $value->qty_prosen,
				'material' 		=> $value->material_nm,
				'unit_nm'		 	=> 'KG',
				'qty_material' 		=> $value->qty_material,
				'qty_timbang' 		=> $value->qty_timbang_mikro + $value->qty_timbang_makro,
				'qty_dif_timbang' 	=> number_format((float)($value->qty_timbang_mikro + $value->qty_timbang_makro) - $value->qty_material, 2, '.', ''),
				// 'tgl_timbang' 		=> $value->update_timbang,
				// 'qty_dif' 			=> number_format((float)$value->qty_material - $value->qty_jmltimbang, 2, '.', ''),
			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function get_timbang_tgl()
	{
		$datas = $this->premix_rencana_produksi_model->get_timbang_tgl();
		echo json_encode($datas->row());
	}


	public function post_proses_material()
	{
		$_POST['post'] = array(
			'notrans' 	=> $_POST['id_transaction'],
			'fml_no' 	=> $_POST['fml_transaction'],
			'ymd' 		=> date("Y", strtotime("+1 year")) . '' . date('md'),
			// 'ymd' 		=> date('Ymd'),
			'item' 		=> $_POST['item'],
			'qty_prod' 	=> $_POST['qty_prod'],
		);
		$_POST['total_kg'] = 0;
		foreach ($_POST['proses'] as $key => $value) {
			$_POST['post']['data'][$key] = array(
				'seq'		=> $value['seq'],
				'material'	=> $value['material_kd'],
				'prosen'	=> $value['qty_prosen'],
				'qty'		=> $value['qty'],
			);
			$_POST['dosing'][$key] = array(
				'qty_timbang_proses'	=> $value['qty'],
				'tgl_timbang_proses'	=> date('Y-m-d H:i:s'),
				'dif'					=> $value['dif'],
				'id'					=> $value['id'],
			);
			$_POST['riwayat'][$key] = array(
				'id_transaction' 	=> $_POST['id_transaction'],
				'id_material' 	=> $value['material_kd'],
				'qty' 			=> $value['qty'],
			);
			$_POST['total_kg'] = $_POST['total_kg'] + $value['qty'];
		}
		// print_r(json_encode($_POST['post']));die();
		
		
		$proses = $this->premix_rencana_produksi_model->post_proses_material();

		echo json_encode($proses);
	}

	public function get_data_formula()
	{
		$url = 'https://ismaf.mydmc.co.id/api/formulapremixdetail?formula=' . $_GET['formula'];
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$r = curl_exec($ch);
		curl_close($ch);

		$result = json_decode($r, true);
		// var_dump($result);die();
		if ($result['meta']['code'] == 200) {
			// $fr = $result;
			foreach ($result['data'] as $key => $value) {
				$result['data'][$key]['material_name'] = '[' . $result['data'][$key]['material'] . '] ' . $result['data'][$key]['material_name'] . '';
				$result['data'][$key]['material_nm'] = $value['material_name'];
			}
			$fr = array("data" => $result['data']);
		} else {
			$fr['data'] = array();
		}
		echo json_encode($fr);

		// var_dump($fr);die();
	}

	public function post_material_formula()
	{
		$urutan = $this->premix_rencana_produksi_model->generate_id()->urutan;
		$_POST['transaction']['tanggal'] = date('Y-m-d');
		$_POST['transaction']['id_transaction'] = date('Ymd', strtotime("+1 year")) . '01PMX' . $urutan;
		$_POST['transaction']['urutan'] = $urutan;
		$proses = $this->premix_rencana_produksi_model->post_material_formula($_POST);

		echo json_encode($proses);
	}

	public function delete()
	{
		$delete = $this->premix_rencana_produksi_model->delete();
		echo json_encode($delete);
	}

	public function list_formula()
	{
		// $originalDate = $_GET['start'];
		$start = DateTime::createFromFormat('d/m/Y', $_GET['start'])->format('Ymd');
		$end = DateTime::createFromFormat('d/m/Y', $_GET['end'])->format('Ymd');
		// $b = $dateTime->format('Ymd');
		$coba = date('Ymd', strtotime($start));

		$url = 'https://ismaf.mydmc.co.id/api/formulapremixmaster';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$r = curl_exec($ch);
		curl_close($ch);
		$array = array();
		$result = json_decode($r, true);
		foreach ($result['data'] as $key => $value) {

			$result['data'][$key]['tgl'] = substr($value['notrans'], 0, 8);
			$dt = substr($value['notrans'], 0, 8);
			$tgl = date('Ymd', strtotime($dt));
			if (($tgl >= $start) && ($tgl <= $end)) {

				$array[] = array(
					'notrans' 	=> $value['notrans'],
					'fml_no' 	=> $value['fml_no'],
					'item_name' => $value['item_name'],
					'tgl' 		=> $tgl,
					'item'		=> $value['item']
				);
			}
		}
		echo json_encode($array);
	}
}
