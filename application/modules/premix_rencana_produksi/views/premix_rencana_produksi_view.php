<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Rencana Produksi Premix</h3>
                        </div>
                        <!-- /.card-header -->
                        <style>
                        </style>
                        <div class="card-body">
                            <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="dt-buttons">
                                            <button data-toggle="modal" data-target="#modal-premix-formula" class="dt-button btn btn-sm btn-primary btn-add">Tambah Material Produksi</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <div class="dataTables_filter">
                                            <label>Tanggal:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dt-buttons ">
                                            <div class="form-inline justify-content-center">
                                                <div class="form-group">
                                                    <div class="tgl" id="datetimepickerawal" data-target-input="nearest">
                                                        <input type="text" class="form-control form-control-sm datetimepicker-input" value="<?php echo date('d/m/Y') ?>" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickerawal" id="bulan" data-target="#datetimepickerawal" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    ~
                                                </div>
                                                <div class="form-group">
                                                    <div class="tgl" id="datetimepickerakhir" data-target-input="nearest">
                                                        <input type="text" class="form-control form-control-sm datetimepicker-input" value="<?php echo date('d/m/Y') ?>" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepickerakhir" id="bulan2" data-target="#datetimepickerakhir" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class=" date dataTables_filter" id="datetimepicker1" data-target-input="nearest">

                                                <input type="text" class="form-control form-control-sm datetimepicker-input" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepicker1" id="bulan" data-target="#datetimepicker1" />

                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="dataTables_filter">
                                            <label>Search:<input type="search" id="tabel_rencana_produksi_filter" class="form-control form-control-sm" placeholder="" aria-controls="tabel_rencana_produksi"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <img src="" alt=""> -->
                            <table id="tabel_rencana_produksi" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>No Transaction</th>
                                        <th>Date</th>
                                        <th>Formula No</th>
                                        <th>Material Name</th>
                                        <th class="text-center">Qty</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modal-premix">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Material Formula</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_penerimaan_sparepart" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">

                    <div class="table-responsive">

                        <table id="tabel_material_premix" width="100%" class="table table-bordered">
                            <thead class="text-center">
                                <tr width="100%">
                                    <th>Seq</th>
                                    <th>Material</th>
                                    <th>Unit</th>
                                    <th>Qty Prosen</th>
                                    <th>Qty Timbang</th>
                                    <th>Dif</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Sub Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!-- <button type="Submit" value="Submit" id="btnSubmit" class="btn btn-primary">Save</button> -->
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-premix-proses" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Proses Material </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_proses_material" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="">Premix No:</label>
                            <input id="proses_idtrans" name="id_transaction" type="text" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Formula Name:</label>
                            <input type="text" id="proses_formulanm" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Formula No:</label>
                            <input type="text" id="proses_formulano" name="fml_transaction" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Item:</label>
                            <input id="proses_item" name="item" type="text" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="">Qty:</label>
                            <input id="proses_prosen" type="text" class="form-control form-control-sm" name="qty_prod" readonly>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Waktu Selesai Timbang:</label>
                            <input id="proses_tgl" type="text" class="form-control form-control-sm" readonly>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tabel_material_premix_proses" width="100%" class="table table-bordered">
                            <thead class="">
                                <tr width="100%">
                                    <th>Seq</th>
                                    <th>Material</th>
                                    <th>Unit</th>
                                    <th>Qty Material</th>
                                    <th>Qty Timbang Aktual</th>
                                    <th>Qty Timbang</th>
                                    <th>Dif</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="3">Sub Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th id="total_timbang">
                                    </th>
                                    <th id="total_dif"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnProccess" class="btn btn-primary">Proses</button>
                </div>
                <!-- <input type="hidden" name="id_transaction" id="no_produksi">
                <input type="hidden" id="input_timbang" name="total_timbang">
                <input type="hidden" id="tgl_produksi" name="tgl_produksi"> -->
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-premix-formula">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat Rencana Produksi Baru </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="post_material_formula" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="">Start Date:</label>
                            <div class=" date dataTables_filter" id="datetimepicker2" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-toggle="datetimepicker" value="<?php echo date('d/m/Y', strtotime('-1 months')); ?>" data-target="#datetimepicker2" id="fml_start" data-target="#datetimepicker2" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="">End Date:</label>
                            <div class=" date dataTables_filter" id="datetimepicker3" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#datetimepicker3" id="fml_end" data-target="#datetimepicker3" />
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="">Nama Formula:</label>
                            <select name="transaction[fml_transaction]" id="fml_nm" class="select2bs4" required>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Item:</label>
                            <input type="text" id="item" name="transaction[item]" class="form-control" readonly>
                            <input type="hidden" id="fml_no" name="transaction[fml_no]" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                            <label for="">Kg:</label>
                            <input type="text" class="qtytimbang form-control" name="transaction[prosen]" id="total_prosen" value="800">
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table id="tabel_material_formula" width="100%" class="table table-bordered">
                            <thead class="">
                                <tr width="100%">
                                    <th>Seq</th>
                                    <!-- <th>Kode</th> -->
                                    <th>Material</th>
                                    <th class="text-nowrap">Qty Prosen</th>
                                </tr>
                            </thead>

                        </table>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <button type="Submit" value="Submit" id="btnTambah" class="btn btn-primary" disabled>Tambah</button>
                </div>
                <input type="hidden" name="transaction[fml_name]" id="fml_name">
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    $(function() {
        // datatable_premix();
        // list_formula();
        $('.tgl').datetimepicker({
            format: 'DD/MM/YYYY',
            autoclose: true,
            // todayHighlight: true,
            minuteStepping: 1, //set the minute stepping

            // minDate:'1900/1/1',               //set a minimum date  <---- HERE 
            maxDate: moment(),
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
        $('.qtytimbang').inputmask('decimal', {
            allowMinus: false,
            integerDigits: 9,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
        });
    });

    function list_formula() {
        var formula = $('#fml_no').val();
        var name = $('#fml_nm').val();
        jQuery.ajax({
            type: "GET",
            url: "https://ismaf.mydmc.co.id/api/formulapremixmaster?formula=" + formula + "&name=" + name, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.data.length; index++) {
                    option += '<option value=' + data.data[index].notrans + ' fml_no=' + data.data[index].fml_no + '>' + data.data[index].item_name + '</option>';
                }
                if (data.meta.code == 200) {
                    showToast(data.meta.message);
                    $('#fml_nm').html(option);
                } else {
                    showToastWarning(data.meta.message);
                }

            },
            error: function(xhr, status, error) {
                showToastWarning('error')
            }

        });
    };
    $("#datetimepicker2,#datetimepicker3").on("change.datetimepicker", ({
        date
    }) => {
        var start = $('#fml_start').val();
        var end = $('#fml_end').val();
        jQuery.ajax({
            type: "GET",
            url: "premix_rencana_produksi/list_formula?start=" + start + "&end=" + end, // the method we are calling
            dataType: "json",
            success: function(data) {
                var option = '<option></option>';
                for (let index = 0; index < data.length; index++) {
                    option += '<option value=' + data[index].notrans + ' item=' + data[index].item + ' fml_no=' + data[index].fml_no + '>' + data[index].item_name + '</option>';
                }
                $('#fml_nm').html(option);

                // if (data.meta.code == 200) {
                //     showToast(data.meta.message);
                // } else {
                //     showToastWarning(data.meta.message);
                // }

            },
            error: function(xhr, status, error) {
                // showToastWarning('error')
            }

        });
    });

    $("#datetimepickerawal").on("change.datetimepicker", ({
        date
    }) => {
        var date2 = $('#bulan2').val();

        datatable_premix(moment(date).format("DD/MM/YYYY"), date2);
    });

    $("#datetimepickerakhir").on("change.datetimepicker", ({
        date
    }) => {
        var date1 = $('#bulan').val();
        datatable_premix(date1, moment(date).format("DD/MM/YYYY"));
    });


    function datatable_premix(date1, date2) {
        $('#tabel_rencana_produksi').DataTable().clear();
        // $('#daily_report_table_body').DataTable().destroy();
        $('#tabel_rencana_produksi').empty();
        var siteTable = $('#tabel_rencana_produksi').DataTable({
            "ajax": 'Premix_rencana_produksi/get_rencana_produksi?date=' + date1 + '&date2=' + date2,
            "paging": false,
            "lengthChange": false,
            "destroy": true,
            searching: true,
            dom: 't',
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [0, "asc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            rowCallback: function(row, data, index) {
                if (data.status == 3) {
                    $(row).find('td').addClass('bg-success');
                    $(row).find('td').css('color', 'white');
                }
            },
            columns: [{
                    data: 'id_transaction',
                    className: 'details-control align-middle text-nowrap',
                    width: '1%'
                },
                {
                    data: 'tanggal',
                    className: 'details-control align-middle text-nowrap text-center',
                    width: '1%'
                },
                {
                    data: 'fml_transaction',
                    className: 'details-control',
                    width: '1%'
                },
                {
                    data: 'fml_name',
                    className: 'details-control align-middle',
                    width: '10%'
                },
                {
                    data: 'prosen',
                    className: 'align-middle text-center text-nowrap',
                    width: '1%'
                },
                {
                    data: 'status_nm',
                    className: 'details-control align-middle text-nowrap text-center',
                    width: '1%'
                },
                {
                    data: 'status',
                    className: 'details-control text-center align-middle text-nowrap',
                    width: '1%',
                    "render": function(data, type, row, meta) {
                        if (row.status == 2) {
                            var result = '<button class="btn btn-outline-success btn-sm btn-proses">Proses</button>';
                        } else if (row.status == 1) {
                            var result = '<button class="btn btn-outline-danger btn-sm btn-hapus">Hapus</button>';
                        } else {
                            var result = '';
                        }
                        return result;
                    }
                },
            ],
        });



        $('#tabel_rencana_produksi tbody').on('dblclick', 'tr', function() {
            var table = $('#tabel_material_premix').DataTable({
                ordering: false,
                "destroy": true,

            });
            table.clear();
            // table.draw();
            var data = siteTable.row(this).data();
            if (data) {
                $('#modal-premix').modal('show');
                var table = $('#tabel_material_premix').DataTable({
                    "oLanguage": {
                        "sEmptyTable": "Gagal memuat data, coba sesaat lagi..."
                    },
                    "ajax": 'Premix_rencana_produksi/get_data_material?id_transaction=' + data.id_transaction,
                    "paging": false,
                    "destroy": true,
                    "searching": false,
                    "info": false,
                    responsive: true,
                    ordering: false,
                    initComplete: function(row, datas) {
                        // if (data.status != 2) {
                        //     table.column(4).visible(false);
                        //     table.column(5).visible(false);
                        // }
                        this.api().buttons().container()
                            .appendTo($('.col-md-6:eq(0)', this.api().table().container()));

                    },
                    rowCallback: function(row, data, index) {
                        // if (data.diff == 0) {
                        //     $(row).css('background-color', 'white');
                        // } else {
                        //     $(row).css('background-color', '#ea8685');
                        // }
                    },
                    "fnDrawCallback": function(oSettings) {
                        $($.fn.dataTable.tables(true)).DataTable()
                            .columns.adjust();
                    },
                    "footerCallback": function(row, data, start, end, display) {
                        var api = this.api(),
                            data;
                        var intVal = function(i) {
                            return typeof i === 'string' ?
                                i.replace(',', '').replace(/[\$,]/g, '.') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };
                        total = api
                            .column(3)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 0);

                        totaltimbang = api
                            .column(4)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 0);
                        $(api.column(4).footer()).html(
                            totaltimbang
                        );

                        totaldif = api
                            .column(5)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 0);
                        $(api.column(5).footer()).html(
                            totaldif
                        );
                        //Total over this page
                        pageTotal = api
                            .column(3, {
                                page: 'current'
                            })
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 2);

                        //Update footer
                        $(api.column(3).footer()).html(
                            total
                        );
                    },
                    columns: [{
                            data: 'seq',
                            className: 'details-control align-middle text-nowrap text-center',
                            width: '1%',
                            orderable: false
                        },
                        {
                            data: 'material_nm',
                            className: 'details-control align-middle text-nowrap',
                            width: '15%',
                            orderable: false

                        },
                        {
                            // data: 'unit',
                            className: 'details-control text-center text-nowrap',
                            width: '1%',
                            orderable: false,
                            "render": function(data, type, row, meta) {
                                return 'KG';
                            }
                        },
                        {
                            data: 'qty_material',
                            className: 'text-center details-control align-middle text-nowrap',
                            width: '1%',
                            orderable: false
                        },
                        {
                            data: 'total',
                            className: 'text-center details-control align-middle text-nowrap',
                            width: '1%',
                            orderable: false,
                        },
                        {
                            data: 'diff',
                            className: 'text-center details-control align-middle',
                            width: '1%',
                            orderable: false,
                        },
                    ],
                });
            }

        });

        $('#tabel_rencana_produksi tbody').on('click', '.btn-proses', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            console.log('yahahaha', datas);
            if (datas.status == 2) {
                $('#no_produksi').val(datas.id_transaction);
                $('#tgl_produksi').val(datas.tanggal);

                $('#proses_item').val(datas.item);
                $('#proses_idtrans').val(datas.id_transaction);
                $('#proses_formulano').val(datas.fml_transaction);
                $('#proses_formulanm').val(datas.fml_name);
                $('#proses_prosen').val(datas.prosen);
                $.ajax({
                    url: <?php base_url() ?> 'premix_rencana_produksi/get_timbang_tgl?id_transaction=' + datas.id_transaction,
                    method: "GET",
                    dataType: "json",
                    beforeSend: function() {
                        $('#proses_tgl').val('');
                    },
                    success: function(data) {
                        $('#proses_tgl').val(data.updated_date);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {}
                })

                var table = $('#tabel_material_premix_proses').DataTable({
                    ordering: false,
                    "destroy": true,

                });
                table.clear();
                $('#modal-premix-proses').modal('show');
                $('#btnProccess').attr('disabled', true);
                // $('#modal-premix-proses').modal({backdrop: 'static', keyboard: false})
                var table = $('#tabel_material_premix_proses').DataTable({
                    "ajax": 'Premix_rencana_produksi/get_example?id_transaction=' + datas.id_transaction,
                    "paging": false,
                    "destroy": true,
                    "searching": false,
                    "info": false,
                    responsive: true,
                    ordering: false,
                    initComplete: function(row, data) {
                        // $('#')
                        $('#btnProccess').attr('disabled', false);
                        this.api().buttons().container()
                            .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
                        $('.qtytimbang').inputmask('decimal', {
                            allowMinus: false,
                            integerDigits: 9,
                            digits: 2,
                            digitsOptional: false,
                            placeholder: '0',
                        });
                        $('.paynow').keyup(function() {
                            var $ThisCheck = $(this).parents('tr').find('.dif');
                            var $difnow = $(this).parents('tr').find('.difnow');
                            var $qty_mat = $(this).parents('tr').find('.qty_material');
                            var dif = $(this).val() - $qty_mat.text(); //different of proses
                            $ThisCheck.text(dif.toFixed(2));
                            $difnow.val(dif.toFixed(2));

                            if (dif !== 0) {
                                $(this).parents('tr').css('background-color', '#e17055');
                                $(this).parents('tr').css('color', 'white');
                            } else {
                                $(this).parents('tr').css('background-color', 'white');
                                $(this).parents('tr').css('color', 'black');
                            }
                            CalcFooter();
                        });

                        // $('.dif').on('DOMSubtreeModified', function() {
                        //     console.log('changed');
                        // });
                    },
                    rowCallback: function(row, data, index) {
                        if (data.qty_dif_timbang == 0) {
                            $(row).css('background-color', 'white');
                            $(row).css('color', 'black');
                        } else {
                            $(row).css('background-color', '#e17055');
                            $(row).css('color', 'white');
                        }
                    },
                    "fnDrawCallback": function(oSettings) {
                        $($.fn.dataTable.tables(true)).DataTable()
                            .columns.adjust();
                    },
                    "footerCallback": function(row, data, start, end, display) {
                        var api = this.api(),
                            data;
                        var intVal = function(i) {
                            return typeof i === 'string' ?
                                i.replace(',', '').replace(/[\$,]/g, '.') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };
                        total = api
                            .column(3)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 0);

                        totalb = api
                            .column(4)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                $('#input_timbang').val(result.toFixed(2));
                                return result.toFixed(2);
                            }, 0);

                        totalc = api
                            .column(5)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                $('#input_timbang').val(result.toFixed(2));
                                return result.toFixed(2);
                            }, 0);

                        totaldif = api
                            .column(6)
                            .data()
                            .reduce(function(a, b) {
                                var x = parseFloat(a);
                                var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                                let result = x + y;
                                return result.toFixed(2)
                            }, 0);


                        //Total over this page
                        pageTotal = api
                            .column(3, {
                                page: 'current'
                            })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 2);

                        //Update footer
                        $(api.column(3).footer()).html(
                            total
                        );
                        $(api.column(4).footer()).html(
                            totalb
                        );

                        $(api.column(5).footer()).html(
                            totalc
                        );

                        $(api.column(6).footer()).html(
                            totaldif
                        );
                    },
                    columns: [{
                            name: 'Seq',
                            data: 'seq',
                            className: 'details-control align-middle text-nowrap',
                            width: '1%',
                            orderable: false
                        },
                        {
                            name: 'Material',
                            data: 'material',
                            className: 'details-control align-middle text-nowrap',
                            width: '15%',
                            orderable: false

                        },
                        {
                            name: 'Unit',
                            data: 'unit_nm',
                            className: 'text-nowrap text-center align-middle',
                            width: '1%',
                            orderable: false
                        },
                        {
                            name: 'Qty Material',
                            data: 'qty_material',
                            className: 'text-right qty_material align-middle text-nowrap',
                            width: '1%',

                        },
                        {
                            data: 'qty_timbang',
                            className: 'text-right align-middle text-nowrap',
                            width: '1%',

                        },
                        {
                            name: 'Qty Timbang',
                            data: 'qty_timbang',
                            className: 'text-right details-control align-middle text-nowrap',
                            width: '1%',
                            orderable: false,
                            "render": function(data, type, row, meta) {
                                return '<input type="hidden" name="proses[' + meta.row + '][seq]" value="' + row.seq + '"><input type="hidden" name="proses[' + meta.row + '][qty_prosen]" value="' + row.qty_prosen + '"><input type="hidden" class="difnow" name="proses[' + meta.row + '][dif]" value="' + row.qty_dif_timbang + '"><input class="qtytimbang paynow" title="double click to type" type="text" size="5"  name="proses[' + meta.row + '][qty]" value="' + row.qty_timbang + '">  <input type="hidden" name="proses[' + meta.row + '][id]" value="' + row.id_dosing + '"> <input type="hidden" name="proses[' + meta.row + '][material_kd]" value="' + row.id_material + '">';
                            }
                        },
                        {
                            name: 'Dif',
                            data: 'qty_dif_timbang',
                            className: 'text-right dif align-middle text-nowrap',
                            width: '1%',
                            orderable: false,
                        },
                    ],
                });
            }


        });

        $('#tabel_rencana_produksi tbody').on('click', '.btn-hapus', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            if (datas.status == 1) {
                swal
                    .fire({
                        title: "Perhatian",
                        html: "Hapus produksi  <b>[" + datas.fml_no + "] " + datas.fml_name + "</b>?",
                        // type: "question",
                        icon: 'warning',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: "#e74c3c",
                        confirmButtonText: "Hapus",
                        cancelButtonText: "Tidak"
                    })
                    .then(function(result) {
                        if (result.value) {
                            $.ajax({
                                url: <?php base_url() ?> 'premix_rencana_produksi/delete',
                                method: "POST",
                                data: {
                                    id_transaction: datas.id_transaction
                                },
                                dataType: "json",
                                beforeSend: function() {
                                    swal.fire({
                                        title: 'Please Wait..!',
                                        text: 'Is working..',
                                        onOpen: function() {
                                            swal.showLoading()
                                        }
                                    })
                                },
                                success: function(data) {
                                    if (data.status) {
                                        swal.fire({
                                            icon: 'success',
                                            title: data.msg,
                                            showConfirmButton: false,
                                            timer: 2000
                                        });
                                        $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                                    } else {
                                        swal.hideLoading();
                                        swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                                    }
                                },
                                complete: function() {
                                    swal.hideLoading();
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    swal.hideLoading();
                                    swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
                                }
                            })
                        } else {}
                    });
            }
        });

        $('#modal-premix').on('hidden.bs.modal', function(e) {
            var table = $('#tabel_material_premix').DataTable();
            table.clear();
            table.draw();
        })
    }

    $('#fml_nm').on('change', function() {
        $('#fml_name').val($('option:selected', this).text());
        var nm = $('option:selected', this).attr('fml_no');
        var item = $('option:selected', this).attr('item');
        $('#fml_no').val(nm);
        $('#item').val(item);
        var table = $('#tabel_material_formula').DataTable();
        table.clear();
        table.draw();
        table = $('#tabel_material_formula').DataTable({
            "oLanguage": {
                "sEmptyTable": "Data Kosong..."
            },
            "ajax": 'premix_rencana_produksi/get_data_formula?formula=' + $(this).val(),
            "paging": false,
            "lengthChange": false,
            "destroy": true,
            searching: false,
            "info": false,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            // responsive: true,
            ordering: false,
            initComplete: function(row, datas) {

                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));

            },
            // "fnDrawCallback": function(oSettings) {
            //     $($.fn.dataTable.tables(true)).DataTable()
            //         .columns.adjust();
            // },
            // "footerCallback": function(row, data, start, end, display) {

            // },
            columns: [{
                    data: 'seq',
                    className: 'details-control align-middle text-nowrap text-center',
                    width: '1%',
                    orderable: false
                },

                {
                    data: 'material_name',
                    className: 'details-control align-middle text-nowrap',
                    width: '15%',
                    orderable: false

                },
                {
                    data: 'prosen',
                    className: 'text-center details-control align-middle prosen',
                    width: '15%',
                    orderable: false,
                    "render": function(data, type, row, meta) {
                        var total = (row.prosen / 100) * $('#total_prosen').val();
                        var result = "<input type='hidden' name='dosing[" + meta.row + "][qty_prosen]' value='" + row.prosen + "'><input type='hidden' name='dosing[" + meta.row + "][sequence]' value='" + row.seq + "'><input type='hidden' name='dosing[" + meta.row + "][material_nm]' value='" + row.material_nm + "'><input type='hidden' name='dosing[" + meta.row + "][material_kd]' value='" + row.material + "'><input type='hidden' name='dosing[" + meta.row + "][qty_material]' value=" + total.toFixed(2) + ">" + total.toFixed(2);
                        return result;
                        // return '<input class="qtytimbang paynow" title="double click to type" type="text" size="5"  name="proses[' + meta.row + '][qty]" value="' + row.qty_jmltimbang + '" readonly="true" ondblclick="this.readOnly=false">  <input type="hidden" name="proses[' + meta.row + '][id_produksi]" value="' + datas.id_produksi + '"> <input type="hidden" name="proses[' + meta.row + '][id_material]" value="' + row.id_material + '">';

                    }
                },
            ],
        });
    })

    $('#tabel_rencana_produksi_filter').keyup(function() {
        $('#tabel_rencana_produksi').DataTable().search(
            $(this).val()
        ).draw();

    });


    $('#fml_nm').on('change', function() {
        if ($('#total_prosen').val() > 0 && $(this).val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
    });

    $('#total_prosen').keyup(function() {
        var tot_prosen = $(this).val()
        if (tot_prosen > 0 && $('#fml_nm').val() !== "") {
            $('#btnTambah').attr('disabled', false);
        } else {
            $('#btnTambah').attr('disabled', true);
        }
        var tot = 0;
        $('#tabel_material_formula').DataTable().$('.prosen').each(function(index, Obj) {
            var value = parseFloat($(this).text().replace(',', ''));
            var data = $('#tabel_material_formula').DataTable().row(index).data();
            var total = (data.prosen / 100) * tot_prosen;
            var $ThisCheck = $(this).parents('tr').find('.prosen');
            var result = "<input type='hidden' name='dosing[" + index + "][qty_prosen]' value='" + data.prosen + "'><input type='hidden' name='dosing[" + index + "][sequence]' value='" + data.seq + "'><input type='hidden' name='dosing[" + index + "][material_nm]' value='" + data.material_nm + "'><input type='hidden' name='dosing[" + index + "][material_kd]' value='" + data.material + "'><input type='hidden' name='dosing[" + index + "][qty_material]' value=" + total.toFixed(2) + ">" + total.toFixed(2);
            $ThisCheck.html(result);
            if (!isNaN(total)) tot += total;
        });
    });

    function CalcFooter() {
        // var amtPage = 0;
        var amtTotal = 0;
        var difTotal = 0;

        var Sum = 0;

        $('#tabel_material_premix_proses').DataTable().$('.paynow').each(function(index, Obj) {
            var value = parseFloat($(this).val().replace(',', ''));

            if (!isNaN(value)) amtTotal += value;
        });

        $('#tabel_material_premix_proses').DataTable().$('.dif').each(function(index, Obj) {

            var value = parseFloat($(this).text().replace(',', ''));
            if (!isNaN(value)) difTotal += value;
        });
        $('#total_timbang').text(
            amtTotal.toFixed(2)
        );

        $('#input_timbang').val(
            amtTotal.toFixed(2)
        );

        $('#total_dif').text(
            difTotal.toFixed(2)
        );
    }

    $('#form_proses_material').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> 'premix_rencana_produksi/post_proses_material',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnProccess').attr('disabled', true);

            },
            success: function(data) {

                $('#btnProccess').attr('disabled', false);
                if (data.status) {
                    showToast(data.msg);
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                    $("#modal-premix-proses").modal("hide");
                } else {
                    showToastError(data.msg);
                };

            }
        });
    });

    $('#post_material_formula').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_rencana_produksi/post_material_formula',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnTambah').attr('disabled', true);
                swal.fire({
                    title: 'Tunggu..!',
                    text: 'Memproses..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                $('#btnTambah').attr('disabled', false);
                if (data.status) {
                    swal.hideLoading();

                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $($.fn.dataTable.tables(true)).DataTable().ajax.reload();
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };
                $('#modal-premix-formula').modal('hide');
            },
            error: function() {
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
            }
        });
    });
</script>