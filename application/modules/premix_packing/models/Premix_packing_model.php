<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Premix_packing_model extends CI_Model
{
    public function get_list_packing()
    {
        $this->db->select("COUNT(a.codes) pack,a.id_transaction,b.tanggal,b.fml_name,b.fml_no,c.nama as status,b.prosen")
            ->from('t_qrcodes_premix_packing a')
            ->join('t_rencana_produksi b','b.id_transaction = a.id_transaction')
            ->join('m_status c','c.id = b.status')
            ->where('b.status',3)
            ->group_by('a.id_transaction');
        // $this->db->get();
        // var_dump($this->db->last_query());die();
        return $this->db->get();
    }

    public function get_codes($id)
    {
        $this->db->where('id_transaction',$id);
        return $this->db->get('t_qrcodes_premix_packing');
    }
}
