<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Premix_packing extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('premix_packing_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Packing | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 6;
			$data['child_active'] = 12;
			$data['grandchild_active'] = null;
			$this->_render('premix_packing_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_list_packing()
	{
		$datas = $this->premix_packing_model->get_list_packing();
		// var_dump($datas->result());die();
		// $data = array();
		// foreach ($datas->result() as $key => $value) {
		// 	if ($value->proses_timbang == 0 && $value->belum_timbang == 0 && $value->status != 2) {
		// 		$status = 'COMPLETED';
		// 		$btn = '<button class="btn btn-outline-success btn-sm btn-proses">Proses</button>';
		// 	}elseif ($value->status == 2) {
		// 		$btn = '';
		// 		$status = 'COMPLETED';
		// 	}else {
		// 		$status = 'READY';
		// 		$btn = '';
		// 	}
		// 	$data[] = array(
		// 		'no_transaction'	=> $value->no_transaction,
		// 		'tanggal'			=> $value->tanggal,
		// 		'formula_no'		=> $value->formula_no,
		// 		'formula_name'		=> $value->formula_name,
		// 		'qty'				=> $value->qty,
		// 		'status_nm'			=> $status,
		// 		'btn'				=> $btn,
		// 		'status'			=> $value->status,
		// 	);
		// }
		$obj = array("data" => $datas->result_array());
		echo json_encode($obj);
	}
	
	public function get_data_material()
	{
		$datas = $this->premix_rencana_produksi_model->get_data_material($_GET['no_transaction']);
		$data = array();
		foreach ($datas->result() as $key => $value) {
			$data[] = array(
				'id_material'		=> $value->id_material,
				'seq' => $value->seq,
				'material' => $value->material,
				'unit_nm' => $value->unit_nm,
				'qty_material' => $value->qty_material,
				'qty_jmltimbang' => $value->qty_jmltimbang,
				'qty_dif' => number_format((float)$value->qty_material - $value->qty_jmltimbang, 2, '.', ''),
			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	
	public function post_proses_material()
	{
		var_dump($_POST['proses']);die();
		$_POST['no_transaction'] = $_POST['no_transaction'];
		$id_material[]=$_POST['id_material'];
		$qty_jmltimbang[]=$_POST['qty_jmltimbang'];
		$c=count($_POST['id_material']);
		$_POST['c']=count($_POST['id_material']);
		$_POST['id_material']=$_POST['id_material'];
		$_POST['qty_jmltimbang']=$_POST['qty_jmltimbang'];
		
		/*
		for($i=0;$i<$c;$i++)
			{
			$arr[]=array(
				'id'=>$_POST['id_material'][$i],
				'qty'=>$_POST['qty_jmltimbang'][$i],
				
				);
			//echo"".$_POST['id_material'][$i]."<br>";
			}
		*/
		//$result=$this->db->update_batch('m_material', $arr, 'id');
		$result = $this->premix_rencana_produksi_model->post_proses_material($_POST);


		echo json_encode($result);
	}

	public function print($id = null)
	{
		$data['codes'] = $this->premix_packing_model->get_codes($id)->result();
		// var_dump($result->result());die();
		$this->load->view('print', $data);
	}
}
