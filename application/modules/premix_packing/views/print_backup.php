<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->


<?php
require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
require_once(APPPATH . "libraries/phpqrcode/qrconfig.php");

?>

<script>
    window.onload = function() {
        window.print();
    }
</script>

<style type="text/css" media="print">
    /* @page {
        size: 58mm 58mm;
    } */



    /*
@media print {
  @page { margin: 0; size: 58mm 58mm; }
  body { margin: 1.6cm; }
}*/
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid">


            <?php

            //echo"<center><H1>Cetak $id </H1>";
            //echo count($detail);
            //echo"<center>";
            foreach ($codes as $value) {
                $content = $value->codes;
                ob_start();
                QRcode::png($content);
                $result_qr_content_in_png = ob_get_contents();
                ob_end_clean();
                // PHPQRCode change the content-type into image/png... we change it again into html
                header("Content-type: text/html");
                $result_qr_content_in_base64 =  base64_encode($result_qr_content_in_png);
                echo '<img width="100px" src="data:image/jpeg;base64,' . $result_qr_content_in_base64 . '" />';
                echo "<br>";
                echo $value->codes;
                echo "<br>
                " . date('d/m/Y', strtotime($value->created_date)) . "
                <br><br><br>";
            }

            echo "<br><br></center>";
            ?>
        </div>
    </section>

</div>