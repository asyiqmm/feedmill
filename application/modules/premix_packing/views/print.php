<?php
require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
require_once(APPPATH . "libraries/phpqrcode/qrconfig.php");

?>

<?php
foreach ($codes as $value) {
    $content = $value->codes;
    ob_start();
    QRcode::png($content);
    $result_qr_content_in_png = ob_get_contents();
    ob_end_clean();
    // PHPQRCode change the content-type into image/png... we change it again into html
    header("Content-type: text/html");
    $result_qr_content_in_base64 =  base64_encode($result_qr_content_in_png);
?>
    <div id="printable">
        <table>
            <tr>
                <td>
                    <?php
                    echo '<img width="100%" height="100%" src="data:image/jpeg;base64,' . $result_qr_content_in_base64 . '" />';
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo $value->codes;
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo date('d/m/Y', strtotime($value->created_date));
                    ?>
                </td>
            </tr>
        </table>
    </div>
<?php
}
?>

<!-- <div id="printable">
    <table border="1">
        <tr>
            <td>John</td>
            <td>Doe</td>
        </tr>
        <tr>
            <td>Jane</td>
            <td>Doe</td>
        </tr>
    </table>
</div> -->

<style>
    td {
        text-align: center;
    }

    @media print {

        #printable {
            display: flex;
            text-align: center;
            justify-content: center;
            align-items: center;
            align-content: center;
            height: 100%;
        }

        html,
        body {
            padding: 0;
            height: 38mm;
            width: 58mm;
        }
    }
</style>

<script>
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById('printable').innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
    //location.reload();
</script>