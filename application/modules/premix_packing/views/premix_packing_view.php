<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Packing</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_packing" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>No Transaction</th>
                                        <th>Formula No</th>
                                        <th>Material Name</th>
                                        <th>Prosen</th>
                                        <th class="text-center">Total Packing</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    $(function() {
        datatable_packing();
    });

    function datatable_packing() {
        var siteTable = $('#tabel_packing').DataTable({
            "lengthChange": false,
            "ajax": 'Premix_packing/get_list_packing',
            "paging": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            "order": [
                [0, "asc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            rowCallback: function(row, data, index) {
                
            },
            columns: [{
                    data: 'tanggal',
                    className: 'details-control align-middle text-nowrap',
                    width: '10%'
                },
                {
                    data: 'id_transaction',
                    className: 'details-control align-middle text-nowrap',
                    width: '10%'
                },
                {
                    data: 'fml_no',
                    className: 'details-control',
                    width: '5%'
                },
                {
                    data: 'fml_name',
                    className: 'details-control align-middle',
                    width: '10%'
                },
                {
                    data: 'prosen',
                    className: 'details-control text-center align-middle',
                    width: '1%'
                },
                {
                    data: 'pack',
                    className: 'align-middle text-center text-nowrap',
                    width: '1%'
                },
                {
                    // data: '',
                    defaultContent:'<button class="btn btn-outline-info btn-sm btn-print" title="print"><i class="fas fa-print"></i> Cetak</button>',
                    className: 'details-control text-center align-middle text-nowrap',
                    width: '1%'
                },
            ],
        });


        $('#tabel_packing tbody').on('dblclick', 'tr', function() {
            var table = $('#tabel_material_premix').DataTable({
                ordering: false,
                "destroy": true,

            });
            table.clear();
            // table.draw();
            var data = siteTable.row(this).data();
            console.log(data);
            $('#modal-premix').modal('show');
            var table = $('#tabel_material_premix').DataTable({
                "ajax": 'Premix_rencana_produksi/get_data_material?no_transaction=' + data.id_transaction,
                "paging": false,
                "destroy": true,
                "searching": false,
                "info": false,
                responsive: true,
                ordering: false,
                initComplete: function() {
                    this.api().buttons().container()
                        .appendTo($('.col-md-6:eq(0)', this.api().table().container()));

                },
                "fnDrawCallback": function(oSettings) {
                    $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
                },
                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        data;
                    var intVal = function(i) {
                        console.log(i);
                        return typeof i === 'string' ?
                            i.replace(',', '').replace(/[\$,]/g, '.') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };
                    total = api
                        .column(3)
                        .data()
                        .reduce(function(a, b) {
                            var x = parseFloat(a);
                            var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                            let result = x + y;
                            return result.toFixed(2)
                        }, 0);

                    //Total over this page
                    pageTotal = api
                        .column(3, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            var x = parseFloat(a);
                            var y = isNaN(parseFloat(b)) ? 0 : parseFloat(b);
                            let result = x + y;
                            return result.toFixed(2)
                        }, 2);

                    //Update footer
                    $(api.column(3).footer()).html(
                        total
                    );
                },
                columns: [{
                        data: 'seq',
                        className: 'details-control align-middle text-nowrap',
                        width: '1%',
                        orderable: false
                    },
                    {
                        data: 'material',
                        className: 'details-control align-middle text-nowrap',
                        width: '15%',
                        orderable: false

                    },
                    {
                        data: 'unit_nm',
                        className: 'details-control',
                        width: '5%',
                        orderable: false
                    },
                    {
                        data: 'qty_material',
                        className: 'text-right details-control align-middle',
                        width: '1%',
                        orderable: false

                    },
                ],
            });

        });

        $('#tabel_packing tbody').on('click', '.btn-print', function() {
            var data = siteTable.row($(this).parents('tr')).data();
            console.log(data)
            window.open("premix_packing/print/" + data.id_transaction);
        });
    }
</script>