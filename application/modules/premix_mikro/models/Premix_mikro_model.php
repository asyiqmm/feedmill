<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Premix_mikro_model extends CI_Model
{
    public function get_mikro()
    {
        "COUNT(CASE WHEN b.sts_mikro = 1 then 1 ELSE NULL END) as 'proses_timbang',
        COUNT(CASE WHEN b.sts_mikro = 0 then 1 ELSE NULL END) as 'belum_timbang',
        sum(b.qty_material) qty,a.no_transaction ,a.tanggal ,c.formula_no ,c.formula_kode ,c.formula_name ,a.status,d.nama status_nm
        from t_rencana_produksi a
        join d_formula_material b on  b.id_rencana_produksi = a.id 
        group by a.no_transaction";
        $this->db->select("a.id_transaction, a.tanggal, a.fml_no, a.fml_transaction, a.fml_name")
            ->from('t_rencana_produksi a')
            // ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
            // ->join('m_formula c', 'c.id = b.id_formula')
            // ->join('m_status d', 'd.id = a.status ')
            // ->group_by('a.no_transaction')
            // ->having('proses_timbang != 0 or belum_timbang != 0');
            ->where('a.tanggal', date('Y-m-d'))
            ->where('a.status', 1)
            ->where('a.active', 1)
            ->where('sts_mikro', 0);
        return $this->db->get();
    }
    public function refresh_bin()
    {
        $this->db->set('bin_open', 0)
            ->update('m_bin');
    }

    public function get_count_mikro($no_transaction)
    {
        "select LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material ,d.original_pack
        from t_rencana_produksi a
        join d_formula_material b on  b.id_rencana_produksi = a.id 
        join m_formula c on c.id = b.id_formula 
        join m_material d on d.id = b.id_material 
        join m_unit_satuan e on e.id = d.unit 
        where a.no_transaction = '2021042701PMX001'
        order by seq";

        "
		select LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm ,b.qty_material ,d.original_pack, b.qty_material MOD d.original_pack qty_mikro
        from t_rencana_produksi a
        join d_formula_material b on  b.id_rencana_produksi = a.id 
        join m_formula c on c.id = b.id_formula 
        join m_material d on d.id = b.id_material 
        join m_unit_satuan e on e.id = d.unit 
        where a.no_transaction = '2021042701PMX001'
        order by seq
		";

        $this->db->select("LPAD(b.`sequence` +0, 3, '0') seq,concat('[',d.material_kode ,'] ',d.material_nm) material, e.unit_nm, b.qty_material, d.original_pack, b.qty_timbang_mikro")
            ->from('t_rencana_produksi a')
            ->join('d_formula_material b', 'b.id_rencana_produksi = a.id')
            ->join('m_formula c', 'c.id = b.id_formula')
            ->join('m_material d', 'd.id = b.id_material')
            ->join('m_unit_satuan e', 'e.id = d.unit')
            ->where('a.no_transaction', $no_transaction)
            ->where('sts_mikro', 2)
            ->order_by('seq');
        return $this->db->get();
        //
        //  ->where('qty_timbang_mikro <> 0')
    }

    public function get_list_timbang($no_transaction, $type)
    {
        $this->db->select("d.id id_bin,LPAD(a.`sequence` +0, 3, '0') seq,a.id_transaction ,
                a.material_kd ,
                a.material_nm ,
                concat('[',d.material ,'] ',d.material_nm) material,
                ifnull(b.original_pack,25) original_pack,
                a.qty_material ,
                a.qty_timbang_mikro ,
                a.qty_material % ifnull(b.original_pack,25) total_mikro,
                format(a.qty_material % ifnull(b.original_pack,25) - a.qty_timbang_mikro,2) qty_mikro,
                a.`sequence`, COALESCE(d.qty,0) qty_bin")
            ->from('d_dosing a')
            ->join('d_original_pack b', 'b.material = a.material_kd and b.active = 1', 'left')
            ->join('t_rencana_produksi c', 'c.id_transaction = a.id_transaction and c.active = 1')
            // ->join('m_material d','d.material_kode = a.material_kd','left')
            ->join('m_bin d', 'd.material = a.material_kd', 'left')
            ->where('a.id_transaction', $no_transaction);
        if ($type >= 2) {
            $this->db->having('a.qty_timbang_mikro < total_mikro')
                ->having('total_mikro <> 0');
        } else {
            $this->db->having('a.qty_timbang_mikro > 0');
        }
        $this->db->order_by('a.`sequence`', 'ASC');
        if ($type == 2) {
            $this->db->limit(1);
        }
        $result =  $this->db->get();

        return $result;
    }

    public function get_list_mikro($no_transaction)
    {
        $this->db->select("LPAD(a.`sequence` +0, 3, '0') seq,a.id_transaction ,
                a.material_kd ,
                a.material_nm ,
                ifnull(b.original_pack,25) original_pack,
                a.qty_material ,
                a.qty_timbang_mikro ,
                a.qty_material % ifnull(b.original_pack,25) total_mikro,
                format(a.qty_material % ifnull(b.original_pack,25) - a.qty_timbang_mikro,2) qty_mikro,
                a.`sequence`")
            ->from('d_dosing a')
            ->join('d_original_pack b', 'b.material = a.material_kd and b.active = 1', 'left')
            ->join('t_rencana_produksi c', 'c.id_transaction = a.id_transaction and c.active = 1')
            ->where('a.id_transaction', $no_transaction);
        // if ($type >= 2) {
        //     $this->db->having('a.qty_timbang_mikro < total_mikro')
        //     ->having('total_mikro <> 0');
        // } else {
        //     $this->db->having('a.qty_timbang_mikro > 0');

        // }
        $this->db->order_by('a.`sequence`', 'ASC');
        // if ($type == 2) {
        //     $this->db->limit(1);
        // }
        $result =  $this->db->get();
        // var_dump($this->db->last_query());die();

        return $result;
    }

    public function post_timbang_material()
    {
        // var_dump($_POST);
        // die();
        $this->db->trans_begin();
        // $insert = array(
        //     'sts_mikro' => 1,
        //     'qty_timbang_mikro' => $data['qty_timbang_mikro'],
        //     'tgl_timbang_mikro' => date('Y-m-d H:i:s'),
        // );
        // $this->db->where('id', $data['id_formula_material']);
        // $this->db->update('d_formula_material', $insert);
        $timbang = floatval($_POST['qty_timbang_mikro']);
        $this->db->set('qty', "`qty` - $timbang", FALSE)
            ->where('material', $_POST['id_material'])
            ->update('m_bin');

        $this->db->set('qty_timbang_mikro', "`qty_timbang_mikro` + $timbang", FALSE)
            ->set('tgl_timbang_mikro', date('Y-m-d H:i:s'))
            ->where('material_kd', $_POST['id_material'])
            ->where('id_transaction', $_POST['id_transaction'])
            ->update('d_dosing');
        $this->db->set('bin_open', 0)
            ->where('material', $_POST['id_material'])
            ->update('m_bin');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil menginput data'
            ];
        }
    }

    public function cek_kode()
    {
        $this->db->select('*')
            ->from('m_bin a')
            ->where('a.bin_code', $_POST['code'])
            ->where('a.material', $_POST['id_material']);
        $bin = $this->db->get();
        $cek_bin = $bin->num_rows();
        // if ($cek_bin > 0) {
        //     $this->db->set('bin_open', 1)
        //         ->where('material', $_POST['id_material'])
        //         ->update('m_bin');

        //     $result = [
        //         'status' => true,
        //         'msg' => 'Kode material cocok'
        //     ];
        // }

        $this->db->select('*')
            ->from('m_qrcodes a')
            ->where('a.codes', $_POST['code'])
            // ->where('b.sts_use', 0)
            ->where('a.material', $_POST['id_material']);
        $cek_material = $this->db->get()->num_rows();


        // var_dump($this->db->last_query());
        // die();
        // var_dump($cek_material > 0 || $cek_bin > 0);die();
        if ($cek_material > 0 || $cek_bin > 0) {
            $this->db->set('bin_open', 1)
                ->where('material', $_POST['id_material'])
                ->update('m_bin');
            // var_dump($this->db->last_query());die();
            $result = [
                'status' => true,
                'msg' => 'Kode material cocok'
            ];
            if ($cek_bin > 0) {
                $result['data'] = $bin->row();
            }
        } else {
            $result = [
                'status' => false,
                'msg' => 'Kode material salah'
            ];
        }
        return $result;
    }

    public function get_fml($id)
    {
        $this->db->select('fml_transaction,prosen')
            ->from('t_rencana_produksi')
            ->where('id_transaction', $id);
        return $this->db->get()->row();
    }
}
