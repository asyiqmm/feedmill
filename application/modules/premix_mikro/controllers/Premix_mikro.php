<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Premix_mikro extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('premix_mikro_model');
		$this->load->model('premix_makro/premix_makro_model');
		$this->load->model('premix_intake/premix_intake_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Mikro | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 6;
			$data['child_active'] = 9;
			$data['grandchild_active'] = 10;
			$this->premix_intake_model->refresh_bin();
			$this->_render('premix_mikro_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}

	public function get_mikro()
	{
		$datas = $this->premix_mikro_model->get_mikro();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function timbang($no)
	{
		$uri = &load_class('URI', 'core');
		$data['parent_active'] = 6;
		$data['child_active'] = 9;
		$data['grandchild_active'] = 10;
		$data['no_transaction'] = $no;
		$data['fml_transaction'] = $this->premix_mikro_model->get_fml($no);
		// $this->_render('timbang_mikro', $data);
		$cek = $this->premix_makro_model->cek_no($no);
		// var_dump($cek->num_rows);die();
		if ($cek->num_rows() > 0) {
			$datas = $this->premix_mikro_model->refresh_bin();
			$this->_render('timbang_mikro', $data);
		} else {
			$this->_render('error_view', $data);
		}
	}

	public function get_count_mikro()
	{
		$data = $this->premix_mikro_model->get_list_timbang($_GET['id_transaction'],1);
		// $datas = $this->premix_mikro_model->get_count_mikro();
		// $data =  json_decode(json_encode($datas->result()), TRUE);
		// foreach ($data as $key => $value) {
		// 	$data[$key]['qty_mikro'] = number_format($value['qty_material'] % $value['original_pack'], 2);
		// }
		// // var_dump($data);die();
		$obj = array("data" => $data->result());
		echo json_encode($obj);
	}

	public function get_list_mikro()
	{
		$data = $this->premix_mikro_model->get_list_mikro($_GET['id_transaction']);
		// $datas = $this->premix_mikro_model->get_count_mikro();
		// $data =  json_decode(json_encode($datas->result()), TRUE);
		// foreach ($data as $key => $value) {
		// 	$data[$key]['qty_mikro'] = number_format($value['qty_material'] % $value['original_pack'], 2);
		// }
		// // var_dump($data);die();
		$obj = array("data" => $data->result());
		echo json_encode($obj);
	}

	public function get_list_timbang()
	{
		$data = $this->premix_mikro_model->get_list_timbang($_GET['id_transaction'],2);
		$cek = $this->db->select('status,sts_mikro,sts_makro')
			->from('t_rencana_produksi')
			->where('id_transaction', $_GET['id_transaction'])->get()->row();

		if ($data->num_rows() == 0) {
			if ($cek->sts_mikro == 0) {
				if ($cek->sts_makro == 1) {
					// QUERY MENGUBAH STATUS MIKRO dan status transaksi
					$this->db->set('status', 2)
						->set('sts_mikro', 1)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				} else {
					$this->db->set('sts_mikro', 1)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				}
			} elseif ($cek->sts_mikro == 1 && $cek->sts_makro == 1) {
				if ($cek->status >= 2) { //cek apakah status sudah dirubah
					//menampilkan informasi dosing selesai
					// $result['status'] = false;
				} else {
					//mengubah status transaksi dan menampilkan informasi dosing selesai
					$this->db->set('status', 2)
						->where('id_transaction', $_GET['id_transaction'])
						->update('t_rencana_produksi');
				}
			}
			$result['status'] = false;
			// else {
			// 	if ($data->num_rows() == 0) {
			// 		$result['status'] = false;
			// 	} else {
			// 		$result = $data->row_array();
			// 		$result['status'] = true;
			// 	}
			// }
		} else {
			$result = $data->row_array();
			$result['status'] = true;
		}


		// var_dump($data->num_rows());die();
		// var_dump($data->result());die();
		// $url = 'https://ismaf.mydmc.co.id/api/formulapremixdetail?formula=' . $_GET['fml_transaction'];
		// $ch = curl_init($url);
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		// $r = curl_exec($ch);
		// curl_close($ch);

		// $result = json_decode($r, true);
		// foreach ($result['data'] as $key => $value) {

		// 	$mikro = (($value['prosen'] / 100) * $_GET['prosen']) % 25;
		// 	if ($mikro) {
		// 	}
		// }
		// var_dump($mikro);
		// die();


		// $sum = $data->num_rows();
		// $result = $data->row();

		// if ($sum > 0) {
		// 	if ($result->qty_mikro == 0 || 0 >= $result->qty_mikro) {

		// 		$this->db->set('sts_mikro', 2)
		// 			->where('id', $result->id_formula_material)
		// 			->update('d_formula_material');
		// 		return $this->get_list_timbang();
		// 	} else {
		// 		$datas = array(
		// 			'status'				=> true,
		// 			'qty_mikro' 			=> $result->qty_mikro,
		// 			'material' 				=> $result->material,
		// 			'id_formula_material' 	=> $result->id_formula_material,
		// 			'id_material'		 	=> $result->id_material,
		// 		);
		// 	}
		// } else {
		// 	$datas = array(
		// 		'status'	=> false
		// 	);
		// }

		echo json_encode($result);
		// die();
	}

	public function post_timbang_material()
	{
		// var_dump($_POST);die();
		$result = $this->premix_mikro_model->post_timbang_material();
		echo json_encode($result);
	}

	public function cek_kode()
	{
		// var_dump($_POST);
		// die();
		$cek = $this->premix_mikro_model->cek_kode();
		echo json_encode($cek);
	}
}
