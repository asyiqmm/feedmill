<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form action="" method="post" id="form_penimbangan" enctype="multipart/form-data" class="form-horizontal">

                <div class="row">
                    <div class="col-12">
                        <div class="card text-center">
                            <div class="card-header">
                                Informasi Material Mikro
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">

                                <div class="col">
                                    <div class="row justify-content-center">
                                        <h2 id="material" style="font-size: 2.5em; font-weight:bold;">-</h2>
                                        <input type="hidden" name="id_transaction" id="id_formula_material">
                                        <input type="hidden" name="id_material" id="id_material">
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <h2 class="description-header" style="font-size: 2.5em;">Target :&nbsp;</h2>
                                        <h2 class="description-header font-weight-bold" style="font-size: 2.5em;" id="qty_target">-</h2>
                                        <span class="description-text" style="font-size: 1.7em;">Kg</span>
                                        <input type="hidden" name="qty_timbang_mikro" id="qty_timbang_mikro">
                                    </div>
                                    <!-- /.description-block -->
                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Timbangan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body ">
                                <div class="col">
                                    <div class="row justify-content-center">
                                        <style>
                                            @media screen and (max-width : 1180px) {
                                                .timbangan {
                                                    font-size: 3.4em;
                                                }
                                            }

                                            @media screen and (min-width : 1181px) {
                                                .timbangan {
                                                    font-size: 5.04em;
                                                }
                                            }
                                        </style>
                                        <table width="100%" style="background-color: darkorange;">
                                            <tr>
                                                <td colspan="2" class="text-right"><span class="font-weight-bold align-top timbangan" id="nilai_timbangan">0.00</span> <span class="font-weight-bold align-top timbangan">KG</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center text-nowrap"><span class="text-light text-danger" style="display: none; font-size:1.6em" id="alert_timbangan">*Timbangan tidak terhubung</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan1" value="1" name="r1" checked>
                                                        <label for="timbangan1">
                                                            Timbangan 1
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="icheck-primary d-inline">
                                                        <input type="radio" class="pilihtimbangan" id="timbangan2" value="2" name="r1">
                                                        <label for="timbangan2">
                                                            Timbangan 2
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- <style>
                                            .h1,
                                            h1 {
                                                font-size: 4.5rem;
                                            }
                                        </style>
                                        <h1 class="description-header font-weight-bold " id="nilai_timbangan">0.00</h1>
                                        <span class="description-text">Kg</span> -->
                                    </div>
                                    <!-- <span class="text-danger" id="alert_timbangan" style="display: none;">*Timbangan tidak terhubung</span> -->
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Cek Kode
                            </div>


                            <!-- /.card-header -->
                            <div class="card-body">
                                <button class="btn btn-primary btn-lg" disabled hidden id="btnSimpan" type="submit">Simpan</button>
                                <input autofocus autocomplete="off" required id="codes" name="codes" class="form-control font-weight-bold text-center" style="font-size: 2em;" />
                                <div class="col-sm-12 sukses" style="display: none;">
                                    <div class="row justify-content-center">
                                        <i class="fas fa-check-circle text-success display-4"></i>
                                    </div>
                                </div>
                                <div class="col-sm-12 gagal" style="display: none;">
                                    <div class="row justify-content-center">
                                        <i class="fas fa-times-circle text-danger display-4"></i>
                                    </div>
                                </div>
                                <div class="col-sm-12 kurang justify-content-center" style="display: none;">
                                    <span class="text-danger">*Stok Bin kurang</span>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                </div>
            </form>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Produksi Mikro</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-3">
                            <table id="tabel_mikro" width="100%" class="table table-hover table-bordered table-valign-middle">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>Seq</th>
                                        <th>Material</th>
                                        <th>Unit</th>
                                        <!-- <th>Qty Original Packing</th> -->
                                        <th>Qty Mikro</th>
                                        <th>Qty Timbang</th>
                                        <th>Dif</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<script>
    document.onkeypress = dump;
    var word = "";

    function dump(e) {
        var unicode = e.keyCode ? e.keyCode : e.charCode;
        var actualkey = String.fromCharCode(unicode);
        word += actualkey;
        console.log('wadasda');
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 5 second for example
    var $input = $('#codes');

    //on keyup, start the countdown
    $input.on('keyup', function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    var cek_status = false;

    function doneTyping() {
        console.log('word', word);

        var input = word.substring(0, 8);
        if (input == '37775814') {
            if (cek_status) {
                $('#btnSimpan').attr('disabled', false);
                $('#btnSimpan').trigger('click');
            } else {
                $('#btnSimpan').attr('disabled', true);
                $('#codes').focus();
                word = '';
            }
        } else {
            $('#btnSimpan').attr('disabled', true);
            $('#codes').focus();
            word = '';
        }

    }

    $('#codes').keypress(function(event) {
        var code = $(this).val();
        var id_material = $('#id_material').val();

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $.ajax({
                url: <?php base_url() ?> '/premix/premix_mikro/cek_kode',
                method: "post",
                data: {
                    code: code,
                    id_material: id_material,
                },
                dataType: "JSON",
                beforeSend: function() {
                    $('#btnSimpan').attr('disabled', true);
                },
                success: function(data) {
                    $("#tabel_mikro")
                        .DataTable()
                        .ajax.reload();
                    // get_list_timbang();

                    if (data.status) {
                        weigh = true;
                        get_timbang();
                        cek_status = true;
                        $('#codes').attr('readonly', true);
                        $('#codes').focusout();
                        $('.sukses').fadeIn(500);
                        $('.gagal').hide();
                        $('#btnSimpan').attr('disabled', true);
                    } else {
                        weight = false;
                        timeout = null;
                        cek_status = false;
                        $('#codes').val('');
                        $('.gagal').fadeIn(500);
                        $('.sukses').hide();
                    };

                }
            });
        }
    });

    $(function() {
        datatable_mikro();
        get_list_timbang();
        // timbang();
    });

    function get_list_timbang() {
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_mikro/get_list_timbang',
            data: {
                id_transaction: '<?php echo $no_transaction ?>',
                prosen: '<?php echo $fml_transaction->prosen ?>',
            },
            method: "GET",
            dataType: "json",
            success: function(data) {
                console.log(data);
                if (data.status) {
                    var bin = data.qty_bin;
                    var qty = data.qty_mikro;
                    if (data.id_bin) {
                        if (Number(bin) >= Number(qty)) {
                            $('.kurang').hide();
                            $('#codes').attr('disabled', false);
                        } else {
                            $('.kurang').fadeIn(500);
                            $('#codes').attr('disabled', true);
                        }
                    } else {
                        $('.kurang').hide();
                        $('#codes').attr('disabled', false);
                    }

                    $('#qty_target').text(data.qty_mikro);
                    $('#material').text(data.material_nm);
                    $('#id_formula_material').val(data.id_transaction);
                    $('#id_material').val(data.material_kd);

                } else {
                    $('#material').text('Selesai').addClass('text-success');
                    weight = false;
                    $('#btnSimpan').attr('disabled', true);
                    $('#qty_target').text(0);
                    $('#id_material').val('');
                }
            }
        })
    }
    var weight = true;
    function get_timbang() {
        console.log('jalan');
        $.ajax({
            url: <?php base_url() ?> '/premix/premix_makro/get_data_timbangan?timbangan=' + $('.pilihtimbangan:checked').val(),
            method: "GET",
            dataType: "json",
            success: function(data) {
                console.log(data.data[0]);
                if (data.data[0].status == 'success') {
                    var kg = Number(data.data[0].random);
                    if (weight) {
                        setTimeout(get_timbang, 500);
                        console.log('Terhubung 0.5 detik');
                    }
                    $('#alert_timbangan').hide();
                    $('#nilai_timbangan').removeClass('text-danger');
                    $('#nilai_timbangan').text(kg.toFixed(2));
                    $('#qty_timbang_mikro').val(kg.toFixed(2));
                }else{
                    // if (weight) {
                        setTimeout(get_timbang, 5000);
                        console.log('tidak terhubung 5 detik');
                    // }
                    $('#alert_timbangan').show();
                    $('#nilai_timbangan').addClass('text-danger');
                    a = Number(0);
                    $('#nilai_timbangan').text(a.toFixed(2));
                    $('#qty_timbang_mikro').val(a.toFixed(2));
                }
                //Timbangan asli
                // if (data.status) {
                //     var kg = Number(data.data.ADC);
                //     if (weight) {
                //         setTimeout(get_timbang, 500);
                //         console.log('Terhubung 0.5 detik');
                //     }
                //     $('#alert_timbangan').hide();
                //     $('#nilai_timbangan').removeClass('text-danger');
                //     $('#nilai_timbangan').text(kg.toFixed(2));
                //     $('#qty_timbang').val(kg.toFixed(2));
                // } else {
                //     if (weight) {
                //         setTimeout(get_timbang, 5000);
                //         console.log('tidak terhubung 5 detik');
                //     }
                //     $('#alert_timbangan').show();
                //     $('#nilai_timbangan').addClass('text-danger');
                //     a = Number(0);
                //     $('#nilai_timbangan').text(a.toFixed(2));
                //     $('#qty_timbang').val(a.toFixed(2));
                // }
            },
            error: function(data) {
                if (weight) {
                    setTimeout(get_timbang, 5000);
                    console.log('tidak terhubung 5 detik');
                }
                $('#alert_timbangan').show();
                $('#nilai_timbangan').addClass('text-danger');
                $('#btnSimpan').attr('disabled', true);
                a = Number(0);
                $('#nilai_timbangan').text(a.toFixed(2));
                $('#qty_timbang').val(a.toFixed(2));
                // console.log(data);
            }
        })
    }

    function datatable_mikro() {
        var siteTable = $('#tabel_mikro').DataTable({
            "lengthChange": false,
            "ajax": <?php base_url() ?> '/premix/premix_mikro/get_count_mikro?id_transaction=<?php echo $no_transaction ?>',
            "paging": false,
            "searching": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "scrollY": "500px",
            "scrollX": true,
            ordering: false,
            "order": [
                [1, "desc"]
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'seq',
                    className: 'details-control align-middle text-nowrap',
                    width: '1%'
                },
                {
                    data: 'material_nm',
                    className: 'details-control align-middle text-nowrap',
                    width: '15%'
                },
                {
                    className: 'details-control text-center',
                    width: '1%',
                    "render": function(data, type, row, meta) {
                        return 'KG';
                    }
                },
                {
                    "targets": 3,
                    data: 'total_mikro',
                    className: 'details-control align-middle text-right',
                    width: '5%',

                },
                {
                    data: 'qty_timbang_mikro',
                    className: 'details-control text-right align-middle text-nowrap',
                    width: '5%',
                },
                {
                    "targets": 5,
                    data: 'dif',
                    width: '1%',
                    "render": function(data, type, row, meta) {
                        var dif = row.qty_timbang_mikro - row.total_mikro;
                        return dif.toFixed(2);
                    }
                }
            ],
        });
        $('#tabel_mikro tbody').on('dblclick', 'tr', function() {
            var data = siteTable.row(this).data();
            // console.log(data);
        });

    }

    $('#form_penimbangan').on('submit', function(event) {
        var codes = $('#codes').val()
        if (codes == '' || codes == null) {
            console.log('hei');
            word = "";
            return false;
        } else {

            console.log('ha');
            event.preventDefault();
            $.ajax({
                url: <?php base_url() ?> '/premix/premix_mikro/post_timbang_material',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                dataType: "JSON",
                beforeSend: function() {
                    $('#btnSimpan').attr('disabled', true);
                    cek_status = false;
                },
                success: function(data) {

                    if (data.status) {
                        $('#codes').attr('readonly', false);
                        $('#btnSimpan').attr('disabled', true);
                        weight = false;
                        cek_status = false;
                        $('#codes').val('');

                        showToast(data.msg);
                        $("#tabel_mikro")
                            .DataTable()
                            .ajax.reload();
                        $('.sukses').hide();
                        $('.gagal').hide();
                    } else {
                        showToastError(data.msg);
                        cek_status = true;
                    };
                    get_list_timbang();

                },
                error: function() {
                    cek_status = true;
                    get_list_timbang();
                }
            });
        }
    });
</script>