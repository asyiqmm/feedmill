<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>receipt</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <!-- <link rel="stylesheet" href="../paper.css">
 -->
    <style>
        @page {
            size: 57mm 100mm
        }
    </style>
    <style>
        @page {
            margin: 0
        }

        body {
            margin: 0
        }

        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            page-break-after: always;
        }

        /** Paper sizes **/
        body.A3 .sheet {
            width: 297mm;
            height: 419mm
        }

        body.A3.landscape .sheet {
            width: 420mm;
            height: 296mm
        }

        body.A4 .sheet {
            width: 210mm;
            height: 296mm
        }

        body.A4.landscape .sheet {
            width: 297mm;
            height: 209mm
        }

        body.A5 .sheet {
            width: 148mm;
            height: 209mm
        }

        body.A5.landscape .sheet {
            width: 210mm;
            height: 147mm
        }

        body.letter .sheet {
            width: 216mm;
            height: 279mm
        }

        body.letter.landscape .sheet {
            width: 280mm;
            height: 215mm
        }

        body.legal .sheet {
            width: 216mm;
            height: 356mm
        }

        body.legal.landscape .sheet {
            width: 357mm;
            height: 215mm
        }

        /** Padding area **/
        .sheet.padding-10mm {
            padding: 10mm
        }

        .sheet.padding-15mm {
            padding: 15mm
        }

        .sheet.padding-20mm {
            padding: 20mm
        }

        .sheet.padding-25mm {
            padding: 25mm
        }

        /** For screen preview **/
        @media screen {
            body {
                background: #e0e0e0
            }

            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                margin: 5mm auto;
            }
        }

        /** Fix for Chrome issue #273306 **/
        @media print {
            body.A3.landscape {
                width: 420mm
            }

            body.A3,
            body.A4.landscape {
                width: 297mm
            }

            body.A4,
            body.A5.landscape {
                width: 210mm
            }

            body.A5 {
                width: 148mm
            }

            body.letter,
            body.legal {
                width: 216mm
            }

            body.letter.landscape {
                width: 280mm
            }

            body.legal.landscape {
                width: 357mm
            }

            .font-12 {
                font-size: 11px;
            }
        }
    </style>
</head>

<body class="receipt">
    <?php
    require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
    require_once(APPPATH . "libraries/phpqrcode/qrconfig.php");

    foreach ($codes as $value) {
        $content = $value->codes;
        ob_start();
        QRcode::png($content);
        $result_qr_content_in_png = ob_get_contents();
        ob_end_clean();
        // PHPQRCode change the content-type into image/png... we change it again into html
        header("Content-type: text/html");
        $result_qr_content_in_base64 =  base64_encode($result_qr_content_in_png);
    ?>
        <section class="sheet" style="text-align: center; justify-content:center; display:flex;">
            <table>
                <tr>
                    <td>
                        <?php
                        echo '<img height="90px" src="data:image/jpeg;base64,' . $result_qr_content_in_base64 . '" />';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-12">
                        <?php
                        echo $value->material_name;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-12">
                        <?php
                        echo $value->codes;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-12">
                        <?php
                        echo date('d/m/Y', strtotime($value->tanggal));
                        ?>
                    </td>
                </tr>
            </table>
        </section>
    <?php
    }
    ?>
</body>

</html>
<script>
    window.print();
    window.onafterprint = function() {
        window.close();
    }
</script>