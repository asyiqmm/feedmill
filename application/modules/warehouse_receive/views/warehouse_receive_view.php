<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">

		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="container-fluid">

			<div class='card'>
				<div class='card-header'>
					<h4 class='card-title'>Data Barang Diterima</h4>
					<div class="card-tools">
						<!-- <a href="<?php echo site_url('warehouse_receive/scanner'); ?>" class="btn btn-warning">Scan QR</a>

						<a href="http://192.168.46.210:4133/pages/Scanner.html" class="btn btn-warning">Scan QR</a> -->
					</div>
				</div>

				<div class='card-body table-responsive p-3'>

					<table id='tabel_allcodes' class='table table-bordered table-hover' style='width:100%'>
						<thead class="bg-primary">
							<tr>
								<th>Tanggal</th>
								<th>Nama</th>
								<th>Banyak Item</th>
								<th>QR</th>

							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>


<div class="modal fade" id="ModalAddQrcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Data QR Code</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="post_qrcode_baru" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body">

					<div class='form-group row'>
						<label for='waktu' class='col-sm-5 col-form-label'>Nama Material</label>
						<div class='col-sm-7'>
							<select class='select2bs4' id='material' name='material' required>
							</select>
						</div>
					</div>
					<div class='form-group row'>
						<label for='jumlah' class='col-sm-5 col-form-label'>Jumlah Barcode</label>
						<div class='col-sm-7'>
							<input type='number' class='form-control' id='jumlah' required min="1" placeholder='0' name='jumlah'>
						</div>
					</div>
					<div class='form-group row'>
						<label for='original_pack' class='col-sm-5 col-form-label'>Original Pack</label>
						<div class="input-group date col-sm-7">
							<input type="text" class="form-control decimal" placeholder='0.00' id="original_pack" name='original_pack'/>
							<div class="input-group-append" data-target="#original_pack">
								<div class="input-group-text"><strong>Kg/item/Netto</strong></div>
							</div>
						</div>
					</div>
					<div class='form-group row'>
						<label for='berat_karung' class='col-sm-5 col-form-label'>Berat Kosong/Karung</label>
						<div class="input-group date col-sm-7" >
							<input type="text" class="form-control decimal" id="berat_karung" placeholder='0.00' name='berat_karung'/>
							<div class="input-group-append" data-target="#berat_karung">
								<div class="input-group-text"><strong>Kg/Tara</strong></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-qrcode">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Data Material Formula</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="" method="post" id="form_penerimaan_sparepart" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-body ">

					<div class="table-responsive">

						<table id="table_qrcode" width="100%" class="table table-bordered">
							<thead class="text-center">
								<tr width="100%">
									<th>No.</th>
									<th>Material Code</th>
									<th>Material</th>
									<th>Status</th>
								</tr>
							</thead>

						</table>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<!-- <button type="Submit" value="Submit" id="btnSubmit" class="btn btn-primary">Save</button> -->
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<script>
	$(function() {
		datatable_warehouse();
		list_material();
	});
	// function list_material() {
	// 	'https://ismaf.mydmc.co.id/api/material';
	// 	jQuery.ajax({
	// 		type: "GET",
	// 		url: "http://192.168.58.4", // the method we are calling
	// 		dataType: "json",
	// 		success: function(data) {
	// 			console.log(data)
	// 			// var option = '<option></option>';
	// 			// for (let index = 0; index < data.data.length; index++) {
	// 			// 	option += '<option value="' + data.data[index].material + '⁑' + data.data[index].name + '⁑' + data.data[index].unit + '" >' + data.data[index].name + '</option>';
	// 			// }
	// 			// if (data.meta.code == 200) {
	// 			// 	showToast(data.meta.message);
	// 			// 	$('#material').html(option);
	// 			// } else {
	// 			// 	showToastWarning(data.meta.message);
	// 			// }

	// 		},
	// 		error: function(xhr, status, error) {
	// 			showToastWarning('error')
	// 		}

	// 	});
	// }
	function list_material() {
		'https://ismaf.mydmc.co.id/api/material';
		jQuery.ajax({
			type: "GET",
			url: "https://ismaf.mydmc.co.id/api/material", // the method we are calling
			dataType: "json",
			success: function(data) {
				var option = '<option></option>';
				for (let index = 0; index < data.data.length; index++) {
					option += '<option value="' + data.data[index].material + '⁑' + data.data[index].name + '⁑' + data.data[index].unit + '" >' + data.data[index].name + '</option>';
				}
				if (data.meta.code == 200) {
					showToast(data.meta.message);
					$('#material').html(option);
				} else {
					showToastWarning(data.meta.message);
				}

			},
			error: function(xhr, status, error) {
				showToastWarning('error')
			}

		});
	}

	function datatable_warehouse() {

		var siteTable = $('#tabel_allcodes').DataTable({
			"info": false,
			"destroy": true,
			"lengthChange": true,
			"order": [
				[0, "desc"]
			],
			"paging": false,
			"pageLength": 7,
			"lengthMenu": [
				[7, 10, 25, 50, -1],
				[7, 10, 25, 50, "All"]
			],
			"scrollCollapse": true,
			"scrollY": "500px",
			"scrollX": true,
			"ajax": 'warehouse_receive/get_all_codes',
			buttons: [{
				text: 'Tambah Kode',
				className: 'btn btn-primary btn-sm btn-add',
				action: function(e, dt, node, config) {
					$('.btn-add')
						.attr('data-toggle', 'modal')
						.attr('data-target', '#ModalAddQrcode');
				}
			}, ],
			"columns": [

				{
					"data": 'tgls',
					"className": 'details-control text-center align-middle text-nowrap',
					"width": '1%'
				},
				{
					"data": 'nama',
					"className": 'details-control align-middle text-nowrap',
					"width": '15%'
				},
				{
					"data": 'total',
					"className": 'details-control text-center align-middle text-nowrap',
					"width": '1%'
				},
				{
					"data": 'idmaterial',
					"className": 'details-control text-left align-middle text-nowrap',
					"width": '1%',
					"orderable": false,
					"render": function(data, type, row) {
						
						if (row.blm_digunakan <= 0) {
							return '<a href="javascript:void(0);" class="btn btn-danger btn-sm disabled item_edit" >Kode Kosong</a>';
						} else {
							return '<a href="warehouse_receive/print/' + row.material + '/' + row.tgls + '/' + row.urutan_material + '" target="_blank" class="btn btn-outline-info btn-sm mr-1" data-id="' + data + '"><i class="fas fa-print"></i>&nbsp Cetak</a><button class="btn btn-sm btn-outline-danger btn-hapus"><i class="fas fa-trash"></i> Hapus</button>';
						}
					}
				},

			],
			"columnDefs": [{
				"targets": '_all',
				"defaultContent": '0'
			}],
			"order": [
				[0, 'desc']
			],

			initComplete: function() {
				this.api().buttons().container()
					.appendTo($('.col-md-6:eq(0)', this.api().table().container()));
			}
		});
		$('#tabel_allcodes tbody').on('click', '.btn-hapus', function() {
			var datas = siteTable.row($(this).parents('tr')).data();
			console.log(datas);
			swal
				.fire({
					title: "Perhatian",
					html: "Hapus barcode <b>[" + datas.nama + "]</b> pada tanggal <b>" + datas.tgls + "</b>?",
					// type: "question",
					icon: 'warning',
					showCloseButton: true,
					showCancelButton: true,
					confirmButtonColor: "#e74c3c",
					confirmButtonText: "Hapus",
					cancelButtonText: "Tidak"
				})
				.then(function(result) {
					if (result.value) {
						$.ajax({
							url: <?php base_url() ?> 'warehouse_receive/delete',
							method: "POST",
							data: {
								material: datas.material,
								urutan_material: datas.urutan_material,
								tanggal: datas.tgls,
								total: datas.total
							},
							dataType: "json",
							beforeSend: function() {
								swal.fire({
									title: 'Please Wait..!',
									text: 'Is working..',
									onOpen: function() {
										swal.showLoading()
									}
								})
							},
							success: function(data) {
								if (data.status) {
									swal.fire({
										icon: 'success',
										title: data.msg,
										showConfirmButton: false,
										timer: 2000
									});
									$($.fn.dataTable.tables(true)).DataTable().ajax.reload();
								} else {
									swal.hideLoading();
									swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
								}
							},
							complete: function() {
								swal.hideLoading();
							},
							error: function(jqXHR, textStatus, errorThrown) {
								swal.hideLoading();
								swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
							}
						})
					} else {}
				});
		});

		$('#tabel_allcodes tbody').on('dblclick', 'tr', function() {
			var data = siteTable.row(this).data();
			var table = $('#table_qrcode').DataTable({
				ordering: false,
				"destroy": true,

			});
			table.clear();
			// table.draw();
			
			if (data) {
				$('#modal-qrcode').modal('show');
				var table = $('#table_qrcode').DataTable({
					"oLanguage": {
						"sEmptyTable": "Gagal memuat data, coba sesaat lagi..."
					},
					"ajax": 'Warehouse_receive/get_qrcode?material=' + data.material + '&urutan_material=' + data.urutan_material + '&tanggal=' + data.tgls,
					"paging": false,
					"destroy": true,
					"searching": false,
					"info": false,
					responsive: true,
					ordering: false,
					initComplete: function(row, datas) {
						// if (data.status != 2) {
						//     table.column(4).visible(false);
						//     table.column(5).visible(false);
						// }
						this.api().buttons().container()
							.appendTo($('.col-md-6:eq(0)', this.api().table().container()));

					},
					rowCallback: function(row, data, index) {
						// if (data.diff == 0) {
						//     $(row).css('background-color', 'white');
						// } else {
						//     $(row).css('background-color', '#ea8685');
						// }
					},
					"fnDrawCallback": function(oSettings) {
						$($.fn.dataTable.tables(true)).DataTable()
							.columns.adjust();
					},
					columns: [{
							className: 'details-control align-middle text-nowrap text-center',
							width: '1%',
							orderable: false,
							"render": function(data, type, row, meta) {
								return meta.row + 1;
							}
						},
						{
							data: 'codes',
							className: 'details-control align-middle text-nowrap',
							width: '15%',
							orderable: false

						},
						{
							className: 'details-control text-left text-nowrap',
							width: '10%',
							orderable: false,
							"render": function(data, type, row, meta) {
								return '[' + row.material + '] ' + row.material_name;
							}
						},
						{
							className: 'text-center details-control align-middle text-nowrap',
							width: '1%',
							orderable: false,
							"render": function(data, type, row, meta) {
								if (row.sts_digunakan == 1) {
									var result = '<button class="btn btn-danger btn-sm" disabled>Telah terpakai</button>';
								} else {
									var result = '<button class="btn btn-success btn-sm" disabled>Tersedia</button>';
								}
								return result;
							}
						},
					],
				});
			}

		});
	}

	$('#post_qrcode_baru').on('submit', function(event) {
		event.preventDefault();
		swal.showLoading()
		$.ajax({
			url: <?php base_url() ?> 'warehouse_receive/post_qrcode_baru',
			type: "post",
			data: new FormData(this),
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			dataType: "JSON",
			beforeSend: function() {
				$('#btnTambah').attr('disabled', true);

				// swal.fire({
				//     title: 'Tunggu..!',
				//     text: 'Memproses..',
				//     onOpen: function() {

				//     }
				// })
			},
			success: function(data) {
				$('#btnTambah').attr('disabled', false);
				if (data.status) {
					$('#ModalAddQrcode').modal('hide');
					swal.hideLoading();

					swal.fire({
						icon: 'success',
						title: data.msg,
						showConfirmButton: false,
						timer: 2000
					});
					$($.fn.dataTable.tables(true)).DataTable().ajax.reload();
				} else {
					swal.hideLoading();
					swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
				};
				$('#modal-premix-formula').modal('hide');
			},
			error: function() {
				swal.hideLoading();
				swal.fire("!Opps ", "Terjadi masalah, coba sesaat lagi", "error");
			}
		});
	});
</script>