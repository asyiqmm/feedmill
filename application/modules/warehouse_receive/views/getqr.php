<!-- Main Sidebar Container -->

<?php $codeqr= $bc; 

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid">
           

			<div class='card'>
				<div class='card-header'>
				<h4 class='card-title'>Code <?php echo $codeqr; ?></h4>
					<div class="card-tools">
					
					</div>
				</div>
				
				<div class='card-body table-responsive p-3'>
				
				
					
				</div>
			</div>

		</div>

            <!-- /.card -->
            
            <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>


<div class="modal fade" id="ModalAddQrcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data QR Code</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form method='post' action='<?php echo site_url('warehouse_receive/add');?>'>
					<div class='form-group row'>
					<label for='waktu' class='col-sm-2 col-form-label'>Nama</label>
						<div class='col-sm-10'>
						<input type='text' class='form-control' id='nama' name='nama' placeholder='nama'>
						</div>
					</div>
					
					<div class='form-group row'>
                    <label for='jumlah' class='col-sm-2 col-form-label'>Jumlah</label>
						<div class='col-sm-10'>
						<input type='text' class='form-control' id='jumlah' placeholder='0' name='jumlah'>
						</div>
					</div>
        
					<!--<input type='submit' class='btn btn-primary' value='Simpan'> 
					
					-->
					
				</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
				<input type="submit" class="btn btn-primary" value="Simpan">

				</form>
			</div>
    </div>
  </div>
</div>


<script>
    $(function() {
        datatable_warehouse();
    });

    function datatable_warehouse() {
		
       var siteTable = $('#tabel_allcodes').DataTable( {
                    "info": true,
                    "destroy": true,
            		"lengthChange": true,
            		"order": [[ 0, "desc" ]],	
            	    "paging": true,
            	    "pageLength": 7,
            	    "lengthMenu": [[7,10, 25, 50, -1], [7,10, 25, 50, "All"]],
            		"scrollCollapse": true,
    	            "scrollY":        "500px",
    		        "scrollX":        true,
                    "ajax": 'warehouse_receive/get_all_codes',
                    "columns": [
                    
                        { 
							"data": 'id',
							"className": 'details-control align-middle text-nowrap',
							"width": '10%'
						},
                        { 
							"data": 'nama',
							"className": 'details-control align-middle text-nowrap',
							"width": '15%'
						},
                        { 
							"data": 'nama' ,
							"className": 'details-control text-center align-middle text-nowrap',
							"width": '5%',
                            "render" : function (data, type, row) {
                                
                                if(row['nama'] == null){
                                    return '<a href="javascript:void(0);" class="btn btn-danger btn-sm disabled item_edit" data-id="'+data+'">Data Kosong</a>';
                                    
                                }else{
                                     
                                    return '<a href="warehouse_receive/print/'+data+'" class="btn btn-info btn-sm" data-id="'+data+'"><i class="fas fa-print"></i>&nbsp Cetak</a>';
                                }
                            }
                        },
                        
                    ],
                    "columnDefs": [
                        {
                            "targets": '_all',
                            "defaultContent": '0'
                        }
                    ],
                    "order": [[ 0, 'desc' ]],
                    
                    initComplete: function () {
                      this.api().buttons().container()
                            .appendTo( $('.col-md-6:eq(0)', this.api().table().container()));
                     }
                } );
    }
</script>


<!--

select material, codeqr



-->