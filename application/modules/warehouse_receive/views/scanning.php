<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <!-- css & js -->
    <link type="text/css" href="<?php echo base_url('assets/pwa/css/styles-scanner.css');?>" rel="stylesheet">
    <link rel="manifest" href="<?php echo base_url('assets/pwa/manifest.json'); ?>">
    
    <link rel="preload" as="script" href="<?php echo base_url('assets/pwa/js/decoder.js'); ?>">
    <!-- ios support -->
    <link rel="apple-touch-icon" href="/img/icons/icon-96x96.png">
    <meta name="apple-mobile-web-app-status-bar" content="#FFE1C4">
    <meta name="theme-color" content="#FFE1C4">

  </head>
  <body>
    <div class="app__layout">
      

      <main class="app__layout-content">
        <video autoplay></video>

        <!-- Dialog  -->
        <div class="app__dialog app__dialog--hide">
          <div class="app__dialog-content">
            <h5>QR Code</h5>
            <input type="text" id="result">
          </div>
          <div class="app__dialog-actions">
            <button type="button" class="app__dialog-open">Open</button>
            <button type="button" class="app__dialog-close">Close</button>            
            <button type="button" class="app__dialog-close">APP</button>
          </div>
        </div>
		
        <div class="app__dialog-overlay app__dialog--hide"></div>
		
        <!-- Snackbar --> 
        <div class="app__snackbar"></div>
      </main>
      
    </div>
    <div class="app__overlay">
      <div class="app__overlay-frame"></div>
      <!-- Scanner animation -->
      <div class="custom-scanner"></div>
      <div class="app__help-text"> </div>
    </div>

    <div class="app__select-photos"></div>

    <!-- Java Script -->
    <script type="module" src="<?php echo base_url('assets/pwa/js/app.js');?>"></script>
    <script type="module" src="<?php echo base_url('assets/pwa/js/main.js'); ?>"></script>
  </body>
</html>
