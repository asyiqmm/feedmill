<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Warehouse_receive_model extends CI_Model
{
	public function post_qrcode_baru($input)
	{
		$this->db->trans_begin();
		$this->db->where('material_kode', $_POST['material']);
		$q = $this->db->get('m_material');

		if ($q->num_rows() > 0) {
			$this->db->set('material_nm', $_POST['material_name']);
			$this->db->set('unit', $_POST['unit']);
			$this->db->where('material_kode', $_POST['material']);
			$this->db->update('m_material');
		} else {
			$this->db->set('material_nm', $_POST['material_name']);
			$this->db->set('unit', $_POST['unit']);
			$this->db->set('material_kode', $_POST['material']);
			$this->db->insert('m_material');
		}
		$material = $_POST['material'];
		if (!empty($_POST['original_pack'])) {
			$this->db->set('active', 0)
				->where('material', $_POST['material'])
				->update('d_original_pack');
			$original_pack = array(
				'berat_karung'	=> $_POST['berat_karung'],
				'material' 		=> $_POST['material'],
				'active' 		=> 1,
			);
			if (!empty($_POST['original_pack'])) {
				$original_pack['original_pack'] = $_POST['original_pack'];
			}
			$this->db->insert('d_original_pack', $original_pack);

			$total_qty = $_POST['original_pack'] * $_POST['jumlah'];
			$sql = "UPDATE m_material SET qty = qty + $total_qty WHERE material_kode = '$material'";
			$this->db->query($sql);
		} else {
			$total_qty = 25 * $_POST['jumlah'];
			$sql = "UPDATE m_material SET qty = qty + $total_qty WHERE material_kode = '$material'";
			$this->db->query($sql);
		}

		$this->db->insert_batch('m_qrcodes', $input);
		// var_dump($this->db->last_query());die();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'Berhasil menambahkan kode'
			];
		}
	}

	public function save()
	{
		require_once(APPPATH . "libraries/phpqrcode/qrlib.php");
		//$this->db->trans_begin();


		$post = $this->input->post();
		$jml = $post['jumlah'];
		$idmaterial = $post['nama'];
		$mydate = date('Ymd');
		$now = date('Y-m-d');

		$this->db->select("max(id) as idx");
		$idx = $this->db->get('m_qrcodes')->row();
		$id = $idx->idx + 1;

		for ($i = 1; $i <= $jml; $i++) {
			$unq = str_pad($id, 4, "0", STR_PAD_LEFT);
			$this->id = '';
			//$this->id_material=$post['nama'];
			$this->type_code = '2';
			//$this->codes=$mydate.''.$idmaterial.''.$unq;
			$this->codes = $mydate . 'MAT' . $unq;
			$this->keterangan = '';
			$this->active = '1';
			//$this->created_date='';
			//$this->updated_date='';

			//$code = $mydate.''.$idmaterial.''.$unq;
			$code = $mydate . '' . $unq;

			//$fileName = $mydate.''.$idmaterial.''.$unq.'.png';
			$fileName = $mydate . '' . $unq . '.png';
			$tempDir = FCPATH . "/assets/upload/qrcode/";
			$filePath = $tempDir . "/" . $fileName;
			QRcode::png($code, $filePath);

			$a = $this->db->insert('m_qrcodes', $this);
			$idn = $this->db->insert_id();

			$arr = array(
				'id_qrcode' => $idn,
				'id_material' => $_POST['nama'],
				'sts_use' => 0,
				'active' => 1
			);
			$this->db->insert('d_material_qrcode', $arr);
			$id++;
		}


		//return $a;
		/*
		if ($this->db->trans_status() === false) {
            $this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
            return [
                'status' => false,
                'msg' => $this->db->error(),
            ];
        } else {
            $this->db->trans_commit();
            return [
                'status' => true,
                'msg' => 'berhasil menginput data'
            ];
        }
		*/
	}


	public function get_material()
	{
		$this->db->select("id, material_nm")
			->order_by("material_nm", "ASC");
		$query = $this->db->get('m_material');
		return $query;
	}

	public function get_all_codes()
	{
		$this->db->select("count(material) total,material,urutan_material,concat('[',material,'] ',material_name) nama, DATE(created_date) tgls, created_date, COUNT(case when sts_digunakan = 0 then 1 end) blm_digunakan")
			->from('m_qrcodes')
			->where('active', 1)
			->group_by("material,material_name,urutan_material,tanggal");
		$result = $this->db->get();
		// var_dump($this->db->last_query());
		// die();
		return $result;
	}

	public function get_qrcode()
	{
		$this->db->select("*")
			->from('m_qrcodes')
			->where('active', 1)
			->where('material', $_GET['material'])
			->where('urutan_material', $_GET['urutan_material'])
			->where('tanggal', $_GET['tanggal']);
		$result = $this->db->get();
		// var_dump($this->db->last_query());
		// die();
		return $result;
	}

	public function get_detail_codesx($id)
	{
		$this->db->select("id, nama, tanggal, codes");
		$this->db->where('nama', $id);
		$query = $this->db->get('qrcode');
		return $query->result();

		//$this->db->where('id',$id);
		//$query = $this->db->query("select id, nama, codes from qrcode where id='$id'");
		//return $query->result();
	}

	public function get_detail_codes($material, $tgl, $urutan)
	{

		$this->db->select('codes,material_name,tanggal');
		$this->db->from('m_qrcodes');
		$this->db->where('material', $material);
		$this->db->where('tanggal', $tgl);
		$this->db->where('urutan_material', $urutan);
		$this->db->where('sts_digunakan', 0);
		$query = $this->db->get();
		// var_dump($this->db->last_query());die();
		return $query->result();

		//$this->db->where('id',$id);
		//$query = $this->db->query("select id, nama, codes from qrcode where id='$id'");
		//return $query->result();
	}

	public function delete()
	{
		$this->db->trans_begin();
		$this->db->set('active', 0);
		$this->db->where('material', $_POST['material']);
		$this->db->where('urutan_material', $_POST['urutan_material']);
		$this->db->where('tanggal', $_POST['tanggal']);
		$this->db->update('m_qrcodes');
		$op = $this->db->select('COALESCE(original_pack,25) original_pack')
			->from('d_original_pack')
			->where('material', $_POST['material'])
			->where('active', 1)
			->limit(1)
			->get()->row();
		$total = $_POST['total'];
		$material = $_POST['material'];
		$sql = "UPDATE m_material SET qty = qty - ($op->original_pack * $total)
		WHERE material_kode = $material";
		// var_dump(
		// 	$sql
		// );
		// die();
		$this->db->query($sql);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'msg' => 'Berhasil menghapus data'
			];
		}
	}
}
