<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Warehouse_receive extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('warehouse_receive_model');
		$this->load->library('form_validation');
	}

	private function _render($view, $data = array())
	{
		$data['title'] 	= "Receive | Feedmill - PT.Dinamika Megatama Citra";
		$data['users'] = $this->ion_auth->user()->row();
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if ($this->ion_auth->logged_in()) {
			$data['parent_active'] = 2;
			$data['child_active'] = 3;
			$data['grandchild_active'] = null;
			//$result['data']=$this->warehouse_receive_model->get_all_codes();

			// $data['material'] = $this->warehouse_receive_model->get_material()->result();
			$this->_render('warehouse_receive_view', $data);
		} else {
			redirect('auth/login', 'refresh');
		}
	}


	public function get_codes()
	{
	}

	public function scanner()
	{
		$data['parent_active'] = 2;
		$data['child_active'] = 3;
		$data['grandchild_active'] = null;
		$this->_render('scanner', $data);
	}

	public function scanning()
	{
		$data['parent_active'] = 2;
		$data['child_active'] = 3;
		$data['grandchild_active'] = null;
		$this->_render('scanning', $data);
	}

	public function getqr($bc)
	{
		$data['parent_active'] = 2;
		$data['child_active'] = 3;
		$data['grandchild_active'] = null;
		$data['bc'] = $bc;
		$this->_render('getqr', $data);
	}


	public function post_qrcode_baru()
	{
		$stud = explode("⁑", $_POST['material']);
		$_POST['material'] = $stud[0];
		$_POST['material_name'] = $stud[1];
		$_POST['unit'] = $stud[2];
		$date = date('Ymd');
		$input = array();
		$max = $this->db->select('COALESCE(max(urutan_material+1), 1) max_material')
			->from('m_qrcodes')
			->where('tanggal', date('Y-m-d'))
			->get()->row();
		// var_dump($max);die();
		for ($i = 0; $i < $_POST['jumlah']; $i++) {

			$codes = $date . 'MAT' . str_pad($max->max_material, 2, "0", STR_PAD_LEFT) . '' . str_pad($i + 1, 2, "0", STR_PAD_LEFT);
			$input[] = array(
				'codes'				=> $codes,
				'material'			=> $_POST['material'],
				'material_name'		=> $_POST['material_name'],
				'urutan_material'	=> $max->max_material,
				'unit'				=> $_POST['unit'],
				'tanggal'			=> date('Y-m-d'),
				'urutan'			=> $i + 1,
			);
		}
		$receive = $this->warehouse_receive_model->post_qrcode_baru($input);
		echo json_encode($receive);
	}
	public function add()
	{

		$receive = $this->warehouse_receive_model;
		//$validation=$this->form_validation;
		//$validation->set_rules($receive->rules());

		//if($validation->run())
		//{
		$receive->save();

		//$this->session->set_flashdata('success', 'Berhasil disimpan');
		//}
		//$this->load->view("receive_view");
		redirect('warehouse_receive', 'refresh');
	}

	public function get_all_codes()
	{
		// var_dump('waduh');die();
		$datas = $this->warehouse_receive_model->get_all_codes();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function get_qrcode()
	{
		// var_dump('waduh');die();
		$datas = $this->warehouse_receive_model->get_qrcode();
		$data =  json_decode(json_encode($datas->result()), TRUE);
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function print($material, $tgl, $urutan)
	{
		$data['parent_active'] = 2;
		$data['child_active'] = 3;
		$data['grandchild_active'] = null;
		// $data['id'] = $id;
		$data['codes'] = $this->warehouse_receive_model->get_detail_codes($material, $tgl, $urutan);
		// var_dump($data);die();
		$this->load->view('print', $data);
	}

	public function delete()
	{
		$delete = $this->warehouse_receive_model->delete();
		echo json_encode($delete);
	}
}
