-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2021 at 10:02 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feedmill`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_material_qrcode`
--

CREATE TABLE `d_material_qrcode` (
  `id` int(11) NOT NULL,
  `id_qrcode` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `sts_use` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mq_qr` (`id_qrcode`),
  ADD KEY `fk_mq_material` (`id_material`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD CONSTRAINT `fk_mq_material` FOREIGN KEY (`id_material`) REFERENCES `m_material` (`id`),
  ADD CONSTRAINT `fk_mq_qr` FOREIGN KEY (`id_qrcode`) REFERENCES `m_qrcodes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
