-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2021 at 03:19 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feedmill`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_dosing`
--

CREATE TABLE `d_dosing` (
  `id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `id_transaction` varchar(25) DEFAULT NULL,
  `material_kd` int(11) NOT NULL,
  `material_nm` varchar(100) DEFAULT NULL,
  `qty_prosen` decimal(10,2) NOT NULL,
  `qty_material` decimal(11,2) NOT NULL,
  `qty_timbang_mikro` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tgl_timbang_mikro` datetime DEFAULT NULL,
  `qty_timbang_makro` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tgl_timbang_makro` datetime DEFAULT NULL,
  `qty_timbang_proses` decimal(10,2) DEFAULT NULL,
  `tgl_timbang_proses` datetime DEFAULT NULL,
  `dif` decimal(10,2) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_dosing`
--

INSERT INTO `d_dosing` (`id`, `sequence`, `id_transaction`, `material_kd`, `material_nm`, `qty_prosen`, `qty_material`, `qty_timbang_mikro`, `tgl_timbang_mikro`, `qty_timbang_makro`, `tgl_timbang_makro`, `qty_timbang_proses`, `tgl_timbang_proses`, `dif`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, '2021081801PMX001', 22000035, 'MIXTOVIT BREEDER', '20.00', '160.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(2, 2, '2021081801PMX001', 22000036, 'MIXTOVIT MIN BREEDER PLUS OPTIMIN', '20.00', '160.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(3, 3, '2021081801PMX001', 22000068, 'VIT E 50 ', '3.60', '28.80', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(4, 4, '2021081801PMX001', 22000010, 'BSG 510', '3.00', '24.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(5, 5, '2021081801PMX001', 22000344, 'ANILOX P-10', '3.00', '24.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(6, 6, '2021081801PMX001', 22000073, 'X P C', '15.00', '120.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(7, 7, '2021081801PMX001', 22000064, 'TBCC', '5.00', '40.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(8, 8, '2021081801PMX001', 21000007, 'LIMESTONE POWDER', '5.00', '40.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(9, 9, '2021081801PMX001', 21000017, 'WHEAT POLLARD', '12.20', '97.60', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(10, 10, '2021081801PMX001', 22000399, 'PHYTASE GRANULAR', '1.20', '9.60', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(11, 11, '2021081801PMX001', 22000355, 'NUTRIBUILDER', '12.00', '96.00', '0.00', NULL, '0.00', NULL, NULL, NULL, NULL, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `d_material_qrcode`
--

CREATE TABLE `d_material_qrcode` (
  `id` int(11) NOT NULL,
  `id_qrcode` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `sts_use` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `d_menu_groups`
--

CREATE TABLE `d_menu_groups` (
  `id` int(11) NOT NULL,
  `id_group` mediumint(8) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_menu_groups`
--

INSERT INTO `d_menu_groups` (`id`, `id_group`, `id_menu`, `created_date`, `updated_date`) VALUES
(1, 1, 1, '2021-04-28 10:33:35', '2021-04-28 10:33:35'),
(2, 1, 2, '2021-04-28 10:54:23', '2021-04-28 13:11:09'),
(3, 1, 3, '2021-04-28 10:54:29', '2021-04-28 13:11:12'),
(4, 1, 4, '2021-04-28 10:54:34', '2021-04-28 13:11:13'),
(5, 1, 5, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(6, 1, 6, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(7, 1, 7, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(8, 1, 8, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(9, 1, 9, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(10, 1, 10, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(11, 1, 11, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(12, 1, 12, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(13, 1, 13, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(14, 1, 14, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(15, 1, 15, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(16, 1, 16, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(17, 1, 17, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(18, 1, 18, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(19, 1, 19, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(20, 1, 20, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(21, 1, 21, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(22, 1, 22, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(23, 1, 23, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(24, 1, 24, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(25, 1, 25, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(26, 1, 26, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(27, 1, 27, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(28, 2, 6, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(29, 2, 8, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(30, 2, 9, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(31, 2, 10, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(32, 2, 11, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(33, 2, 25, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(34, 2, 27, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(35, 2, 26, '2021-08-10 08:31:00', '2021-08-10 08:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `d_original_pack`
--

CREATE TABLE `d_original_pack` (
  `id` int(11) NOT NULL,
  `material` varchar(25) NOT NULL,
  `original_pack` decimal(10,2) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_original_pack`
--

INSERT INTO `d_original_pack` (`id`, `material`, `original_pack`, `active`, `created_date`, `updated_date`) VALUES
(0, 'DEFAULT', '25.00', 1, '2021-08-16 08:50:11', '2021-08-16 08:50:18'),
(1, '22000072', '5.00', 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_bin`
--

CREATE TABLE `m_bin` (
  `id` int(11) NOT NULL,
  `bin_no` int(11) NOT NULL,
  `bin_code` varchar(100) NOT NULL,
  `bin_material` int(11) DEFAULT NULL,
  `material` int(15) DEFAULT NULL,
  `material_nm` varchar(25) DEFAULT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `slave` int(11) NOT NULL,
  `relay` int(11) DEFAULT NULL,
  `bin_open` double NOT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_bin`
--

INSERT INTO `m_bin` (`id`, `bin_no`, `bin_code`, `bin_material`, `material`, `material_nm`, `qty`, `active`, `slave`, `relay`, `bin_open`, `created_date`, `updated_date`) VALUES
(4, 6, '20210818BIN94165', 17, 22000068, 'VIT E 50 ', '0.00', 1, 2, 5, 0, '2021-08-18 11:47:38', '2021-08-18 14:05:45'),
(5, 7, '20210818BIN59685', 18, 22000010, 'BSG 510', '0.00', 1, 2, 6, 0, '2021-08-18 11:47:44', '2021-08-18 14:05:44'),
(6, 8, '20210818BIN95149', 10, 22000355, 'NUTRIBUILDER', '0.00', 1, 2, 7, 0, '2021-08-18 11:51:04', '2021-08-18 14:04:55'),
(7, 10, '20210818BIN65640', 11, 22000015, 'COPPER SULFAT', '0.00', 1, 1, 6, 0, '2021-08-18 11:51:11', '2021-08-18 14:05:02'),
(8, 9, '20210818BIN32031', 12, 22000393, 'CONCENTRASE P', '0.00', 1, 1, 7, 0, '2021-08-18 11:51:17', '2021-08-18 14:04:59'),
(9, 1, '20210818BIN80015', 13, 22000072, 'XANTHOFIL 2%', '0.00', 1, 2, 0, 1, '2021-08-18 11:51:24', '2021-08-18 14:59:17'),
(10, 2, '20210818BIN81826', 14, 22000070, 'VITAMIN LAYER', '0.00', 1, 2, 1, 0, '2021-08-18 11:51:31', '2021-08-18 14:05:52'),
(11, 3, '20210818BIN92843', 20, 22000073, 'X P C', '0.00', 1, 2, 2, 0, '2021-08-18 13:12:39', '2021-08-18 14:05:50'),
(12, 4, '20210818BIN65275', 21, 22000064, 'TBCC', '0.00', 1, 2, 3, 0, '2021-08-18 13:12:50', '2021-08-18 14:05:48'),
(13, 5, '20210818BIN15787', 8, 22000009, 'BMD . 100', '0.00', 1, 2, 4, 0, '2021-08-18 13:12:55', '2021-08-18 14:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `m_material`
--

CREATE TABLE `m_material` (
  `id` int(11) NOT NULL,
  `material_kode` int(11) NOT NULL,
  `material_nm` varchar(100) NOT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `qty_temp` decimal(10,2) NOT NULL,
  `bag_pack` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(5) NOT NULL DEFAULT 'KG',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_material`
--

INSERT INTO `m_material` (`id`, `material_kode`, `material_nm`, `qty`, `qty_temp`, `bag_pack`, `unit`, `active`, `created_date`, `updated_date`) VALUES
(1, 22000330, 'NUTRIMIX DMC-BRO', '100.00', '23.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 15:49:30'),
(2, 22000331, 'BETACELL MINERAL DMC-BRO', '125.00', '25.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-12 13:49:14'),
(3, 22000399, 'PHYTASE GRANULAR', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(4, 22000292, 'BM OX', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(5, 22000406, 'AXTRA XAP 101 TPT', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(6, 21000007, 'LIMESTONE POWDER', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(7, 21000017, 'WHEAT POLLARD', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(8, 22000009, 'BMD . 100', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(9, 22000384, 'ZAMSTAT 120', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(10, 22000355, 'NUTRIBUILDER', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(11, 22000015, 'COPPER SULFAT', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(12, 22000393, 'CONCENTRASE P', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(13, 22000072, 'XANTHOFIL 2%', '25.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-18 13:35:46'),
(14, 22000070, 'VITAMIN LAYER', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 15:46:46', '2021-08-12 08:26:21'),
(15, 22000035, 'MIXTOVIT BREEDER', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(16, 22000036, 'MIXTOVIT MIN BREEDER PLUS OPTIMIN', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(17, 22000068, 'VIT E 50 ', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(18, 22000010, 'BSG 510', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(19, 22000344, 'ANILOX P-10', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(20, 22000073, 'X P C', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(21, 22000064, 'TBCC', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `m_menus`
--

CREATE TABLE `m_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_nm` varchar(50) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `url` varchar(50) DEFAULT NULL,
  `orders` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menus`
--

INSERT INTO `m_menus` (`menu_id`, `menu_nm`, `icon`, `url`, `orders`, `parent_id`, `child_id`, `active`, `keterangan`, `created_date`, `updated_date`) VALUES
(1, 'Dashboard', 'fa-tachometer-alt', 'dashboard', 1, NULL, NULL, 1, '', '2021-04-28 10:32:42', '2021-04-28 10:43:17'),
(2, 'Warehouse', 'fa-warehouse', NULL, 2, NULL, NULL, 1, '', '2021-04-28 10:36:12', '2021-04-28 10:43:20'),
(3, 'Receive', 'fa-circle', 'warehouse_receive', 3, 2, NULL, 1, '', '2021-04-28 10:42:57', '2021-04-28 10:43:24'),
(4, 'Stok', 'fa-circle', 'warehouse_stok', 4, 2, NULL, 1, '', '2021-04-28 10:44:27', '2021-04-28 10:44:27'),
(5, 'Pemakaian', 'fa-circle', 'warehouse_pemakaian', 5, 2, NULL, 1, '', '2021-04-28 12:43:09', '2021-04-28 12:43:09'),
(6, 'Premix', 'fa-mortar-pestle', NULL, 6, NULL, NULL, 1, '', '2021-04-28 12:46:24', '2021-04-28 12:46:24'),
(7, 'Rencana Produksi', 'fa-circle', 'premix_rencana_produksi', 7, 6, NULL, 1, '', '2021-04-28 12:47:37', '2021-04-28 12:47:37'),
(8, 'Intake Premix', 'fa-circle', 'premix_intake', 8, 6, NULL, 1, '', '2021-04-28 12:48:56', '2021-04-28 12:48:56'),
(9, 'Dosing', 'fa-circle', NULL, 9, 6, NULL, 1, '', '2021-04-28 12:49:36', '2021-04-28 12:49:36'),
(10, 'Mikro', 'fa-dot-circle', 'premix_mikro', 10, 6, 9, 1, '', '2021-04-28 13:02:22', '2021-04-28 13:02:22'),
(11, 'Makro', 'fa-dot-circle', 'premix_makro', 11, 6, 9, 1, '', '2021-04-28 13:31:37', '2021-04-28 13:31:37'),
(12, 'Packing', 'fa-circle', 'premix_packing', 12, 6, NULL, 1, '', '2021-04-28 13:32:57', '2021-04-28 13:32:57'),
(13, 'Produksi', 'fa-spinner', NULL, 13, NULL, NULL, 1, '', '2021-04-28 13:38:45', '2021-04-28 13:38:45'),
(14, 'Rencana Produksi', 'fa-circle', 'produksi_rencana_produksi', 14, 13, NULL, 1, '', '2021-04-28 13:39:46', '2021-04-28 13:39:46'),
(15, 'Intake', 'fa-circle', 'produksi_intake', 15, 13, NULL, 1, '', '2021-04-28 13:40:20', '2021-04-28 13:40:20'),
(16, 'Dosing', 'fa-circle', 'produksi_dosing', 16, 13, NULL, 1, '', '2021-04-28 13:40:49', '2021-04-28 13:40:49'),
(17, 'Palet', 'fa-circle', 'produksi_palet', 17, 13, NULL, 1, '', '2021-04-28 13:41:23', '2021-04-28 13:41:23'),
(18, 'Bagging', 'fa-circle', 'produksi_bagging', 18, 13, NULL, 1, '', '2021-04-28 13:42:11', '2021-04-28 13:42:11'),
(19, 'Wharehouse Shipment', 'fa-truck', NULL, 19, NULL, NULL, 1, '', '2021-04-28 13:45:07', '2021-04-28 13:45:07'),
(20, 'Opname', 'fa-circle', 'ws_opname', 20, 19, NULL, 1, '', '2021-04-28 13:45:57', '2021-04-28 13:45:57'),
(21, 'Stok', 'fa-circle', 'ws_stok', 21, 19, NULL, 1, '', '2021-04-28 13:46:52', '2021-04-28 13:46:52'),
(22, 'Shipment', 'fa-circle', 'ws_shipment', 22, 19, NULL, 1, '', '2021-04-28 13:47:21', '2021-04-28 13:47:21'),
(23, 'QA', 'fa-microscope', 'quality_assurance', 23, NULL, NULL, 1, '', '2021-04-28 13:50:03', '2021-04-28 13:50:03'),
(24, 'Nutritionist', 'fa-tablets', 'nutritionist', 23, NULL, NULL, 1, '', '2021-04-28 13:50:03', '2021-04-28 13:50:03'),
(25, 'Data Master', 'fa-circle', NULL, 13, 6, NULL, 1, '', '2021-05-10 09:48:00', '2021-05-10 09:48:00'),
(26, 'Data Material', 'fa-dot-circle', 'data_material', 14, 6, 25, 1, '', '2021-05-10 09:50:54', '2021-05-10 09:50:54'),
(27, 'Data Bin', 'fa-dot-circle', 'data_bin', 15, 6, 25, 1, '', '2021-05-10 09:50:54', '2021-05-10 09:50:54');

-- --------------------------------------------------------

--
-- Table structure for table `m_qrcodes`
--

CREATE TABLE `m_qrcodes` (
  `id` int(11) NOT NULL,
  `codes` varchar(100) NOT NULL,
  `material` varchar(25) DEFAULT NULL,
  `material_name` varchar(25) DEFAULT NULL,
  `urutan_material` int(11) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `keterangan` varchar(250) NOT NULL,
  `sts_digunakan` double NOT NULL DEFAULT 0,
  `tgl_digunakan` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_qrcodes`
--

INSERT INTO `m_qrcodes` (`id`, `codes`, `material`, `material_name`, `urutan_material`, `unit`, `tanggal`, `urutan`, `keterangan`, `sts_digunakan`, `tgl_digunakan`, `active`, `created_date`, `updated_date`) VALUES
(1, '20210818MAT0101', '22000072', 'XANTHOFIL 2%', 1, 'KG', '2021-08-18', 1, '', 0, NULL, 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46'),
(2, '20210818MAT0102', '22000072', 'XANTHOFIL 2%', 1, 'KG', '2021-08-18', 2, '', 0, NULL, 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46'),
(3, '20210818MAT0103', '22000072', 'XANTHOFIL 2%', 1, 'KG', '2021-08-18', 3, '', 0, NULL, 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46'),
(4, '20210818MAT0104', '22000072', 'XANTHOFIL 2%', 1, 'KG', '2021-08-18', 4, '', 0, NULL, 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46'),
(5, '20210818MAT0105', '22000072', 'XANTHOFIL 2%', 1, 'KG', '2021-08-18', 5, '', 0, NULL, 1, '2021-08-18 13:35:46', '2021-08-18 13:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

CREATE TABLE `m_status` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`id`, `nama`, `active`, `created_date`, `updated_date`) VALUES
(1, 'READY', 1, '2021-04-30 13:13:42', '2021-04-30 13:13:42'),
(2, 'COMPLETED', 1, '2021-04-30 13:13:42', '2021-04-30 13:13:42'),
(3, 'FINISH', 1, '2021-05-10 10:19:08', '2021-05-10 10:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `m_unit_satuan`
--

CREATE TABLE `m_unit_satuan` (
  `id` int(11) NOT NULL,
  `unit_nm` varchar(10) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_unit_satuan`
--

INSERT INTO `m_unit_satuan` (`id`, `unit_nm`, `active`, `created_date`, `updated_date`) VALUES
(1, 'KG', 1, '2021-04-29 11:31:57', '2021-04-29 11:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `t_qrcodes_premix_packing`
--

CREATE TABLE `t_qrcodes_premix_packing` (
  `id` int(11) NOT NULL,
  `codes` varchar(25) NOT NULL,
  `id_transaction` varchar(20) NOT NULL,
  `no_pack` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_rencana_produksi`
--

CREATE TABLE `t_rencana_produksi` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(25) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `fml_transaction` varchar(25) DEFAULT NULL,
  `fml_no` varchar(25) NOT NULL,
  `item` varchar(18) NOT NULL,
  `fml_name` varchar(50) NOT NULL,
  `prosen` decimal(10,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `tgl_dosing` datetime DEFAULT NULL,
  `tgl_proses` datetime DEFAULT NULL,
  `sts_mikro` tinyint(1) NOT NULL DEFAULT 0,
  `sts_makro` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_rencana_produksi`
--

INSERT INTO `t_rencana_produksi` (`id`, `id_transaction`, `tanggal`, `urutan`, `fml_transaction`, `fml_no`, `item`, `fml_name`, `prosen`, `status`, `tgl_dosing`, `tgl_proses`, `sts_mikro`, `sts_makro`, `active`, `created_date`, `updated_date`) VALUES
(7, '2021081801PMX001', '2021-08-18', 1, '2021081801MP0001', '1808/21', '22200377', 'PMX BREED E 180821', '800.00', 1, NULL, NULL, 0, 0, 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat_intake`
--

CREATE TABLE `t_riwayat_intake` (
  `id` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `code_barang` varchar(50) DEFAULT NULL,
  `qty` decimal(10,2) NOT NULL,
  `bin_no` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat_stok`
--

CREATE TABLE `t_riwayat_stok` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(20) NOT NULL,
  `id_material` int(11) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$08$Nye1ftMEbKlIJ01DlPhnlumKPzkXgEdzf98MA/kTvt5PEHMItD1B2', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1629335397, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'premix', '$2y$08$v4ek4.yJJFvn8avPIsJ1qOLIj3cRRHYbysEp.NlYv5EpbG7Ue1hHS', NULL, '', NULL, NULL, NULL, NULL, 1628558744, 1629268682, 1, 'Admin', 'Premix', 'PREMIX\r\n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1),
(7, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_dosing`
--
ALTER TABLE `d_dosing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mq_qr` (`id_qrcode`),
  ADD KEY `fk_mq_material` (`id_material`);

--
-- Indexes for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_original_pack`
--
ALTER TABLE `d_original_pack`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_bin`
--
ALTER TABLE `m_bin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bin_material` (`bin_material`);

--
-- Indexes for table `m_material`
--
ALTER TABLE `m_material`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `material_kode` (`material_kode`);

--
-- Indexes for table `m_menus`
--
ALTER TABLE `m_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codes` (`codes`);

--
-- Indexes for table `m_status`
--
ALTER TABLE `m_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_unit_satuan`
--
ALTER TABLE `m_unit_satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_qrcodes_premix_packing`
--
ALTER TABLE `t_qrcodes_premix_packing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rencana_produksi`
--
ALTER TABLE `t_rencana_produksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_transaction` (`id_transaction`) USING BTREE;

--
-- Indexes for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tri_material` (`id_material`);

--
-- Indexes for table `t_riwayat_stok`
--
ALTER TABLE `t_riwayat_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_dosing`
--
ALTER TABLE `d_dosing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `d_original_pack`
--
ALTER TABLE `d_original_pack`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `m_bin`
--
ALTER TABLE `m_bin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `m_material`
--
ALTER TABLE `m_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `m_menus`
--
ALTER TABLE `m_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_status`
--
ALTER TABLE `m_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_unit_satuan`
--
ALTER TABLE `m_unit_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_qrcodes_premix_packing`
--
ALTER TABLE `t_qrcodes_premix_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_rencana_produksi`
--
ALTER TABLE `t_rencana_produksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_riwayat_stok`
--
ALTER TABLE `t_riwayat_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD CONSTRAINT `fk_mq_material` FOREIGN KEY (`id_material`) REFERENCES `m_material` (`id`),
  ADD CONSTRAINT `fk_mq_qr` FOREIGN KEY (`id_qrcode`) REFERENCES `m_qrcodes` (`id`);

--
-- Constraints for table `m_bin`
--
ALTER TABLE `m_bin`
  ADD CONSTRAINT `fk_bin_material` FOREIGN KEY (`bin_material`) REFERENCES `m_material` (`id`);

--
-- Constraints for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  ADD CONSTRAINT `fk_tri_material` FOREIGN KEY (`id_material`) REFERENCES `m_material` (`id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
