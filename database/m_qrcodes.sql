-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2021 at 09:52 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feedmill`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_qrcodes`
--

CREATE TABLE `m_qrcodes` (
  `id` int(11) NOT NULL,
  `codes` varchar(100) NOT NULL,
  `type_code` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_qrcodes`
--

INSERT INTO `m_qrcodes` (`id`, `codes`, `type_code`, `keterangan`, `active`, `created_date`, `updated_date`) VALUES
(1, '20210430110820001', 1, 'bin nomor 01', 1, '2021-05-06 14:48:42', '2021-05-06 14:48:42'),
(2, '20210430110820002', 1, 'bin nomor 02', 1, '2021-05-06 14:48:42', '2021-05-06 14:50:39'),
(3, '20210430110820003', 2, 'barcode material VITAMIN LAYER', 1, '2021-05-06 14:51:47', '2021-05-06 14:51:47'),
(4, '20210430110820004', 2, 'barcode material BETACELL MINERAL DMC LY', 1, '2021-05-06 14:51:47', '2021-05-06 14:51:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codes` (`codes`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
