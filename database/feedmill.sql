-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2021 at 03:36 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `feedmill`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_dosing`
--

CREATE TABLE `d_dosing` (
  `id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `id_transaction` varchar(25) DEFAULT NULL,
  `material_kd` int(11) NOT NULL,
  `material_nm` varchar(100) DEFAULT NULL,
  `qty_prosen` decimal(10,2) NOT NULL,
  `qty_material` decimal(11,2) NOT NULL,
  `qty_timbang_mikro` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tgl_timbang_mikro` datetime DEFAULT NULL,
  `qty_timbang_makro` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tgl_timbang_makro` datetime DEFAULT NULL,
  `qty_timbang_proses` decimal(10,2) DEFAULT NULL,
  `tgl_timbang_proses` datetime DEFAULT NULL,
  `dif` decimal(10,2) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_dosing`
--

INSERT INTO `d_dosing` (`id`, `sequence`, `id_transaction`, `material_kd`, `material_nm`, `qty_prosen`, `qty_material`, `qty_timbang_mikro`, `tgl_timbang_mikro`, `qty_timbang_makro`, `tgl_timbang_makro`, `qty_timbang_proses`, `tgl_timbang_proses`, `dif`, `active`, `created_date`, `updated_date`) VALUES
(1, 1, '2022101101PMX001', 22000034, 'MIXTOVIT BROILER', '6.00', '48.00', '25.00', '2021-10-11 10:09:24', '0.00', NULL, '25.00', '2021-10-11 10:23:20', '-23.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(2, 2, '2022101101PMX001', 22000033, 'MIXTOVIT MIN MIX BROILER', '10.00', '80.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-80.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(3, 3, '2022101101PMX001', 22000399, 'PHYTASE GRANULAR', '2.30', '18.40', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-18.40', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(4, 4, '2022101101PMX001', 22000292, 'BM OX', '3.00', '24.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-24.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(5, 5, '2022101101PMX001', 22000406, 'AXTRA XAP 101 TPT', '2.00', '16.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-16.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(6, 6, '2022101101PMX001', 21000007, 'LIMESTONE POWDER', '14.00', '112.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-112.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(7, 7, '2022101101PMX001', 21000017, 'WHEAT POLLARD', '30.92', '247.36', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-247.36', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(8, 8, '2022101101PMX001', 22000072, 'XANTHOFIL 2%', '1.60', '12.80', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-12.80', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(9, 9, '2022101101PMX001', 22000009, 'BMD . 100', '13.00', '104.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-104.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(10, 10, '2022101101PMX001', 22000286, 'MONENDOX 200BMP', '10.00', '80.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-80.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(11, 11, '2022101101PMX001', 22000064, 'TBCC', '4.68', '37.44', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-37.44', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20'),
(12, 12, '2022101101PMX001', 22000393, 'CONCENTRASE P', '2.50', '20.00', '0.00', NULL, '0.00', NULL, '0.00', '2021-10-11 10:23:20', '-20.00', 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `d_material_qrcode`
--

CREATE TABLE `d_material_qrcode` (
  `id` int(11) NOT NULL,
  `id_qrcode` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `sts_use` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `d_menu_groups`
--

CREATE TABLE `d_menu_groups` (
  `id` int(11) NOT NULL,
  `id_group` mediumint(8) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_menu_groups`
--

INSERT INTO `d_menu_groups` (`id`, `id_group`, `id_menu`, `created_date`, `updated_date`) VALUES
(1, 1, 1, '2021-04-28 10:33:35', '2021-04-28 10:33:35'),
(2, 1, 2, '2021-04-28 10:54:23', '2021-04-28 13:11:09'),
(3, 1, 3, '2021-04-28 10:54:29', '2021-04-28 13:11:12'),
(4, 1, 4, '2021-04-28 10:54:34', '2021-04-28 13:11:13'),
(5, 1, 5, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(6, 1, 6, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(7, 1, 7, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(8, 1, 8, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(9, 1, 9, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(10, 1, 10, '2021-04-28 13:11:50', '2021-04-28 13:11:50'),
(11, 1, 11, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(12, 1, 12, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(13, 1, 13, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(14, 1, 14, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(15, 1, 15, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(16, 1, 16, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(17, 1, 17, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(18, 1, 18, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(19, 1, 19, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(20, 1, 20, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(21, 1, 21, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(22, 1, 22, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(23, 1, 23, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(24, 1, 24, '2021-04-28 13:51:05', '2021-04-28 13:51:05'),
(25, 1, 25, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(26, 1, 26, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(27, 1, 27, '2021-05-10 09:51:46', '2021-05-10 09:51:46'),
(28, 2, 6, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(29, 2, 8, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(30, 2, 9, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(31, 2, 10, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(32, 2, 11, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(33, 2, 25, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(34, 2, 27, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(35, 2, 26, '2021-08-10 08:31:00', '2021-08-10 08:31:00'),
(36, 1, 28, '2021-10-19 11:36:29', '2021-10-19 11:36:29'),
(37, 1, 29, '2021-10-19 11:36:29', '2021-10-19 11:36:29'),
(38, 3, 28, '2021-10-19 16:00:42', '2021-10-19 16:00:42'),
(39, 3, 29, '2021-10-19 16:00:42', '2021-10-19 16:00:42'),
(40, 3, 23, '2021-10-19 16:00:42', '2021-10-19 16:00:42'),
(41, 1, 30, '2021-10-25 16:18:24', '2021-10-25 16:18:24'),
(42, 1, 31, '2021-10-25 16:18:24', '2021-10-25 16:18:24'),
(43, 1, 32, '2021-10-25 16:24:18', '2021-10-25 16:24:18'),
(44, 4, 28, '2021-10-29 09:05:42', '2021-10-29 09:05:42'),
(45, 4, 29, '2021-10-29 09:05:42', '2021-10-29 09:05:42'),
(46, 3, 35, '2021-11-01 14:08:43', '2021-11-01 14:08:43');

-- --------------------------------------------------------

--
-- Table structure for table `d_original_pack`
--

CREATE TABLE `d_original_pack` (
  `id` int(11) NOT NULL,
  `material` varchar(25) NOT NULL,
  `original_pack` decimal(10,2) NOT NULL,
  `berat_karung` decimal(10,2) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_original_pack`
--

INSERT INTO `d_original_pack` (`id`, `material`, `original_pack`, `berat_karung`, `active`, `created_date`, `updated_date`) VALUES
(0, 'DEFAULT', '25.00', '0.20', 1, '2021-08-16 08:50:11', '2021-10-01 08:22:04'),
(1, '22000072', '5.00', '0.20', 0, '2021-08-18 13:35:46', '2021-10-01 08:22:04'),
(2, '22000034', '25.00', '0.20', 0, '2021-09-01 10:27:15', '2021-10-11 10:06:03'),
(3, '22000033', '25.00', '0.20', 0, '2021-09-01 10:27:30', '2021-10-01 08:22:04'),
(4, '22000399', '25.00', '0.20', 0, '2021-09-01 10:27:42', '2021-10-01 08:22:04'),
(5, '22000292', '25.00', '0.20', 0, '2021-09-01 10:27:52', '2021-10-01 08:22:04'),
(6, '22000292', '25.00', '0.20', 0, '2021-09-01 10:28:03', '2021-10-01 08:22:04'),
(7, '22000406', '25.00', '0.20', 0, '2021-09-01 10:28:15', '2021-10-01 08:22:04'),
(8, '21000007', '25.00', '0.20', 0, '2021-09-01 10:28:26', '2021-10-01 08:22:04'),
(9, '21000017', '25.00', '0.20', 0, '2021-09-01 10:28:40', '2021-10-01 08:22:04'),
(10, '22000072', '25.00', '0.20', 0, '2021-09-01 10:28:55', '2021-10-01 08:22:04'),
(11, '22000357', '25.00', '0.20', 1, '2021-09-01 10:29:06', '2021-10-01 08:22:04'),
(12, '22000075', '25.00', '0.20', 1, '2021-09-01 10:29:17', '2021-10-01 08:22:04'),
(13, '22000010', '25.00', '0.20', 1, '2021-09-13 13:38:49', '2021-10-01 08:22:04'),
(14, '22000070', '25.00', '0.20', 0, '2021-09-15 08:33:14', '2021-10-01 08:22:04'),
(15, '22000330', '25.00', '0.20', 0, '2021-09-15 10:08:41', '2021-10-11 09:37:25'),
(16, '22000331', '25.00', '0.20', 1, '2021-09-15 10:11:24', '2021-10-01 08:22:04'),
(17, '22000399', '25.00', '0.20', 0, '2021-09-15 10:12:09', '2021-10-11 10:10:32'),
(18, '22000292', '25.00', '0.20', 1, '2021-09-15 10:13:09', '2021-10-01 08:22:04'),
(19, '22000406', '25.00', '0.20', 1, '2021-09-15 10:13:48', '2021-10-01 08:22:04'),
(20, '21000007', '25.00', '0.20', 1, '2021-09-15 10:15:08', '2021-10-01 08:22:04'),
(21, '21000017', '25.00', '0.20', 1, '2021-09-15 10:15:47', '2021-10-01 08:22:04'),
(22, '22000324', '25.00', '0.20', 1, '2021-09-15 10:16:24', '2021-10-01 08:22:04'),
(23, '22000355', '25.00', '0.20', 1, '2021-09-15 10:17:19', '2021-10-01 08:22:04'),
(24, '22000015', '25.00', '0.20', 1, '2021-09-15 10:18:07', '2021-10-01 08:22:04'),
(25, '22000393', '25.00', '0.20', 1, '2021-09-15 10:19:57', '2021-10-01 08:22:04'),
(26, '22000072', '25.00', '0.20', 1, '2021-09-15 10:20:52', '2021-10-01 08:22:04'),
(27, '22000295', '25.00', '0.20', 1, '2021-09-15 10:21:43', '2021-10-01 08:22:04'),
(28, '22000070', '25.00', '0.20', 0, '2021-09-15 13:31:21', '2021-10-01 08:22:04'),
(29, '20000002', '25.00', '0.20', 1, '2021-09-15 15:23:08', '2021-10-01 08:22:04'),
(30, '21000002', '250.00', '0.20', 1, '2021-09-15 15:26:17', '2021-10-01 08:22:04'),
(31, '22000033', '20.00', '0.20', 1, '2021-09-15 15:33:14', '2021-10-01 08:22:04'),
(32, '22000070', '25.00', '0.20', 0, '2021-09-17 15:52:54', '2021-10-01 08:21:56'),
(33, '22000070', '25.00', '0.20', 0, '2021-09-17 15:57:16', '2021-10-11 09:30:25'),
(34, '22000070', '25.00', '0.20', 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(35, '22000330', '25.00', '0.20', 1, '2021-10-11 09:37:25', '2021-10-11 09:37:25'),
(36, '22000034', '25.00', '0.20', 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(37, '22000399', '25.00', '0.20', 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'qa', 'Quality Assurance'),
(4, 'operator', 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_bin`
--

CREATE TABLE `m_bin` (
  `id` int(11) NOT NULL,
  `bin_no` int(11) NOT NULL,
  `bin_code` varchar(100) NOT NULL,
  `bin_material` int(11) DEFAULT NULL,
  `material` int(15) DEFAULT NULL,
  `material_nm` varchar(25) DEFAULT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `slave` int(11) NOT NULL,
  `relay` int(11) DEFAULT NULL,
  `bin_open` double NOT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_bin`
--

INSERT INTO `m_bin` (`id`, `bin_no`, `bin_code`, `bin_material`, `material`, `material_nm`, `qty`, `active`, `slave`, `relay`, `bin_open`, `created_date`, `updated_date`) VALUES
(4, 6, '20210818BIN94165', 6, 21000007, 'LIMESTONE POWDER', '0.00', 1, 2, 5, 0, '2021-08-18 11:47:38', '2021-09-15 10:52:29'),
(5, 7, '20210818BIN59685', 19, 22000344, 'ANILOX P-10', '0.00', 1, 2, 6, 0, '2021-08-18 11:47:44', '2021-09-15 10:53:02'),
(6, 8, '20210818BIN95149', 13, 22000072, 'XANTHOFIL 2%', '24.00', 1, 2, 7, 0, '2021-08-18 11:51:04', '2021-09-15 10:59:48'),
(7, 10, '20210818BIN65640', 25, 22000075, 'YANQOL', '0.00', 1, 1, 1, 0, '2021-08-18 11:51:11', '2021-09-15 10:53:12'),
(8, 9, '20210818BIN32031', 24, 22000357, 'MAXUS', '0.00', 1, 1, 0, 0, '2021-08-18 11:51:17', '2021-09-15 10:53:09'),
(9, 1, '20210818BIN80015', 22, 22000034, 'MIXTOVIT BROILER', '-1.00', 1, 2, 0, 0, '2021-08-18 11:51:24', '2021-10-11 10:09:24'),
(10, 2, '20210818BIN81826', 23, 22000033, 'MIXTOVIT MIN MIX BROILER', '19.99', 1, 2, 1, 0, '2021-08-18 11:51:31', '2021-09-15 15:35:43'),
(11, 3, '20210818BIN92843', 17, 22000068, 'VIT E 50 ', '0.00', 1, 2, 2, 0, '2021-08-18 13:12:39', '2021-10-11 10:10:02'),
(12, 4, '20210818BIN65275', 4, 22000292, 'BM OX', '0.00', 1, 2, 3, 0, '2021-08-18 13:12:50', '2021-09-15 10:52:54'),
(13, 5, '20210818BIN15787', 5, 22000406, 'AXTRA XAP 101 TPT', '0.00', 1, 2, 4, 0, '2021-08-18 13:12:55', '2021-09-15 10:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `m_limit_ews`
--

CREATE TABLE `m_limit_ews` (
  `id` int(11) NOT NULL,
  `jenis` varchar(25) NOT NULL,
  `fase` varchar(25) NOT NULL,
  `min_temp_aktual` decimal(10,2) NOT NULL,
  `max_ampere` decimal(10,2) NOT NULL,
  `max_dust_cooler` decimal(10,2) NOT NULL,
  `std_pdi` decimal(10,2) NOT NULL,
  `max_tepungan` decimal(10,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_limit_ews`
--

INSERT INTO `m_limit_ews` (`id`, `jenis`, `fase`, `min_temp_aktual`, `max_ampere`, `max_dust_cooler`, `std_pdi`, `max_tepungan`, `active`, `created_date`, `updated_date`) VALUES
(1, 'BROILER', 'STARTER', '83.00', '400.00', '10.00', '90.00', '12.00', 1, '2021-10-25 16:15:25', '2021-10-25 16:33:34'),
(3, 'BROILER', 'PRESTARTER', '82.00', '400.00', '10.00', '90.00', '15.00', 1, '2021-10-26 11:39:02', '2021-10-26 11:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `m_material`
--

CREATE TABLE `m_material` (
  `id` int(11) NOT NULL,
  `material_kode` int(11) NOT NULL,
  `material_nm` varchar(100) NOT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `qty_temp` decimal(10,2) NOT NULL,
  `bag_pack` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(5) NOT NULL DEFAULT 'KG',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_material`
--

INSERT INTO `m_material` (`id`, `material_kode`, `material_nm`, `qty`, `qty_temp`, `bag_pack`, `unit`, `active`, `created_date`, `updated_date`) VALUES
(1, 22000330, 'NUTRIMIX DMC-BRO', '302.00', '23.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-10-11 09:37:25'),
(2, 22000331, 'BETACELL MINERAL DMC-BRO', '170.00', '25.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(3, 22000399, 'PHYTASE GRANULAR', '431.60', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-10-11 10:10:32'),
(4, 22000292, 'BM OX', '551.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(5, 22000406, 'AXTRA XAP 101 TPT', '309.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(6, 21000007, 'LIMESTONE POWDER', '213.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(7, 21000017, 'WHEAT POLLARD', '94.44', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(8, 22000009, 'BMD . 100', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(9, 22000384, 'ZAMSTAT 120', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-08-10 14:05:20'),
(10, 22000355, 'NUTRIBUILDER', '-21.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(11, 22000015, 'COPPER SULFAT', '75.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 10:18:07'),
(12, 22000393, 'CONCENTRASE P', '55.00', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(13, 22000072, 'XANTHOFIL 2%', '337.20', '0.00', '0.00', 'KG', 1, '2021-08-10 14:05:20', '2021-09-15 14:43:32'),
(14, 22000070, 'VITAMIN LAYER', '500.00', '0.00', '0.00', 'KG', 1, '2021-08-10 15:46:46', '2021-10-11 09:30:25'),
(15, 22000035, 'MIXTOVIT BREEDER', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(16, 22000036, 'MIXTOVIT MIN BREEDER PLUS OPTIMIN', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(17, 22000068, 'VIT E 50 ', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(18, 22000010, 'BSG 510', '125.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-09-13 13:38:49'),
(19, 22000344, 'ANILOX P-10', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(20, 22000073, 'X P C', '0.00', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-08-18 09:28:19'),
(21, 22000064, 'TBCC', '-37.44', '0.00', '0.00', 'KG', 1, '2021-08-18 09:28:19', '2021-09-15 14:43:32'),
(22, 22000034, 'MIXTOVIT BROILER', '350.00', '0.00', '0.00', 'KG', 1, '2021-09-01 10:24:03', '2021-10-11 10:23:20'),
(23, 22000033, 'MIXTOVIT MIN MIX BROILER', '270.00', '0.00', '0.00', 'KG', 1, '2021-09-01 10:24:03', '2021-09-15 15:33:14'),
(24, 22000357, 'MAXUS', '241.20', '0.00', '0.00', 'KG', 1, '2021-09-01 10:24:03', '2021-09-15 14:43:32'),
(25, 22000075, 'YANQOL', '154.00', '0.00', '0.00', 'KG', 1, '2021-09-01 10:24:03', '2021-09-15 14:43:32'),
(26, 22000008, 'BETACELL MINERAL DMC LY', '0.00', '0.00', '0.00', 'KG', 1, '2021-09-14 15:25:29', '2021-09-14 15:25:29'),
(27, 22000024, 'HALQUINOL 60%', '0.00', '0.00', '0.00', 'KG', 1, '2021-09-14 15:25:29', '2021-09-14 15:25:29'),
(28, 22000358, '\r\nGUSTOR N', '0.00', '0.00', '0.00', 'KG', 1, '2021-09-14 15:25:29', '2021-09-14 15:25:29'),
(29, 22000369, 'CHROMIUM PICOLINATE', '0.00', '0.00', '0.00', 'KG', 1, '2021-09-14 15:25:29', '2021-09-14 15:25:29'),
(30, 22000324, 'MONTEBAN 100', '75.00', '0.00', '0.00', 'KG', 1, '2021-09-15 10:06:56', '2021-09-15 10:16:24'),
(31, 22000295, 'HEMICELL HT ', '75.00', '0.00', '0.00', 'KG', 1, '2021-09-15 10:06:56', '2021-09-15 10:21:43'),
(32, 20000002, 'DOC P/S BROILER', '125.00', '0.00', '0.00', 'BRD', 1, '2021-09-15 15:23:08', '2021-09-15 15:23:08'),
(33, 21000002, 'CORN GLUTEN MEAL . CGM 60%', '2500.00', '0.00', '0.00', 'MT', 1, '2021-09-15 15:26:17', '2021-09-15 15:26:17'),
(34, 22000286, 'MONENDOX 200BMP', '0.00', '0.00', '0.00', 'KG', 1, '2021-10-11 09:34:42', '2021-10-11 09:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `m_menus`
--

CREATE TABLE `m_menus` (
  `menu_id` int(11) NOT NULL,
  `menu_nm` varchar(50) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `url` varchar(50) DEFAULT NULL,
  `orders` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menus`
--

INSERT INTO `m_menus` (`menu_id`, `menu_nm`, `icon`, `url`, `orders`, `parent_id`, `child_id`, `active`, `keterangan`, `created_date`, `updated_date`) VALUES
(1, 'Dashboard', 'fa-tachometer-alt', 'dashboard', 1, NULL, NULL, 1, '', '2021-04-28 10:32:42', '2021-04-28 10:43:17'),
(2, 'Warehouse', 'fa-warehouse', NULL, 2, NULL, NULL, 1, '', '2021-04-28 10:36:12', '2021-04-28 10:43:20'),
(3, 'Receive', 'fa-circle', 'warehouse_receive', 3, 2, NULL, 1, '', '2021-04-28 10:42:57', '2021-04-28 10:43:24'),
(4, 'Stok', 'fa-circle', 'warehouse_stok', 4, 2, NULL, 1, '', '2021-04-28 10:44:27', '2021-04-28 10:44:27'),
(5, 'Pemakaian', 'fa-circle', 'warehouse_pemakaian', 5, 2, NULL, 1, '', '2021-04-28 12:43:09', '2021-04-28 12:43:09'),
(6, 'Premix', 'fa-mortar-pestle', NULL, 6, NULL, NULL, 1, '', '2021-04-28 12:46:24', '2021-04-28 12:46:24'),
(7, 'Rencana Produksi', 'fa-circle', 'premix_rencana_produksi', 7, 6, NULL, 1, '', '2021-04-28 12:47:37', '2021-04-28 12:47:37'),
(8, 'Intake Premix', 'fa-circle', 'premix_intake', 8, 6, NULL, 1, '', '2021-04-28 12:48:56', '2021-04-28 12:48:56'),
(9, 'Dosing', 'fa-circle', NULL, 9, 6, NULL, 1, '', '2021-04-28 12:49:36', '2021-04-28 12:49:36'),
(10, 'Mikro', 'fa-dot-circle', 'premix_mikro', 10, 6, 9, 1, '', '2021-04-28 13:02:22', '2021-04-28 13:02:22'),
(11, 'Makro', 'fa-dot-circle', 'premix_makro', 11, 6, 9, 1, '', '2021-04-28 13:31:37', '2021-04-28 13:31:37'),
(12, 'Packing', 'fa-circle', 'premix_packing', 12, 6, NULL, 1, '', '2021-04-28 13:32:57', '2021-04-28 13:32:57'),
(13, 'Produksi', 'fa-spinner', NULL, 13, NULL, NULL, 1, '', '2021-04-28 13:38:45', '2021-04-28 13:38:45'),
(14, 'Rencana Produksi', 'fa-circle', 'produksi_rencana_produksi', 14, 13, NULL, 1, '', '2021-04-28 13:39:46', '2021-04-28 13:39:46'),
(15, 'Intake', 'fa-circle', 'produksi_intake', 15, 13, NULL, 1, '', '2021-04-28 13:40:20', '2021-04-28 13:40:20'),
(16, 'Dosing', 'fa-circle', 'produksi_dosing', 16, 13, NULL, 1, '', '2021-04-28 13:40:49', '2021-04-28 13:40:49'),
(17, 'Palet', 'fa-circle', 'produksi_palet', 17, 13, NULL, 1, '', '2021-04-28 13:41:23', '2021-04-28 13:41:23'),
(18, 'Bagging', 'fa-circle', 'produksi_bagging', 18, 13, NULL, 1, '', '2021-04-28 13:42:11', '2021-04-28 13:42:11'),
(19, 'Wharehouse Shipment', 'fa-truck', NULL, 19, NULL, NULL, 1, '', '2021-04-28 13:45:07', '2021-04-28 13:45:07'),
(20, 'Opname', 'fa-circle', 'ws_opname', 20, 19, NULL, 1, '', '2021-04-28 13:45:57', '2021-04-28 13:45:57'),
(21, 'Stok', 'fa-circle', 'ws_stok', 21, 19, NULL, 1, '', '2021-04-28 13:46:52', '2021-04-28 13:46:52'),
(22, 'Shipment', 'fa-circle', 'ws_shipment', 22, 19, NULL, 1, '', '2021-04-28 13:47:21', '2021-04-28 13:47:21'),
(23, 'QA', 'fa-microscope', 'quality_assurance', 23, NULL, NULL, 1, '', '2021-04-28 13:50:03', '2021-04-28 13:50:03'),
(24, 'Nutritionist', 'fa-tablets', 'nutritionist', 23, NULL, NULL, 1, '', '2021-04-28 13:50:03', '2021-04-28 13:50:03'),
(25, 'Data Master', 'fa-circle', NULL, 13, 6, NULL, 1, '', '2021-05-10 09:48:00', '2021-05-10 09:48:00'),
(26, 'Data Material', 'fa-dot-circle', 'data_material', 14, 6, 25, 1, '', '2021-05-10 09:50:54', '2021-05-10 09:50:54'),
(27, 'Data Bin', 'fa-dot-circle', 'data_bin', 15, 6, 25, 1, '', '2021-05-10 09:50:54', '2021-05-10 09:50:54'),
(28, 'EWS', 'fa-filter', NULL, 24, NULL, NULL, 1, '', '2021-10-19 11:34:19', '2021-10-19 11:34:19'),
(29, 'Data Produksi', 'fa-circle', 'ews_rencana_produksi', 25, 28, NULL, 1, '', '2021-10-19 11:36:01', '2021-10-19 11:36:01'),
(30, 'Data Master', 'fa-circle', NULL, 26, 28, NULL, 1, '', '2021-10-25 16:17:46', '2021-10-25 16:17:46'),
(31, 'Limit EWS', 'fa-dot-circle', 'ews_limit_ews', 28, 28, 30, 1, '', '2021-10-25 16:19:25', '2021-10-25 16:19:25'),
(32, 'Data Pakan', 'fa-dot-circle', 'ews_data_pakan', 27, 28, 30, 1, '', '2021-10-25 16:22:48', '2021-10-25 16:22:48'),
(34, 'Rencana Produksi', 'fa-circle', 'ews_rencana_produksi', 25, 28, NULL, 1, 'Menu EWS untuk supervisor', '2021-10-28 09:10:59', '2021-10-28 09:10:59'),
(35, 'Report EWS', 'fa-circle', 'report_ews', 29, 28, NULL, 1, '', '2021-11-01 14:07:42', '2021-11-01 14:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `m_pakan`
--

CREATE TABLE `m_pakan` (
  `kode_pakan` varchar(15) NOT NULL,
  `nama_pakan` varchar(25) NOT NULL,
  `jenis` varchar(25) NOT NULL,
  `fase` varchar(25) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_pakan`
--

INSERT INTO `m_pakan` (`kode_pakan`, `nama_pakan`, `jenis`, `fase`, `active`, `created_date`, `updated_date`) VALUES
('BRO-ST001', 'SB 20 FCK', 'BROILER', 'STARTER', 1, '2021-10-19 11:15:14', '2021-10-19 11:15:14'),
('BRO-ST002', 'SB 21 CR', 'BROILER', 'STARTER', 1, '2021-10-19 11:15:14', '2021-10-19 11:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `m_qrcodes`
--

CREATE TABLE `m_qrcodes` (
  `id` int(11) NOT NULL,
  `codes` varchar(100) NOT NULL,
  `material` varchar(25) DEFAULT NULL,
  `material_name` varchar(25) DEFAULT NULL,
  `urutan_material` int(11) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `keterangan` varchar(250) NOT NULL,
  `sts_digunakan` double NOT NULL DEFAULT 0,
  `tgl_digunakan` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_qrcodes`
--

INSERT INTO `m_qrcodes` (`id`, `codes`, `material`, `material_name`, `urutan_material`, `unit`, `tanggal`, `urutan`, `keterangan`, `sts_digunakan`, `tgl_digunakan`, `active`, `created_date`, `updated_date`) VALUES
(1, '20211011MAT0101', '22000070', 'VITAMIN LAYER', 1, 'KG', '2021-10-11', 1, '', 0, NULL, 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(2, '20211011MAT0102', '22000070', 'VITAMIN LAYER', 1, 'KG', '2021-10-11', 2, '', 0, NULL, 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(3, '20211011MAT0103', '22000070', 'VITAMIN LAYER', 1, 'KG', '2021-10-11', 3, '', 0, NULL, 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(4, '20211011MAT0104', '22000070', 'VITAMIN LAYER', 1, 'KG', '2021-10-11', 4, '', 0, NULL, 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(5, '20211011MAT0105', '22000070', 'VITAMIN LAYER', 1, 'KG', '2021-10-11', 5, '', 0, NULL, 1, '2021-10-11 09:30:25', '2021-10-11 09:30:25'),
(6, '20211011MAT0201', '22000330', 'NUTRIMIX DMC-BRO', 2, 'KG', '2021-10-11', 1, '', 1, '2021-10-11 09:53:59', 1, '2021-10-11 09:37:25', '2021-10-11 09:53:59'),
(7, '20211011MAT0202', '22000330', 'NUTRIMIX DMC-BRO', 2, 'KG', '2021-10-11', 2, '', 1, '2021-10-11 09:57:37', 1, '2021-10-11 09:37:25', '2021-10-11 09:57:37'),
(8, '20211011MAT0203', '22000330', 'NUTRIMIX DMC-BRO', 2, 'KG', '2021-10-11', 3, '', 0, NULL, 1, '2021-10-11 09:37:25', '2021-10-11 09:37:25'),
(9, '20211011MAT0204', '22000330', 'NUTRIMIX DMC-BRO', 2, 'KG', '2021-10-11', 4, '', 0, NULL, 1, '2021-10-11 09:37:25', '2021-10-11 09:37:25'),
(10, '20211011MAT0205', '22000330', 'NUTRIMIX DMC-BRO', 2, 'KG', '2021-10-11', 5, '', 0, NULL, 1, '2021-10-11 09:37:25', '2021-10-11 09:37:25'),
(11, '20211011MAT0301', '22000034', 'MIXTOVIT BROILER', 3, 'KG', '2021-10-11', 1, '', 0, NULL, 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(12, '20211011MAT0302', '22000034', 'MIXTOVIT BROILER', 3, 'KG', '2021-10-11', 2, '', 0, NULL, 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(13, '20211011MAT0303', '22000034', 'MIXTOVIT BROILER', 3, 'KG', '2021-10-11', 3, '', 0, NULL, 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(14, '20211011MAT0304', '22000034', 'MIXTOVIT BROILER', 3, 'KG', '2021-10-11', 4, '', 0, NULL, 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(15, '20211011MAT0305', '22000034', 'MIXTOVIT BROILER', 3, 'KG', '2021-10-11', 5, '', 0, NULL, 1, '2021-10-11 10:06:03', '2021-10-11 10:06:03'),
(16, '20211011MAT0401', '22000399', 'PHYTASE GRANULAR', 4, 'KG', '2021-10-11', 1, '', 0, NULL, 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32'),
(17, '20211011MAT0402', '22000399', 'PHYTASE GRANULAR', 4, 'KG', '2021-10-11', 2, '', 0, NULL, 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32'),
(18, '20211011MAT0403', '22000399', 'PHYTASE GRANULAR', 4, 'KG', '2021-10-11', 3, '', 0, NULL, 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32'),
(19, '20211011MAT0404', '22000399', 'PHYTASE GRANULAR', 4, 'KG', '2021-10-11', 4, '', 0, NULL, 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32'),
(20, '20211011MAT0405', '22000399', 'PHYTASE GRANULAR', 4, 'KG', '2021-10-11', 5, '', 0, NULL, 1, '2021-10-11 10:10:32', '2021-10-11 10:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

CREATE TABLE `m_status` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`id`, `nama`, `active`, `created_date`, `updated_date`) VALUES
(1, 'READY', 1, '2021-04-30 13:13:42', '2021-04-30 13:13:42'),
(2, 'COMPLETED', 1, '2021-04-30 13:13:42', '2021-04-30 13:13:42'),
(3, 'FINISH', 1, '2021-05-10 10:19:08', '2021-05-10 10:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `m_unit_satuan`
--

CREATE TABLE `m_unit_satuan` (
  `id` int(11) NOT NULL,
  `unit_nm` varchar(10) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_unit_satuan`
--

INSERT INTO `m_unit_satuan` (`id`, `unit_nm`, `active`, `created_date`, `updated_date`) VALUES
(1, 'KG', 1, '2021-04-29 11:31:57', '2021-04-29 11:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `t_ews_produksi`
--

CREATE TABLE `t_ews_produksi` (
  `no_input` int(11) NOT NULL,
  `kode_pakan` varchar(15) NOT NULL,
  `nama_pakan` varchar(25) NOT NULL,
  `jenis_pakan` varchar(25) NOT NULL,
  `fase_pakan` varchar(25) NOT NULL,
  `kode_batch` varchar(20) NOT NULL,
  `formula` varchar(20) NOT NULL,
  `no_pellet` varchar(10) DEFAULT NULL,
  `tgl_produksi` date NOT NULL,
  `shift` int(11) NOT NULL,
  `steam` decimal(10,2) DEFAULT NULL,
  `retentioner` decimal(10,2) DEFAULT NULL,
  `temp_panel` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `temp_aktual` decimal(10,2) DEFAULT NULL,
  `min_temp_aktual` decimal(10,2) DEFAULT NULL,
  `temp_aktual_min_temp_aktual` tinyint(1) DEFAULT NULL,
  `ampere` decimal(10,2) DEFAULT NULL,
  `max_ampere` decimal(10,2) DEFAULT NULL,
  `ampere_max_ampere` tinyint(1) DEFAULT NULL,
  `speed_feeder` decimal(10,2) DEFAULT NULL,
  `dust_cooler` decimal(10,2) DEFAULT NULL,
  `max_dust_cooler` decimal(10,2) DEFAULT NULL,
  `dust_cooler_max_dust_cooler` tinyint(1) DEFAULT NULL,
  `pdi_pellet` decimal(10,2) DEFAULT NULL,
  `std_pdi` decimal(10,2) DEFAULT NULL,
  `pdi_pellet_std_pdi` tinyint(1) DEFAULT NULL,
  `crumble` decimal(10,2) DEFAULT NULL,
  `sz8_sample1` decimal(10,2) DEFAULT NULL,
  `sz8_sample2` decimal(10,2) DEFAULT NULL,
  `8_mesh` decimal(10,2) DEFAULT NULL,
  `sz14_sample1` decimal(10,2) DEFAULT NULL,
  `sz14_sample2` decimal(10,2) DEFAULT NULL,
  `tepungan` decimal(10,2) DEFAULT NULL,
  `max_tepungan` decimal(10,2) DEFAULT NULL,
  `tepungan_max_tepungan` tinyint(1) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_ews_produksi`
--

INSERT INTO `t_ews_produksi` (`no_input`, `kode_pakan`, `nama_pakan`, `jenis_pakan`, `fase_pakan`, `kode_batch`, `formula`, `no_pellet`, `tgl_produksi`, `shift`, `steam`, `retentioner`, `temp_panel`, `status`, `active`, `created_date`, `updated_date`, `temp_aktual`, `min_temp_aktual`, `temp_aktual_min_temp_aktual`, `ampere`, `max_ampere`, `ampere_max_ampere`, `speed_feeder`, `dust_cooler`, `max_dust_cooler`, `dust_cooler_max_dust_cooler`, `pdi_pellet`, `std_pdi`, `pdi_pellet_std_pdi`, `crumble`, `sz8_sample1`, `sz8_sample2`, `8_mesh`, `sz14_sample1`, `sz14_sample2`, `tepungan`, `max_tepungan`, `tepungan_max_tepungan`, `keterangan`) VALUES
(1, 'BRO-ST001', 'SB 20 FCK', 'BROILER', 'STARTER', '222', '010521', '1', '2021-10-19', 1, '1.00', '1.00', 1, 3, 1, '2021-10-19 15:14:58', '2021-11-02 15:37:07', '84.00', '83.00', 0, '351.00', '400.00', 0, '25.00', '10.00', '10.00', 1, '89.00', '90.00', 1, '88.00', '55.00', '55.00', '55.00', '12.00', '12.00', '12.00', '12.00', 1, 'percobaan'),
(2, 'BRO-ST002', 'SB 21 CR', 'BROILER', 'STARTER', '1231321', '090521', '1', '2021-10-19', 3, '2.00', '2.00', 2, 1, 1, '2021-10-19 15:15:39', '2021-10-27 14:53:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'BRO-ST002', 'SB 21 CR', 'BROILER', 'STARTER', '1231321', '010521', '2', '2021-10-27', 1, '20.00', '22.00', 22, 1, 1, '2021-10-27 15:30:37', '2021-10-27 15:30:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'BRO-ST001', 'SB 20 FCK', 'BROILER', 'STARTER', '1051', '010521', '4', '2021-10-29', 2, '1.00', '2.00', 3, 2, 1, '2021-10-29 08:49:03', '2021-10-29 11:09:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_qrcodes_premix_packing`
--

CREATE TABLE `t_qrcodes_premix_packing` (
  `id` int(11) NOT NULL,
  `codes` varchar(25) NOT NULL,
  `id_transaction` varchar(20) NOT NULL,
  `no_pack` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_qrcodes_premix_packing`
--

INSERT INTO `t_qrcodes_premix_packing` (`id`, `codes`, `id_transaction`, `no_pack`, `active`, `created_date`, `updated_date`) VALUES
(1, '202110110101', '2022101101PMX001', 1, 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `t_rencana_produksi`
--

CREATE TABLE `t_rencana_produksi` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(25) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `fml_transaction` varchar(25) DEFAULT NULL,
  `fml_no` varchar(25) NOT NULL,
  `item` varchar(18) NOT NULL,
  `fml_name` varchar(50) NOT NULL,
  `prosen` decimal(10,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `tgl_dosing` datetime DEFAULT NULL,
  `tgl_proses` datetime DEFAULT NULL,
  `sts_mikro` tinyint(1) NOT NULL DEFAULT 0,
  `sts_makro` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_rencana_produksi`
--

INSERT INTO `t_rencana_produksi` (`id`, `id_transaction`, `tanggal`, `urutan`, `fml_transaction`, `fml_no`, `item`, `fml_name`, `prosen`, `status`, `tgl_dosing`, `tgl_proses`, `sts_mikro`, `sts_makro`, `active`, `created_date`, `updated_date`) VALUES
(1, '2022101101PMX001', '2021-10-11', 1, '2021100401MP0001', '0410/21', '22200393', 'PMX BRO S2 041021', '800.00', 3, NULL, '2021-10-11 10:23:20', 1, 1, 1, '2021-10-11 09:34:42', '2021-10-11 10:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat_intake`
--

CREATE TABLE `t_riwayat_intake` (
  `id` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `code_barang` varchar(50) DEFAULT NULL,
  `qty` decimal(10,2) NOT NULL,
  `bin_no` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_riwayat_intake`
--

INSERT INTO `t_riwayat_intake` (`id`, `id_material`, `code_barang`, `qty`, `bin_no`, `active`, `created_date`, `updated_date`) VALUES
(5, 13, '20210818MAT0102', '2.11', 1, 1, '2021-09-01 09:58:54', '2021-09-01 09:58:54'),
(6, 13, '20210818MAT0101', '2.48', 1, 1, '2021-09-01 10:19:07', '2021-09-01 10:19:07'),
(7, 13, '20210818MAT0104', '34.80', 1, 1, '2021-09-01 10:19:51', '2021-09-01 10:19:51'),
(8, 13, '20210818MAT0103', '33.79', 1, 1, '2021-09-01 10:23:19', '2021-09-01 10:23:19'),
(9, 13, '20210915MAT1303', '24.00', 8, 1, '2021-09-15 10:59:48', '2021-09-15 10:59:48'),
(10, 10, '20210915MAT1001', '25.00', 1, 1, '2021-09-15 11:00:54', '2021-09-15 11:00:54'),
(11, 10, '20210915MAT1002', '24.00', 1, 1, '2021-09-15 11:01:32', '2021-09-15 11:01:32'),
(12, 31, '20210915MAT1403', '24.00', 2, 1, '2021-09-15 11:02:14', '2021-09-15 11:02:14'),
(13, 1, '20210915MAT0205', '25.00', 1, 1, '2021-09-15 11:04:38', '2021-09-15 11:04:38'),
(14, 2, '20210915MAT0302', '24.00', 1, 1, '2021-09-15 13:56:29', '2021-09-15 13:56:29'),
(15, 2, '20210915MAT0303', '25.00', 1, 1, '2021-09-15 14:00:19', '2021-09-15 14:00:19'),
(16, 1, '20210915MAT0201', '24.00', 1, 1, '2021-09-15 14:13:57', '2021-09-15 14:13:57'),
(17, 1, '20210915MAT0203', '1.42', 1, 1, '2021-09-15 15:15:28', '2021-09-15 15:15:28'),
(18, 1, '20210915MAT0204', '20.01', 1, 1, '2021-09-15 15:31:55', '2021-09-15 15:31:55'),
(19, 23, '20210915MAT1801', '19.99', 2, 1, '2021-09-15 15:35:42', '2021-09-15 15:35:42'),
(25, 1, '20211011MAT0201', '24.00', 1, 1, '2021-10-11 09:53:59', '2021-10-11 09:53:59'),
(28, 1, '20211011MAT0202', '24.00', 1, 1, '2021-10-11 09:57:37', '2021-10-11 09:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat_stok`
--

CREATE TABLE `t_riwayat_stok` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(20) NOT NULL,
  `id_material` int(11) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_riwayat_stok`
--

INSERT INTO `t_riwayat_stok` (`id`, `id_transaction`, `id_material`, `qty`, `active`, `created_date`, `updated_date`) VALUES
(1, '2022091501PMX004', 22000330, '48.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(2, '2022091501PMX004', 22000331, '80.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(3, '2022091501PMX004', 22000399, '18.40', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(4, '2022091501PMX004', 22000292, '24.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(5, '2022091501PMX004', 22000406, '16.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(6, '2022091501PMX004', 21000007, '112.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(7, '2022091501PMX004', 21000017, '230.56', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(8, '2022091501PMX004', 22000357, '8.80', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(9, '2022091501PMX004', 22000075, '96.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(10, '2022091501PMX004', 22000355, '96.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(11, '2022091501PMX004', 22000064, '37.44', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(12, '2022091501PMX004', 22000393, '20.00', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(13, '2022091501PMX004', 22000072, '12.80', 1, '2021-09-15 14:43:32', '2021-09-15 14:43:32'),
(14, '2022101101PMX001', 22000034, '25.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(15, '2022101101PMX001', 22000033, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(16, '2022101101PMX001', 22000399, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(17, '2022101101PMX001', 22000292, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(18, '2022101101PMX001', 22000406, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(19, '2022101101PMX001', 21000007, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(20, '2022101101PMX001', 21000017, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(21, '2022101101PMX001', 22000072, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(22, '2022101101PMX001', 22000009, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(23, '2022101101PMX001', 22000286, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(24, '2022101101PMX001', 22000064, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20'),
(25, '2022101101PMX001', 22000393, '0.00', 1, '2021-10-11 10:23:20', '2021-10-11 10:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$08$Nye1ftMEbKlIJ01DlPhnlumKPzkXgEdzf98MA/kTvt5PEHMItD1B2', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1635922144, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'premix', '$2y$08$v4ek4.yJJFvn8avPIsJ1qOLIj3cRRHYbysEp.NlYv5EpbG7Ue1hHS', NULL, '', NULL, NULL, NULL, NULL, 1628558744, 1631694519, 1, 'Admin', 'Premix', 'PREMIX\r\n', NULL),
(3, '::1', 'qa', '$2y$08$mUH0DPCaIgzMOpQgMmnpgufmplXf8KX4YAdAY.uo6qDxEo/XSRtbO', NULL, '', NULL, NULL, NULL, NULL, 1634633854, 1635905036, 1, 'Q', 'A', NULL, NULL),
(4, '::1', 'operator', '$2y$08$/1VHyEn0efnlIvQihnaWWeatioSkaEehI8dq8tmqtvfj5MpbMqvly', NULL, '', NULL, NULL, NULL, NULL, 1635473001, 1635903024, 1, 'Ope', 'rator', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1),
(7, 2, 2),
(9, 3, 3),
(13, 4, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_dosing`
--
ALTER TABLE `d_dosing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mq_qr` (`id_qrcode`),
  ADD KEY `fk_mq_material` (`id_material`);

--
-- Indexes for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_original_pack`
--
ALTER TABLE `d_original_pack`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_bin`
--
ALTER TABLE `m_bin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bin_material` (`bin_material`);

--
-- Indexes for table `m_limit_ews`
--
ALTER TABLE `m_limit_ews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_material`
--
ALTER TABLE `m_material`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `material_kode` (`material_kode`);

--
-- Indexes for table `m_menus`
--
ALTER TABLE `m_menus`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `m_pakan`
--
ALTER TABLE `m_pakan`
  ADD PRIMARY KEY (`kode_pakan`);

--
-- Indexes for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codes` (`codes`);

--
-- Indexes for table `m_status`
--
ALTER TABLE `m_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_unit_satuan`
--
ALTER TABLE `m_unit_satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_ews_produksi`
--
ALTER TABLE `t_ews_produksi`
  ADD PRIMARY KEY (`no_input`),
  ADD KEY `fk_pakan_id` (`kode_pakan`);

--
-- Indexes for table `t_qrcodes_premix_packing`
--
ALTER TABLE `t_qrcodes_premix_packing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rencana_produksi`
--
ALTER TABLE `t_rencana_produksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_transaction` (`id_transaction`) USING BTREE;

--
-- Indexes for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tri_material` (`id_material`);

--
-- Indexes for table `t_riwayat_stok`
--
ALTER TABLE `t_riwayat_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_dosing`
--
ALTER TABLE `d_dosing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `d_menu_groups`
--
ALTER TABLE `d_menu_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `d_original_pack`
--
ALTER TABLE `d_original_pack`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `m_bin`
--
ALTER TABLE `m_bin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `m_limit_ews`
--
ALTER TABLE `m_limit_ews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_material`
--
ALTER TABLE `m_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `m_menus`
--
ALTER TABLE `m_menus`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `m_qrcodes`
--
ALTER TABLE `m_qrcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `m_status`
--
ALTER TABLE `m_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_unit_satuan`
--
ALTER TABLE `m_unit_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_qrcodes_premix_packing`
--
ALTER TABLE `t_qrcodes_premix_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_rencana_produksi`
--
ALTER TABLE `t_rencana_produksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_riwayat_stok`
--
ALTER TABLE `t_riwayat_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_material_qrcode`
--
ALTER TABLE `d_material_qrcode`
  ADD CONSTRAINT `fk_mq_material` FOREIGN KEY (`id_material`) REFERENCES `m_material` (`id`),
  ADD CONSTRAINT `fk_mq_qr` FOREIGN KEY (`id_qrcode`) REFERENCES `m_qrcodes` (`id`);

--
-- Constraints for table `m_bin`
--
ALTER TABLE `m_bin`
  ADD CONSTRAINT `fk_bin_material` FOREIGN KEY (`bin_material`) REFERENCES `m_material` (`id`);

--
-- Constraints for table `t_ews_produksi`
--
ALTER TABLE `t_ews_produksi`
  ADD CONSTRAINT `fk_pakan_id` FOREIGN KEY (`kode_pakan`) REFERENCES `m_pakan` (`kode_pakan`) ON DELETE CASCADE;

--
-- Constraints for table `t_riwayat_intake`
--
ALTER TABLE `t_riwayat_intake`
  ADD CONSTRAINT `fk_tri_material` FOREIGN KEY (`id_material`) REFERENCES `m_material` (`id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
